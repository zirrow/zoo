<?php
class ModelDesignStiker extends Model {

	public function getProductStikers($product_id) {

		$stikers = array();

		$sql = "SELECT r.product_id
				FROM " . DB_PREFIX . "relatedoptions r
					LEFT JOIN " . DB_PREFIX . "relatedoptions_special rs ON (r.relatedoptions_id = rs.relatedoptions_id)
					LEFT JOIN " . DB_PREFIX . "product p ON (r.product_id = p.product_id) 
					LEFT JOIN " . DB_PREFIX . "product_description pd ON (r.product_id = pd.product_id) 
					LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (r.product_id = p2s.product_id)
		        WHERE p.status = '1'
		            AND r.product_id = '" . $product_id . "'
			        AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
					AND (rs.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') 
		        GROUP BY r.product_id";

		$query = $this->db->query($sql);

		if ($query->num_rows != 0) {
			$stikers['rb'] = $this->getStikerById($this->config->get('config_stiker_sale'));
		}

		$sql = "SELECT ps.product_id
				FROM " . DB_PREFIX . "product_special ps 
					LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) 
					LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
					LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)
				WHERE p.status = '1'
				    AND ps.product_id = '" . $product_id . "'
					AND p.date_available <= NOW() 
					AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' 
					AND (ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') 
					AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
				GROUP BY ps.product_id";

		$query = $this->db->query($sql);

		if ($query->num_rows != 0) {
			$stikers['rb'] = $this->getStikerById($this->config->get('config_stiker_sale'));
		}

		$sql = "SELECT date_available, stiker_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $product_id . "'";

		$query = $this->db->query($sql);

		$date_time_array = getdate(time());
		$timestamp = mktime($date_time_array['hours'], $date_time_array['minutes'], $date_time_array['seconds'], $date_time_array['mon'], $date_time_array['mday'] - $this->config->get('config_stiker_new_date'), $date_time_array['year']);

		if ($query->row['date_available'] >= date("Y-m-d", $timestamp)) {
			$stikers['rt'] = $this->getStikerById($this->config->get('config_stiker_new'));
		}

		if ($query->row['stiker_id']){
			$stikers['lb'] = $this->getStikerById($query->row['stiker_id']);
		}

		$sql = "SELECT setting FROM " . DB_PREFIX . "module WHERE code = 'featured'";

		$query = $this->db->query($sql);

		$featured_product_id = array();
		foreach ($query->rows as $setting){
			$featured = json_decode($setting['setting'],true);

			foreach ($featured['product'] as $product){
				$featured_product_id[] = $product;
			}

		}

		if (in_array($product_id,$featured_product_id)){
			$stikers['lt'] = $this->getStikerById($this->config->get('config_stiker_recommended'));
		}

		return $stikers;

	}

	public function getStikerById ($stiker_id){

		$sql = "SELECT st.image, st.text_on, std.title 
				FROM " . DB_PREFIX . "stiker st
					LEFT JOIN " . DB_PREFIX . "stiker_description std ON (st.stiker_id = std.stiker_id)
				WHERE st.stiker_id = '".$stiker_id."'
					AND std.language_id = '".(int)$this->config->get('config_language_id')."'";

		$query = $this->db->query($sql);

		return $query->row;
	}
}