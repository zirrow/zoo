<?php echo $header; ?>

<div class="breadcrumbs">
  <div class="container">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>


<?php echo $column_left; ?>
      <?php echo $content_top; ?>

          <section class="cd-intro">
            <h1><?php echo $heading_title; ?></h1>
          </section> <!-- .cd-intro -->

          <section class="cd-products-comparison-table">
            <header>

              <div class="actions">
                <a href="#0" class="reset">Reset</a>
                <a href="#0" class="filter">Filter</a>
              </div>
            </header>

            <div class="cd-products-table">
              <div class="features">
                <div class="top-info"><?php echo $text_name; ?></div>
                <ul class="cd-features-list">
                  <li><?php echo $text_price; ?></li>
                  <li><?php echo $text_rating; ?></li>
                  <li><?php echo $text_model; ?></li>
                  <li><?php echo $text_manufacturer; ?></li>
                  <li><?php echo $text_availability; ?></li>
                  <?php foreach ($attribute_groups as $attribute_group) { ?>
                    <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                      <li><?php echo $attribute['name']; ?></li>
                    <?php } ?>
                  <?php } ?>
                  <li></li>
                </ul>
              </div> <!-- .features -->

              <div class="cd-products-wrapper">
                <ul class="cd-products-columns">

                  <?php foreach ($products as $product) { ?>
                    <li class="product">
                    <div class="top-info">
                      <div class="check"></div>
                      <?php if ($product['thumb']) { ?>
                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                      <?php } ?>
                      <a href="<?php echo $product['href']; ?>"><h3><?php echo $product['name']; ?></h3></a>
                    </div> <!-- .top-info -->

                    <ul class="cd-features-list">
                      <li>
                        <?php if ($product['price']) { ?>
                          <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?>
                          <?php } else { ?>
                            <strike><?php echo $product['price']; ?></strike> <?php echo $product['special']; ?>
                          <?php } ?>
                        <?php } ?>
                      </li>
                      <li>
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <?php if ($product['rating'] < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                          <?php } ?>
                        <?php } ?>
                      </li>
                      <li><?php echo $product['model']; ?></li>
                      <li><?php echo $product['manufacturer']; ?></li>
                      <li><?php echo $product['availability']; ?></li>

                      <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                          <?php if (isset($product['attribute'][$key])) { ?>
                            <li><?php echo $product['attribute'][$key]; ?></li>
                          <?php } else { ?>
                            <li></li>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>

                      <li>
                        <input type="button" value="<?php echo $button_cart; ?>" class="btn btn-primary btn-block" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" />
                      </li>
                      <li>
                        <a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a>
                      </li>

                    </ul>
                  </li> <!-- .product -->
                  <?php } ?>


                </ul> <!-- .cd-products-columns -->
              </div> <!-- .cd-products-wrapper -->

              <ul class="cd-table-navigation">
                <li><a href="#0" class="prev inactive">Prev</a></li>
                <li><a href="#0" class="next">Next</a></li>
              </ul>
            </div> <!-- .cd-products-table -->
          </section> <!-- .cd-products-comparison-table -->

    <?php echo $content_bottom; ?>
<?php echo $column_right; ?>

<?php echo $footer; ?>
