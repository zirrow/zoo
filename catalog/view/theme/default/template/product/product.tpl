<?php echo $header; ?>

<?php echo $column_left; ?>

	<div class="conteiner">
		<?php echo $content_top; ?>
	</div>


	<div class="breadcrumbs">
	  <div class="container">
	    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	    <?php } ?>
	  </div>
	</div>


	<section class="product-page">
        <div class="container">
            <div class="product-page__inner">
                <div class="product-page__slider">
			        <div class="product-page__slider-layout">
			          <?php echo $stikers; ?>
			        </div>

			        <?php if ($thumb) { ?>
			          <div class="product-page__slider-container easyzoom easyzoom--with-thumbnails easyzoom--overlay">
			            <a href="<?php echo $thumb_zoom; ?>">
			              <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" width="253" height="253">
			            </a>
			          </div>
			        <?php } ?>

			        <ul class="thumbnail">
			          <?php if ($thumb) { ?>
			            <li class="item item--active">
			              <a href="javascript:void(0);" data-standard="<?php echo $thumb; ?>" data-easyzoom="<?php echo $thumb_zoom; ?>">
			                <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
			              </a>
			            </li>
			          <?php } ?>

			          <?php if ($images) { ?>
			            <?php foreach ($images as $image) { ?>
			              <li class="item">
			                <a href="javascript:void(0);" data-standard="<?php echo $image['thumb']; ?>" data-easyzoom="<?php echo $image['popup']; ?>">
			                  <img src="<?php echo $image['thumb']; ?>">
			                </a>
			              </li>
			            <?php } ?>
			          <?php } ?>
			        </ul>
                </div>

                <div class="product-page__description">
                    <div class="product-page__description-inner">
			          <div class="product-page__description-line-15">
			            <h3 class="product-page__description-title"><?php echo $heading_title; ?></h3>
			          </div>

                      <div class="product-page__description-line-15">
			            <?php if ($manufacturer) { ?>
			                <a href="<?php echo $manufacturer_link; ?>" class="product-page__description-maker"><?php echo $manufacturer; ?></a>
			            <?php } ?>

			            <?php if ($review_status) { ?>
			              <div class="product-page__description-rating rating">
			                <?php for ($i = 1; $i <= 5; $i++) { ?>
			                  <?php if ($rating < $i) { ?>
			                    <span class="rating__span fa fa-stack">
			                      <i class="rating__icon fa fa-star-o fa-stack-1x"></i>
			                    </span>
			                  <?php } else { ?>
			                    <span class="rating__span fa fa-stack">
			                      <i class="rating__icon fa fa-star-o fa-stack-1x"></i>
			                    </span>
			                  <?php } ?>
			                <?php } ?>
			              </div>

			              <span class="product-page__description-rating-count">
			                <a href="#tab-review-link" class="reviews-link"><?php echo $reviews; ?></a>
			              </span>
			            <?php } ?>

			            <a onclick="wishlist.add('<?php echo $product_id; ?>');" title="<?php echo $button_wishlist; ?>" class="product-page__description-favorites"></a>
			            <a onclick="compare.add('<?php echo $product_id; ?>');" title="<?php echo $button_compare; ?>" class="product-page__description-compare"></a>
                      </div>

			          <div class="product-page__description-line-25">
			            <?php if(!$logged){ ?>
			                <p class="product-page__description-reg">
				                <a href="<?php echo $register; ?>">
					                <?php echo $text_register; ?>
				                </a>
				                <?php echo $text_more_sale; ?>
			                </p>
			            <?php } ?>

				        <?php if(isset($description_sale)) { ?>
			                <p class="product-page__description-red"><?php echo $description_sale; ?></p>
				        <?php } ?>
			          </div>

                      <div class="product-page__description-line-25">

			            <?php foreach($related_options as $related_option){ ?>
			              <div class="product-page__description-line-options">
				              <div class="product-page__description-line-option">
			                    <div class="product-page__description-line-option-wrap">
									<label for="option-<?php echo $related_option['relatedoptions_id']; ?>">
										<input type="radio" name="option" id="option-<?php echo $related_option['relatedoptions_id']; ?>">
										<span class="product-page__description-line-option-text"></span>
										<?php echo $related_option['name']; ?>
									</label>
			                    </div>
					              <?php if (!empty($related_option['sku'])) { ?>
					                <span class="product-page__description-line-option-article"><?php echo $related_option['sku']; ?></span>
					              <?php } ?>

			                  </div>

				              <?php if (!empty($related_option['specials']) || !empty($related_option['discounts'])) { ?>
				                    <?php if (!empty($related_option['specials'])) { ?>
						                <div class="product-page__description-line-sale"><span>-<?php echo $related_option['specials'][0]['percent']; ?>%</span>
						                  <strike><?php echo $related_option['price']; ?></strike>
						                </div>

						                <div class="product-page__description-line-price">
							                <span><strong><?php echo $related_option['specials'][0]['price']; ?></strong></span>
						                </div>
				                    <?php } else { ?>
							              <div class="product-page__description-line-sale"><span>-<?php echo $related_option['discounts'][0]['percent']; ?>%</span>
								              <strike><?php echo $related_option['price']; ?></strike>
							              </div>

							              <div class="product-page__description-line-price">
								              <span><strong><?php echo $related_option['discounts'][0]['price']; ?></strong></span>
							              </div>
				                    <?php } ?>
				              <?php } else { ?>
					              <div class="product-page__description-line-price">
						              <span><strong><?php echo $related_option['price']; ?></strong></span>
					              </div>
				              <?php } ?>

				              <?php foreach ($related_option['options'] as $option_key => $option_value) { ?>
				                <input type="hidden" name="<?php echo $option_key; ?>" value="<?php echo $option_value; ?>">
				              <?php } ?>

			              </div>
			            <?php } ?>

                      </div>

			          <div class="product-page__description-line-10" id="product">

				          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />

			              <div class="product-page__description-count">
				              <div class="count">
					              <span class="count__max count__max--active"></span>
					              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="count__input">
					              <span class="count__min"></span>
				              </div>
			              </div>

				          <div class="product-page__description-price">

					          <?php if ($special) { ?>
						          <div class="product-page__description-price-sale"><span>-<?php echo $percent; ?>%</span>
							          <strike><?php echo $price; ?></strike>
						          </div>

						          <div class="product-page__description-price-normal">
							          <span><?php echo $special; ?></span>
						          </div>
					          <?php } else { ?>
						          <div class="product-page__description-price-normal">
							          <span><?php echo $price; ?></span>
						          </div>
					          <?php } ?>

				          </div>

				          <div>
					          <?php if ($minimum > 1) { ?>
					            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
					          <?php } ?>
				          </div>

				          <div class="product-page__description-add">
					          <button type="button" data-loading-text="<?php echo $text_loading; ?>" class="product__button" onclick="cart.add('<?php echo $product_id; ?>',options,quantity);"><?php echo $button_cart; ?></button>
				          </div>

				          <a onclick="wishlist.add('<?php echo $product_id; ?>');" title="<?php echo $button_wishlist; ?>" class="product-page__description-favorites"></a>
					      <a onclick="compare.add('<?php echo $product_id; ?>');" title="<?php echo $button_compare; ?>" class="product-page__description-compare"></a>
			          </div>

	                    <?php /* ?>
			          <div class="product-page__description-line">
			            <form id="oneClick" class="one-click">
			              <div class="one-click__layout">
			                <input type="text" placeholder="+38(0xx)xxx-xxxx" value="" name="phone" class="one-click__input">
				            <input type="hidden" name="product-id" value="<?php echo $product_id ?>">
			                <input type="submit" value="Заказ в один клик" class="one-click__button">
			              </div>
			            </form>
			          </div>
	                    <?php */ ?>
                    </div>
                </div>

	            <?php echo $column_right ?>

            </div>
        </div>
	</section>

	<div class="self-tabs">
		<div class="self-tabs__inner">
			<div class="self-tabs__wrap">
				<div data-id="tab-description" class="self-tabs__title self-tabs__title--active"><?php echo $tab_description; ?></div>

				<?php if ($attribute_groups) { ?>
					<div data-id="tab-specification" class="self-tabs__title"><?php echo $tab_attribute; ?></div>
				<?php } ?>

				<?php if (!empty($custom_tabs)) { ?>
					<?php foreach($custom_tabs as $custom_tab_kay => $custom_tab){ ?>
						<div data-id="custom-tab-<?php echo $custom_tab_kay; ?>" class="self-tabs__title"><?php echo $custom_tab['name']; ?></div>
					<?php } ?>
				<?php } ?>

				<?php /* ?>
					<div data-id="tab-4" class="self-tabs__title">Кому покупают?<span>(500)</span></div>
				<?php */ ?>

				<?php if ($review_status) { ?>
					<div data-id="tab-review" id="tab-review-link" class="self-tabs__title"><?php echo $tab_review; ?></div>
				<?php } ?>
			</div>
		</div>
		<div id="tab-description" class="self-tabs__content">
			<div class="self-tabs__content-box">
				<?php echo $description; ?>
			</div>

			<?php if ($tags) { ?>
				<div class="container">
					<p><?php echo $text_tags; ?>
						<?php for ($i = 0; $i < count($tags); $i++) { ?>
							<?php if ($i < (count($tags) - 1)) { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
							<?php } else { ?>
								<a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
							<?php } ?>
						<?php } ?>
					</p>
				</div>
			<?php } ?>
		</div>

		<?php if ($attribute_groups) { ?>
			<div id="tab-specification" style="display:none;" class="self-tabs__content">
				<div class="self-tabs__content-box">
					<table class="table table-bordered">
						<?php foreach ($attribute_groups as $attribute_group) { ?>
							<thead>
							<tr>
								<td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
							</tr>
							</thead>
							<tbody>
								<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
								<tr>
									<td><?php echo $attribute['name']; ?></td>
									<td><?php echo $attribute['text']; ?></td>
								</tr>
								<?php } ?>
							</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		<?php } ?>

		<?php if (!empty($custom_tabs)) { ?>
			<?php foreach($custom_tabs as $custom_tab_kay => $custom_tab){ ?>
				<div id="custom-tab-<?php echo $custom_tab_kay; ?>" style="display:none;" class="self-tabs__content">
					<div class="self-tabs__content-box">
						<?php echo $custom_tab['text']; ?>
					</div>
				</div>
			<?php } ?>
		<?php } ?>

		<?php /* ?>
		<div id="tab-4" style="display:none;" class="self-tabs__content">
			<div class="self-tabs__content-box">
				<div class="flex-box-container">
					<div class="box-flex">
						<div class="box-flex__inner">
							<div class="box-flex__img">
								<img src="static/img/content/dog-1.jpg">
							</div>
							<div class="box-flex__content">
								<span class="box-flex__content-title">Мичи</span>
								<span class="box-flex__content-desc">Чихухауа</span>
							</div>
						</div>
					</div>
				</div>
				<div class="pagination-wrap">
					<div class="pagination">
						<a href="#" class="pagination__page-numbers pagination__page-numbers--prev"></a>
						<span class="pagination__page-numbers pagination__page-numbers--current">1</span>
						<a href="#" class="pagination__page-numbers">2</a>
						<a href="#" class="pagination__page-numbers">3</a>
						<a href="#" class="pagination__page-numbers">4</a>
						<!--input(type='text' placeholder='№...').pagination__page-numbers.pagination__page-numbers--number-->
						<a href="#" class="pagination__page-numbers pagination__page-numbers--next"></a>
					</div>
				</div>
			</div>
		</div>
		<?php */ ?>

		<div id="tab-review" style="display:none;" class="self-tabs__content">
			<div class="self-tabs__content-box">
				<div id="review"></div>
			</div>

			<form class="review__leave" id="form-review">
				<div class="review__leave-inner">
					<h3 class="review__leave-title"><?php echo $text_write; ?></h3>
					<div class="review__leave-textarea">
						<textarea name="text" rows="5" id="input-review"></textarea>
						<?php if (!$review_guest) { ?>
							<span><?php echo $text_login; ?></span>
						<?php } ?>
					</div>
					<div class="review-rating review__rating">
						<label class="review-rating__label">
							<input type="radio" name="rating" value="1" class="review-rating__input"><span class="review-rating__span fa fa-stack"><i class="review-rating__icon fa fa-star fa-stack-1x"></i></span>
						</label>
						<label class="review-rating__label">
							<input type="radio" name="rating" value="2" class="review-rating__input"><span class="review-rating__span fa fa-stack"><i class="review-rating__icon fa fa-star fa-stack-1x"></i></span>
						</label>
						<label class="review-rating__label">
							<input type="radio" name="rating" value="3" class="review-rating__input"><span class="review-rating__span fa fa-stack"><i class="review-rating__icon fa fa-star fa-stack-1x"></i></span>
						</label>
						<label class="review-rating__label">
							<input type="radio" name="rating" value="4" class="review-rating__input"><span class="review-rating__span fa fa-stack"><i class="review-rating__icon fa fa-star fa-stack-1x"></i></span>
						</label>
						<label class="review-rating__label">
							<input type="radio" name="rating" value="5" class="review-rating__input"><span class="review-rating__span fa fa-stack"><i class="review-rating__icon fa fa-star fa-stack-1x"></i></span>
						</label>
					</div>
					<div class="review__leave-line">
						<?php if ($review_guest) { ?>
							<input type="hidden" name="name" value="<?php echo $entry_user_name; ?>" id="input-name" />
							<input type="submit" value="<?php echo $text_send; ?>" id="button-review" class="review__leave-button">
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
	</div>

    <?php if ($products) { ?>
		<section class="standard">
			<div class="standard__inner">
				<div class="container">
					<h4 class="main-title"><span class="main-title__text"><?php echo $text_related; ?></span></h4>
					<div class="standard__recommend">
						<div class="swiper-container">
							<div class="swiper-wrapper">
								<?php foreach ($products as $product) { ?>
									<div class="swiper-slide">
										<div class="product">
											<div class="product__layout">
												<?php echo $product['stikers']; ?>
												<div class="product__item">
													<div class="product__img">
														<a href="<?php echo $product['href']; ?>">
															<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
														</a>
													</div>
													<div class="product__content">
														<h3 class="product__caption">
															<a href="<?php echo $product['href']; ?>" class="product__caption-title">
																<?php echo $product['name']; ?>
															</a>
														</h3>
														<div class="product__review">
															<div class="product__rating rating">
																<?php for ($i = 1; $i <= 5; $i++) { ?>
																<?php if ($product['rating'] < $i) { ?>
																<span class="rating__span fa fa-stack">
																			<i class="rating__icon fa fa-star-o fa-stack-1x"></i>
																		</span>
																<?php } else { ?>
																<span class="rating__span fa fa-stack">
																			<i class="rating__icon fa fa-star fa-stack-1x"></i>
																		</span>
																<?php } ?>
																<?php } ?>
															</div><a href="<?php echo $product['href']; ?>#tab-review" class="product__review-count"><?php echo $product['reviews']; ?></a>
														</div>
														<div class="product-grid__weight">
															<div class="product-grid__select"><span class="product-grid__select-current"><?php echo $text_range; ?></span>
																<div class="product-grid__hidden">
																	<ul class="product-grid__hidden-list">
																		<?php foreach ($product['options'] as $option){ ?>
																		<li class="product-grid__hidden-item" id="option-combination-id-<?php echo $option['relatedoptions_id']; ?>">
																			<span class="product-grid__hidden-name"><?php echo $option['name']; ?></span>
																			<div class="product-grid__hidden-cost">
																				<?php if (!empty($option['specials']) || !empty($option['discounts'])) { ?>
																					<?php if (!empty($option['specials'])) { ?>
																						<span class="product-grid__hidden-cost-new"><?php echo $option['specials'][0]['price']; ?></span>
																						<strike class="product-grid__hidden-cost-old"><?php echo $option['price']; ?></strike>
																						<span class="product-grid__hidden-cost-sale">- <?php echo $option['specials'][0]['percent']; ?> %</span>
																					<?php }else{ ?>
																						<span class="product-grid__hidden-cost-new"><?php echo $option['discounts'][0]['price']; ?></span>
																						<strike class="product-grid__hidden-cost-old"><?php echo $option['price']; ?></strike>
																						<span class="product-grid__hidden-cost-sale">- <?php echo $option['discounts'][0]['percent']; ?> %</span>
																					<?php } ?>
																				<?php }else{ ?>
																					<span class="product-grid__hidden-cost-current"><?php echo $option['price']; ?></span>
																				<?php } ?>
																			</div>
																			<?php foreach ($option['options'] as $option_key => $option_value) { ?>
																			<input type="hidden" name="<?php echo $option_key; ?>" value="<?php echo $option_value; ?>">
																			<?php } ?>
																		</li>
																		<?php } ?>
																	</ul>

																</div>
															</div>
														</div>
														<div class="product__cost">
															<?php if($product['special']){ ?>
															<span class="product__cost-price product__cost-price--new"><?php echo $product['special']; ?></span>
															<span class="product__cost-price product__cost-price--old"><?php echo $product['price']; ?></span>
															<span class="product__cost-discount">-<?php echo $product['percent']; ?>%</span>
															<?php } else { ?>
															<span class="product__cost-price product__cost-price--current"><?php echo $product['price']; ?></span>
															<?php } ?>
														</div>
														<div class="product__button-wrap">
															<button type="button" class="product__button" onclick="cart.add('<?php echo $product['product_id']; ?>',options);"><?php echo $button_cart; ?></button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<div class="swiper-button-prev"></div>
						<div class="swiper-button-next"></div>
					</div>
				</div>
			</div>
		</section>
    <?php } ?>

    <?php echo $content_bottom; ?>

	<script type="text/javascript">
		$('.reviews-link').on('click',function (e) {
		    e.preventDefault();

             var elementClick = $(this).attr("href"),
                destination = $(elementClick).offset().top-60;

            jQuery("html:not(:animated),body:not(:animated)").stop().animate({
                scrollTop: destination
            }, 600);

            $('#tab-review-link').trigger('click');
        });
	</script>

	<script type="text/javascript"><!--
	$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
		$.ajax({
			url: 'index.php?route=product/product/getRecurringDescription',
			type: 'post',
			data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
			dataType: 'json',
			beforeSend: function() {
				$('#recurring-description').html('');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				if (json['success']) {
					$('#recurring-description').html(json['success']);
				}
			}
		});
	});
	//--></script>

	<script type="text/javascript"><!--
	$('#review').delegate('.pagination a', 'click', function(e) {
	    e.preventDefault();

	    $('#review').fadeOut('slow');

	    $('#review').load(this.href);

	    $('#review').fadeIn('slow');
	});

	$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

	$('#button-review').on('click', function(e) {
        e.preventDefault();
		$.ajax({
			url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
			type: 'post',
			dataType: 'json',
			data: $("#form-review").serialize(),
			beforeSend: function() {
				$('#button-review').button('loading');
			},
			complete: function() {
				$('#button-review').button('reset');
			},
			success: function(json) {
			    console.log(json);
				$('.alert-success, .alert-danger').remove();

				if (json['error']) {
					$('#form-review').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				}

				if (json['success']) {
					$('#form-review').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                    $('#form-review').hide();

					$('textarea[name=\'text\']').val('');
					$('input[name=\'rating\']:checked').prop('checked', false);
				}
			}
		});
	});

	//--></script>

<?php echo $footer; ?>
