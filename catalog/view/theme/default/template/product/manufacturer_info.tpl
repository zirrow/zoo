<?php echo $header; ?>
<?php echo $content_top_full; ?>

<div class="breadcrumbs">
  <div class="container">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
</div>

<div class="container">
  <?php echo $content_top; ?>
</div>

<section class="catalog">
  <div class="container content_full" id="content">
    <?php echo $column_left; ?>
    <div>

      <?php if ($products) { ?>

      <div class="catalog__top">
        <div class="catalog__top-field">
          <div class="catalog__top-line"><span class="catalog__top-name"><?php echo $text_sort; ?></span>
            <div class="product__weight">
              <select id="input-sort" class="catalog__top-select catalog__top-select--sort" onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="catalog__top-field">
          <div class="catalog__top-line"><span class="catalog__top-name"><?php echo $text_limit; ?></span>
            <div class="product__weight catalog__top-select">
              <select id="input-limit" class="catalog__top-select catalog__top-select--count" onchange="location = this.value;">
                <?php foreach ($limits as $limits) { ?>
                <?php if ($limits['value'] == $limit) { ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="catalog__top-line catalog__top-line--buttons">
            <button class="button-toggle button-toggle--grid" id="grid-view" data-toggle="tooltip" title="<?php echo $button_grid; ?>"></button>
            <button class="button-toggle button-toggle--list" id="list-view" data-toggle="tooltip" title="<?php echo $button_list; ?>"></button>
          </div>
        </div>
      </div>

      <div class="catalog__content-inner">
        <?php foreach ($products as $product) { ?>
        <div class="product-grid">
          <div class="product-grid__layout">
            <?php echo $product['stikers']; ?>
            <div class="product-grid__item">
              <div class="product-grid__img">
                <a href="<?php echo $product['href']; ?>">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
                </a>
              </div>
              <div class="product-grid__content">
                <h3 class="product-grid__caption">
                  <a href="<?php echo $product['href']; ?>" class="product-grid__caption-title">
                    <?php echo $product['name']; ?>
                  </a>
                </h3>

                <div class="product-grid__review">
                  <div class="product-grid__rating rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="rating__span fa fa-stack">
																		<i class="rating__icon fa fa-star-o fa-stack-1x"></i>
																	</span>
                    <?php } else { ?>
                    <span class="rating__span fa fa-stack">
																		<i class="rating__icon fa fa-star fa-stack-1x"></i>
																	</span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <a href="<?php echo $product['href']; ?>#tab-review" class="product__review-count"><?php echo $product['reviews']; ?></a>
                </div>

                <div class="product-grid__weight">
                  <div class="product-grid__select"><span class="product-grid__select-current"><?php echo $text_range; ?></span>
                    <div class="product-grid__hidden">
                      <ul class="product-grid__hidden-list">
                        <?php foreach ($product['options'] as $option){ ?>
                        <li class="product-grid__hidden-item" id="option-combination-id-<?php echo $option['relatedoptions_id']; ?>">
                          <span class="product-grid__hidden-name"><?php echo $option['name']; ?></span>
                          <div class="product-grid__hidden-cost">
                            <?php if (!empty($option['specials'])) { ?>
                            <span class="product-grid__hidden-cost-new"><?php echo $option['specials'][0]['price']; ?></span>
                            <strike class="product-grid__hidden-cost-old"><?php echo $option['price']; ?></strike>
                            <?php if ($option['specials'][0]['percent']){ ?>
                            <span class="product-grid__hidden-cost-sale">- <?php echo $option['specials'][0]['percent']; ?> %</span>
                            <?php } ?>
                            <?php }else{ ?>
                            <span class="product-grid__hidden-cost-current"><?php echo $option['price']; ?></span>
                            <?php } ?>
                          </div>
                          <?php foreach ($option['options'] as $option_key => $option_value) { ?>
                          <input type="hidden" name="<?php echo $option_key; ?>" value="<?php echo $option_value; ?>">
                          <?php } ?>
                        </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="product-grid__cost">
                  <?php if($product['special']){ ?>
                  <span class="product-grid__cost-price product__cost-price--new"><?php echo $product['special']; ?></span>
                  <span class="product-grid__cost-price product__cost-price--old"><?php echo $product['price']; ?></span>
                  <span class="product-grid__cost-discount">-<?php echo $product['percent']; ?>%</span>
                  <?php } else { ?>
                  <span class="product-grid__cost-price product__cost-price--current"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </div>

                <div class="product-grid__button-wrap">
                  <button type="button" class="product-grid__button" onclick="cart.add('<?php echo $product['product_id']; ?>', options ,'<?php echo $product['minimum']; ?>');"><?php echo $button_cart; ?></button>
                  <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="product-page__description-favorites"></a>
                  <a data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="product-page__description-compare"></a>
                </div>

              </div>
            </div>
          </div>
        </div>
        <?php } ?>

        <?php foreach ($products as $product) { ?>
        <div class="product-list">
          <div class="product-list__layout">
            <div class="product-list__item">
              <div class="product-list__left">
                <div class="product-list__img">
                  <?php echo $product['stikers']; ?>
                  <a href="<?php echo $product['href']; ?>">
                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
                  </a>
                </div>

                <div class="product-list__review">
                  <div class="product-list__rating rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="rating__span fa fa-stack">
																		<i class="rating__icon fa fa-star-o fa-stack-1x"></i>
																	</span>
                    <?php } else { ?>
                    <span class="rating__span fa fa-stack">
																		<i class="rating__icon fa fa-star fa-stack-1x"></i>
																	</span>
                    <?php } ?>
                    <?php } ?>
                  </div>
                  <a href="<?php echo $product['href']; ?>#tab-review" class="product-list__review-count"><?php echo $product['reviews']; ?></a>
                </div>
              </div>
              <div class="product-list__content">

                <div class="product-list__title">
                  <h3 class="product-list__caption">
                    <a href="<?php echo $product['href']; ?>" class="product-list__caption-title"><?php echo $product['name']; ?></a>
                  </h3>
                  <div class="product-list__icons">
                    <a data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="product-page__description-favorites"></a>
                    <a data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="product-page__description-compare"></a>
                  </div>
                </div>

                <div class="product-list__content-text">
                  <div class="product-list__content-description">

                    <?php if(!empty($product['description_sale'])) { ?>
                    <p class="product-page__description-red">
                      <?php echo $product['description_sale']; ?>
                    </p>
                    <?php } ?>

                    <p class="product-list__description">
                      <?php echo $product['description']; ?>
                    </p>
                    <a href="<?php echo $product['href']; ?>" class="product-list__read"><?php echo $text_read_more; ?></a>
                  </div>

                  <div class="product-list__content-options">

                    <?php foreach($product['options'] as $option){ ?>
                    <div class="product-list__content-options-line">
                      <div class="product-list__content-option">
                        <span><?php echo $option['name']; ?></span>
                      </div>
                      <div class="product-list__cost">
                        <?php if(!empty($option['specials'])){ ?>
                        <span class="product-list__cost-price product__cost-price--new"><?php echo $option['specials'][0]['price']; ?></span>
                        <span class="product-list__cost-price product__cost-price--old"><?php echo $option['price']; ?></span>
                        <?php } else { ?>
                        <span class="product-list__cost-price product__cost-price--current"><?php echo $option['price']; ?></span>
                        <?php } ?>
                      </div>
                      <script>
                          //Простите меня, идиота. Как сделать по другому я не придумал.
                          var val<?php echo $option['relatedoptions_id']; ?>  = {};
                          <?php foreach($option['options'] as $key => $value){ ?>
                              val<?php echo $option['relatedoptions_id']; ?>[<?php echo $key; ?>] = <?php echo $value; ?>;
                          <?php } ?>
                      </script>
                      <div class="product-list__button-wrap">
                        <button type="button" class="product-list__button" onclick="cart.add('<?php echo $product['product_id']; ?>', val<?php echo $option['relatedoptions_id']; ?> ,'<?php echo $product['minimum']; ?>');"><?php echo $button_cart; ?></button>
                      </div>
                    </div>
                    <?php } ?>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>

      <div class="pagination-wrap">
        <?php echo $pagination; ?>
      </div>
      <?php } ?>

      <?php if (!$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="product__button"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>

    </div>

</section>

<div class="container">
  <?php echo $content_bottom; ?>
</div>

<div class="page__buffer"></div>

<?php echo $column_right; ?>
<?php echo $footer; ?>
