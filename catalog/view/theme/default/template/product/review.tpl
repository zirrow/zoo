<?php if ($reviews) { ?>
	<?php foreach ($reviews as $review) { ?>
		<div class="review__item ">
			<div class="review__item-wrap">
				<div class="review__title"><span class="review__name"><?php echo $review['author']; ?></strong></span>
					<div class="product-page__description-rating rating">
						<?php for ($i = 1; $i <= 5; $i++) { ?>
							<?php if ($review['rating'] < $i) { ?>
								<span class="rating__span fa fa-stack"><i class="rating__icon fa fa-star-o fa-stack-1x"></i></span>
							<?php } else { ?>
								<span class="rating__span fa fa-stack"><i class="rating__icon fa fa-star fa-stack-1x"></i></span>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
					<?php /* ?>
					<div class="review__pets">
						<span class="review__pets-text review__pets-text--active">Мичи (чихуахуа, 1,2 мес.)</span>
						<span class="review__pets-text">Ральф (доберман, 2 года.)</span>
					</div>
					<?php */ ?>
				<p class="review__content"><?php echo $review['text']; ?></p>
				<div class="review__bottom">
					<span class="review__data"><?php echo $review['date_added']; ?></span>
					<?php /* ?>
					<a href="#" class="review__answer">Ответить</a>
					<?php */ ?>
				</div>
			</div>
		</div>
	<?php } ?>
<div class="pagination-wrap">
	<?php echo $pagination; ?>
</div>
<?php } else { ?>
	<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
