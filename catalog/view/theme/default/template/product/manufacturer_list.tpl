<?php echo $header; ?>


<?php echo $column_left; ?>
	<?php echo $content_top_full; ?>
		<?php echo $content_top; ?>

		<div class="breadcrumbs">
			<div class="container">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<a href="<?php echo $breadcrumb['href']; ?>"> <?php echo $breadcrumb['text']; ?> </a>
				<?php } ?>
			</div>
		</div>

		<div class="brands">
			<div class="container">
				<h1><?php echo $heading_title; ?></h1>


				<?php if ($categories) { ?>

					<div class="brands__title">
						<strong><?php echo $text_index; ?></strong>
						<?php foreach ($categories as $category) { ?>
							<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
						<?php } ?>
					</div>

					<div class="brands__list">
						<?php foreach ($categories as $category) { ?>
							<span id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></span>
							<?php if ($category['manufacturer']) { ?>
								<div class="brands__item">
									<?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
											<?php foreach ($manufacturers as $manufacturer) { ?>
												<a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
											<?php } ?>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="page__buffer"></div>


<?php echo $content_bottom_full; ?>
<?php echo $content_bottom; ?>
<?php echo $column_right; ?>

<?php echo $footer; ?>