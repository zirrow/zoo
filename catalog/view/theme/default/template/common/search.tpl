<div id="search" class="header__search">
	<div class="header__search-inner">
		<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>">
		<button type="button"><i class="fa fa-search"></i></button>
	</div>
</div>