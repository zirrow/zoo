<?php echo $header; ?>

<section class="cart">
  <form class="cart-form">
    <div id="content" class="container">

    <?php echo $column_left; ?>

    <h2 class="cart-title">
      <?php echo $heading_title; ?>
    </h2>

    <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
      <div class="cart__inner">