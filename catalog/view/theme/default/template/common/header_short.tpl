<div class="header__middle">
    <div class="container">
        <div class="header__middle-inner">

            <div class="header__phones header__phones--one">
                <ul class="header__phones-list">
                    <li>
                        <span class="header__phones-link">
                            <a href="tel:<?php echo $telephone; ?>">
                                <?php echo $telephone; ?>
                            </a>
                        </span>
                    </li>

                    <?php if ($telephone2) { ?>
                        <li>
                            <span class="header__phones-link">
                                <a href="tel:<?php echo $telephone2; ?>">
                                    <?php echo $telephone2; ?>
                                </a>
                            </span>
                        </li>
                    <?php } ?>

                </ul>

                <div class="header__phones-buttons">
                    <a href="<?php echo $contact; ?>"><?php echo $text_schedule; ?></a>
                </div>

            </div>

            <div class="header__logo logo" id="logo">
                <?php if ($logo) { ?>
                    <?php if ($home == $og_url) { ?>
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
                    <?php } else { ?>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
                    <?php } ?>
                <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>

            <div class="header__phones header__phones--two">
                <ul class="header__phones-list">
                    <?php if ($telephone3) { ?>
                        <li>
                            <span class="header__phones-link">
                                <a href="tel:<?php echo $telephone3; ?>">
                                    <?php echo $telephone3; ?>
                                </a>
                            </span>
                        </li>
                    <?php } ?>

                    <?php if ($telephone4) { ?>
                        <li>
                            <span class="header__phones-link">
                                <a href="tel:<?php echo $telephone4; ?>">
                                    <?php echo $telephone4; ?>
                                </a>
                            </span>
                        </li>
                    <?php } ?>

                </ul>

                <div class="header__phones-buttons">
                    <?php echo $callback; ?>
                </div>

            </div>

            <button type="button" class="hamburger hamburger--emphatic">
                <span class="hamburger-box">
                    <span class="hamburger-inner">
                    </span>
                </span>
            </button>
        </div>
    </div>
</div>