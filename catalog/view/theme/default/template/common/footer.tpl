  <div class="page__buffer"></div>
</section>

<div class="page__footer">
  <?php if(!$logged){ ?>
    <section class="subscribe">
      <div class="container">
        <div class="subscribe__inner">
          <div class="subscribe__content">
            <?php echo $text_subscription_title; ?>
          </div>
          <form id="subscribe" class="subscribe__form">
            <div class="subscribe__field">
              <input type="email" placeholder="Введите Ваш e-mail" name="Newsletter_email">
            </div>
            <input type="button" id="Newsletter" value="Да">
          </form>
        </div>
      </div>
    </section>
  <?php } ?>
  <footer class="footer">
    <div class="footer__middle">
      <div class="container">
        <div class="footer__inner">
          <div class="footer__nav">
            <h4><?php echo $text_extra; ?></h4>
            <ul>
	            <?php if (!empty($categories)){ ?>
	              <?php foreach($categories as $categorie){ ?>
	                <li>
	                  <a href="<?php echo $categorie['href']; ?>" class="footer__link">
	                    <?php echo $categorie['name']; ?>
	                  </a>
	                </li>
	                <?php foreach($categorie['children'] as $children){ ?>
	                  <a href="<?php echo $children['href']; ?>" class="footer__link">
	                    <?php echo $children['name']; ?>
	                  </a>
	                <?php } ?>
	              <?php } ?>
	            <?php } ?>
	            <li><a href="<?php echo $manufacturer; ?>" class="footer__link"><?php echo $text_manufacturer; ?></a></li>
            </ul>
          </div>
          <div class="footer__nav">
            <h4><?php echo $text_information; ?></h4>
            <ul>
              <?php foreach($informations as $information){ ?>
                <li>
                  <a href="<?php echo $information['href']; ?>" class="footer__link">
                    <?php echo $information['title']; ?>
                  </a>
                </li>
              <?php } ?>
              <li><a href="<?php echo $special; ?>" class="footer__link"><?php echo $text_sale; ?></a></li>
              <li><a href="<?php echo $contact; ?>" class="footer__link"><?php echo $text_contact; ?></a></li>
            </ul>
          </div>
          <div class="footer__socials">
            <h4><?php echo $text_service; ?></h4>

            <ul>
              <?php if($config_vk){ ?>
                <li>
                  <a href="<?php echo $config_vk; ?>" class="footer__socials-link footer__socials-link--vk"></a>
                </li>
              <?php } ?>
              <?php if($config_fb){ ?>
                <li>
                  <a href="<?php echo $config_fb; ?>" class="footer__socials-link footer__socials-link--facebook"></a>
                </li>
              <?php } ?>
              <?php if($config_yt){ ?>
                <li>
                  <a href="<?php echo $config_yt; ?>" class="footer__socials-link footer__socials-link--youtube"></a>
                </li>
              <?php } ?>
              <?php if($config_g){ ?>
                <li>
                  <a href="<?php echo $config_g; ?>" class="footer__socials-link footer__socials-link--google"></a>
                </li>
              <?php } ?>
              <?php if($config_tw){ ?>
                <li>
                  <a href="<?php echo $config_tw; ?>" class="footer__socials-link footer__socials-link--instagram"></a>
                </li>
              <?php } ?>
            </ul>
            <a href="#" class="footer__socials-button"><?php echo $text_review; ?></a>
              <div class="callback-popup-btn"><?php echo $callback; ?></div>

          </div>
          <div class="footer__info">
            <h4><?php echo $text_contact;?></h4>
            <ul>
              <li><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a>
              </li>
              <li><a href="tel:<?php echo $telephone2; ?>"><?php echo $telephone2; ?></a>
              </li>
              <li><a href="tel:<?php echo $telephone3; ?>"><?php echo $telephone3; ?></a>
              </li>
              <li><a href="tel:<?php echo $telephone4; ?>"><?php echo $telephone4; ?></a>
              </li>
              <?php echo $open; ?>
              <li><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer__bottom">
      <div class="container">
        <p><?php echo $powered; ?></p>
      </div>
    </div>
  </footer>
</div>

<script type="text/javascript"><!--
      $('#Newsletter').on('click', function() {

          $.ajax({
              url: 'index.php?route=account/edit/OnlyNewsletter',
              type: 'post',
              data: $('input[name=\'Newsletter_email\']'),
              dataType: 'json',

              success: function(json) {
                  if(!json['error_mail'])
                  {
                      $('.subscribe__inner').html('<div class="subscribe__content"><h4><?php echo $text_newsletter_ok; ?></h4></div>');
                      //$('.footer__subscribe').html('');
                  }else{
                      var html = '<label id="Newsletter_email-error" class="error" for="Newsletter_email">'+json['error_mail']+'</label>';
                      $('.subscribe__field [type="email"]').addClass('error');
                      $('.subscribe__field [type="email"]').after(html);
                  }

              },
              error: function(xhr, ajaxOptions, thrownError) {
                  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
              }
          });
      });
//--></script>

  <script src="catalog/view/javascript/main.min.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
  <script src="catalog/view/javascript/separate-js/common.js" type="text/javascript"></script>


</body></html>