<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
	<base href="<?php echo $base; ?>" />

	<?php if ($description) { ?>
		<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<?php } ?>

	<?php if ($keywords) { ?>
		<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>

	<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo $og_url; ?>" />

	<?php if ($og_image) { ?>
		<meta property="og:image" content="<?php echo $og_image; ?>" />
	<?php } else { ?>
		<meta property="og:image" content="<?php echo $logo; ?>" />
	<?php } ?>

	<meta property="og:site_name" content="<?php echo $name; ?>" />

	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<script src="catalog/view/javascript/jquery/easyzoom/easyzoom.js" type="text/javascript"></script>
	<script>
        (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
	</script>

	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
	<link href="//fonts.googleapis.com/css?family=Roboto:400,400i,700&amp;amp;subset=cyrillic" rel="stylesheet">

	<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
	<link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">


	<?php foreach ($styles as $style) { ?>
		<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>

	<?php foreach ($links as $link) { ?>
		<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<?php foreach ($scripts as $script) { ?>
		<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>

	<?php foreach ($analytics as $analytic) { ?>
		<?php echo $analytic; ?>
	<?php } ?>

</head>


<body class="<?php echo (isset($class) && $class ? $class : '') ?> page header__<?php echo $header_type ?>">
	<nav id="top" class="navigation">
	  <div class="container">

		  <div class="navigation__inner">
			  <nav class="navigation__menu">
				  <ul class="navigation__list">
					  <?php foreach ($informations as $information) { ?>
					    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
					  <?php } ?>
					  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
					  <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
				  </ul>
			  </nav>
			  <nav class="navigation__nav">
				  <ul class="navigation__list navigation__list--icons">
					  <?php echo $currency; ?>
					  <?php echo $language; ?>
					  <li><a href="<?php echo $compare; ?>" class="navigation__link navigation__link--compare" id="compare-total"><?php echo $text_compare; ?></a></li>
					  <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" class="navigation__link navigation__link--favorites" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a></li>

					  <?php if ($logged) { ?>
					    <li><a href="<?php echo $account; ?>" class="navigation__link navigation__link--enter"><?php echo $user_name; ?><?php echo $user_discount; ?></a></li>
					  <?php }else{ ?>
						  <li><a href="<?php echo $register; ?>" class="navigation__link navigation__link--registration"><?php echo $text_register; ?></a></li>
						  <li><a href="javascript:;" data-fancybox="" data-src="#personal" class="navigation__link navigation__link--login"><?php echo $text_login; ?></a></li>
					  <?php } ?>
				  </ul>
			  </nav>
		  </div>

		  <div id="personal" style="display: none;" class="personal">
			  <h3 class="personal__title"><?php echo $text_enter; ?></h3>
			  <form method="post" action="<?php echo $action ?>" class="personal__form">
				  <div class="personal__field">
					  <input type="text" placeholder="<?php echo $text_login; ?>" name="email">
				  </div>
				  <div class="personal__field">
					  <input type="password" placeholder="<?php echo $text_pass; ?>" name="password">
				  </div>
				  <div class="personal__reg">
					  <a href="<?php echo $forgotten; ?>"><?php echo $text_forgot; ?></a>
					  <a href="<?php echo $register; ?>"><?php echo $text_reg; ?></a>
				  </div>
				  <input type="submit" value="<?php echo $button_enter; ?>">
			  </form>
			  <p><?php echo $text_social; ?></p>

			  <div class="personal__social">
				  <ul>
					  <li>
				        <?php echo $ulogin; ?>
					  <li>

					  <!--
					  <li>
						  <a href="#" class="personal__social-link personal__social-link--vk"></a>
					  </li>
					  <li>
						  <a href="#" class="personal__social-link personal__social-link--facebook"></a>
					  </li>
					  <li>
						  <a href="#" class="personal__social-link personal__social-link--youtube"></a>
					  </li>
					  <li>
						  <a href="#" class="personal__social-link personal__social-link--google"></a>
					  </li>
					  <li>
						  <a href="#" class="personal__social-link personal__social-link--insta"></a>
					  </li>
					  -->

				  </ul>
			  </div>

		  </div>
	  </div>
	</nav>

	<div class="header">
		<?php if ($header_type && $header_type === 'short') { ?>
			<?php include DIR_TEMPLATE . 'default/template/common/header_short.tpl'; ?>
		<?php } else { ?>
			<?php include DIR_TEMPLATE . 'default/template/common/header_full.tpl'; ?>
		<?php } ?>

		<?php if ($categories) { ?>
			<div class="header__bottom">
				<div class="container">
					<nav class="header__menu">
						<ul class="header__menu-root">

							<?php foreach ($categories as $category) { ?>
								<?php if ($category['children']) { ?>
									<li>

										<a href="<?php echo $category['href']; ?>" class="header__link">
											<?php echo $category['name']; ?>
										</a>

										<?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
											<ul class="header__menu-dropdown">
												<?php foreach ($children as $child) { ?>

													<?php if ($child['children']) { ?>
															<li>
																<a href="<?php echo $child['href']; ?>">
																	<?php echo $child['name']; ?>
																</a>

																<ul class="header__menu-dropdown-second">
																	<?php foreach ($child['children'] as $child_2) { ?>
																		<li><a href="<?php echo $child_2['href']; ?>"><?php echo $child_2['name']; ?></a></li>
																	<?php } ?>
																</ul>
															</li>
													<?php } else { ?>
														<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
													<?php } ?>

												<?php } ?>
											</ul>
										<?php } ?>

									</li>
								<?php } else { ?>
									<li><a href="<?php echo $category['href']; ?>" class="header__link"><?php echo $category['name']; ?></a></li>
								<?php } ?>
							<?php } ?>

						</ul>
					</nav>
				</div>
			</div>

			<div class="side-navigation">
				<div class="side-navigation__inner">
					<ul class="side-navigation__top">
						<?php if ($logo) { ?>
							<?php if ($home == $og_url) { ?>
								<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
							<?php } else { ?>
								<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
							<?php } ?>
						<?php } else { ?>
							<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
						<?php } ?>
						<a class="side-navigation__close">
							<span class="is-close"></span>
						</a>
					</ul>

					<ul class="side-navigation__main-menu">

						<li class="side-navigation__main-item">
							<a href="<?php echo $home; ?>" class="side-navigation__main-link"><?php echo $text_home; ?></a>
						</li>


						<?php foreach ($categories as $category_mob) { ?>
							<?php if ($category_mob['children']) { ?>
								<li class="side-navigation__main-item side-navigation__main-item-children">
									<div class="side-navigation__arrow"></div>
									<a href="<?php echo $category_mob['href']; ?>" class="side-navigation__main-link">
										<?php echo $category_mob['name']; ?>
									</a>
									<?php foreach ($category_mob['children'] as $children_mob) { ?>
										<ul class="side-navigation__sub-nav">
											<?php if ($children_mob['children']) { ?>
												<li class="side-navigation__sub-item">
													<a href="<?php echo $children_mob['href']; ?>" class="side-navigation__sub-link">
														<?php echo $children_mob['name']; ?>
													</a>

													<ul class="side-navigation__sub-nav-second">
														<?php foreach ($children_mob['children'] as $child_2) { ?>
															<li class="side-navigation__sub-item-second">
																<a href="<?php echo $child_2['href']; ?>" class="side-navigation__sub-link-second"><?php echo $child_2['name']; ?></a>
															</li>
														<?php } ?>
													</ul>
												</li>
											<?php } else { ?>
												<li class="side-navigation__sub-item">
													<a href="<?php echo $children_mob['href']; ?>" class="side-navigation__sub-link"><?php echo $children_mob['name']; ?></a>
												</li>
											<?php } ?>
										</ul>
									<?php } ?>
								</li>
							<?php } else { ?>
								<li class="side-navigation__main-item side-navigation__main-item-children">
									<a href="<?php echo $category_mob['href']; ?>" class="side-navigation__main-link"><?php echo $category_mob['name']; ?></a>
								</li>
							<?php } ?>
						<?php } ?>


						<?php foreach ($informations as $information) { ?>
							<li class="side-navigation__main-item side-navigation__main-item-last">
								<a href="<?php echo $information['href']; ?>" class="side-navigation__main-link"><?php echo $information['title']; ?></a>
							</li>
						<?php } ?>
						<li class="side-navigation__main-item side-navigation__main-item-last">
							<a href="<?php echo $special; ?>" class="side-navigation__main-link"><?php echo $text_special; ?></a>
						</li>
						<li class="side-navigation__main-item side-navigation__main-item-last">
							<a href="<?php echo $contact; ?>" class="side-navigation__main-link"><?php echo $text_contact; ?></a>
						</li>

					</ul>
				</div>
			</div>
		<?php } ?>
	</div>

	<section class="page__wrapper">