<div id="cart" class="header__cart">
	<button data-toggle="dropdown" type="button" class="dropdown-toggle">
		<span class="header__cart-icon"></span>
		<span id="cart-cost"><?php echo $text_total; ?></span>
		<span id="cart-total"><?php echo $text_items; ?></span>
		<span id="cart-discount"><?php echo $cart_user_discount; ?></span>
	</button>
	<ul class="dropdown-menu">
		<li>
			<h4 class="dropdown-menu-title"><?php echo $text_cart_name; ?></h4>
		</li>
		<li>
			<?php if ($products || $vouchers) { ?>
				<div class="dropdown-menu__cart-wrap">
					<?php foreach ($products as $product) { ?>

						<div class="item-cart">
							<div class="item-cart__inner">
								<div class="item-cart__img">
									<a href="<?php echo $product['href']; ?>">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
									</a>
								</div>
								<div class="item-cart__content">
									<div class="item-cart__title">
										<a href="<?php echo $product['href']; ?>" >
											<span class="item-cart__title-text"><?php echo $product['name']; ?></span>
										</a>
									</div>
									<div class="item-cart__article">
										<?php if ($product['option']) { ?>
											<?php foreach ($product['option'] as $option) { ?>
												<span class="item-cart__article-pack"><?php echo $option['name']; ?> <?php echo $option['value']; ?></span>
											<?php } ?>
										<?php } ?>

										<?php if ($product['recurring']) { ?>
											<small>
												<?php echo $text_recurring; ?> <?php echo $product['recurring']; ?>
											</small>
										<?php } ?>
										<span class="item-cart__article-text"><?php echo $product['sku']; ?></span>
									</div>
									<div class="item-cart__favorites">
										<span onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></span>
									</div>
									<div class="item-cart__count">
										<div class="count count--alt">
											<span class="count__max count__max--active" onclick="cart.update(<?php echo $product['cart_id']?>,Number($('#cart_quantity').val())+Number(1))"></span>
											<input type="text" id="cart_quantity" name="quantity" value="<?php echo $product['quantity']; ?>" class="count__input">
											<span class="count__min" onclick="cart.update(<?php echo $product['cart_id']?>,Number($('#cart_quantity').val())-Number(1))"></span>
										</div>
									</div>

									<?php if($product['price_old']){ ?>
										<div class="item-cart__cost item-cart__cost--two">
											<div class="item-cart__cost-inner">
												<span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['total']; ?></span>
												<span class="item-cart__cost-price item-cart__cost-price--old"><?php echo $product['price_old']; ?></span>
											</div>
											<span class="item-cart__cost-discount">-<?php echo $product['percent']; ?>%</span>
										</div>
									<?php } else { ?>
										<div class="item-cart__cost item-cart__cost--two">
											<div class="item-cart__cost-inner">
												<span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['total']; ?></span>
											</div>
										</div>
									<?php } ?>

								</div>
								<div class="item-cart__close">
									<button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="item-cart__close-button">
									</button>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>

				<div class="dropdown-menu__total-wrap">
					<span>Сумма заказа:<span><?php echo $total; ?></span></span>
					<strike><?php echo $total_old; ?></strike>
				</div>

				<div class="dropdown-menu__order-wrap">
					<a href="<?php echo $checkout; ?>" class="dropdown-menu__order-link"><?php echo $text_checkout; ?></a>
					<a href="<?php echo $cart; ?>" class="cart__order-link"><?php echo $text_cart; ?></a>
				</div>

				<div class="dropdown-menu__one-click">
					<span><?php echo $text_one_click?></span>
					<form id="oneClick" class="one-click">
						<div class="one-click__layout">
							<input type="text" placeholder="+38(0xx)xxx-xxxx" name="one_click_cart_phone" class="one-click__input">
							<input type="submit" value="<?php echo $text_submit_one_click?>" class="one-click__button">
						</div>
					</form>
				</div>


			<?php } else { ?>
				<div class="item-cart">
					<p class="text-center"><?php echo $text_empty; ?></p>
				</div>
			<?php } ?>
		</li>

		<div class="dropdown-menu__close">
			<button class="dropdown-menu__close-button"></button>
		</div>
	</ul>
</div>


<script type="text/javascript"><!--

        $(document).on("submit", "#oneClick", function(){
        event.preventDefault();

        var telephon = $('[name="one_click_cart_phone"]').val();
        if(telephon[telephon.length-1] && telephon[telephon.length-1]!='_')
        {

            $.ajax({
                url: 'index.php?route=module/one_click_cart/BuyOneClick',
                type: 'post',
                dataType: 'json',
                data: $("[name='one_click_cart_phone']"),

                success: function(json) {

                    if(json['error'])
                    {
                        $('.dropdown-menu__one-click span').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }else{
                        var html = '<div class="item-cart">';
                        html +=    	'<p class="text-center"><?php echo $text_empty; ?></p>';
                        html +=    '</div>';
                        $('.dropdown-menu__one-click').parent().html(html);

                        alert(json['success']);
                    }
                },

                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }else{
            $('.dropdown-menu__one-click span').html('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + 'Телефон не введён!' + '</div>');
        }
    });
    //--></script>



















<?php /* ?>
<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-inverse btn-block btn-lg dropdown-toggle">
	  <i class="fa fa-shopping-cart"></i> <span id="cart-total"><?php echo $text_items; ?></span>
  </button>

  <ul class="dropdown-menu pull-right">
    <?php if ($products || $vouchers) { ?>
	    <li>
	      <table class="table table-striped">
	        <?php foreach ($products as $product) { ?>
		        <tr>
		          <td class="text-center">
			          <?php if ($product['thumb']) { ?>
		                <a href="<?php echo $product['href']; ?>">
			                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
		                </a>
		              <?php } ?>
		          </td>
		          <td class="text-left">
			          <a href="<?php echo $product['href']; ?>">
				          <?php echo $product['name']; ?>
			          </a>

		                <?php if ($product['option']) { ?>
		                    <?php foreach ($product['option'] as $option) { ?>
		                        <br />
		                        -
			                    <small>
				                    <?php echo $option['name']; ?> <?php echo $option['value']; ?>
			                    </small>
		                    <?php } ?>
		                <?php } ?>

		                <?php if ($product['recurring']) { ?>
		                    <br />
		                    -
			                <small>
				                <?php echo $text_recurring; ?> <?php echo $product['recurring']; ?>
			                </small>
		                <?php } ?>
		          </td>
		          <td class="text-right">x <?php echo $product['quantity']; ?></td>
		          <td class="text-right"><?php echo $product['total']; ?></td>
		          <td class="text-center">
			          <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs">
				          <i class="fa fa-times"></i>
			          </button>
		          </td>
		        </tr>
	        <?php } ?>

	        <?php foreach ($vouchers as $voucher) { ?>
		        <tr>
		          <td class="text-center"></td>
		          <td class="text-left"><?php echo $voucher['description']; ?></td>
		          <td class="text-right">x&nbsp;1</td>
		          <td class="text-right"><?php echo $voucher['amount']; ?></td>
		          <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
		        </tr>
	        <?php } ?>

	      </table>
	    </li>
	    <li>
	      <div>
	        <table class="table table-bordered">
	          <?php foreach ($totals as $total) { ?>
		          <tr>
		            <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
		            <td class="text-right"><?php echo $total['text']; ?></td>
		          </tr>
	          <?php } ?>
	        </table>
	        <p class="text-right">
		        <a href="<?php echo $cart; ?>">
			        <strong>
				        <i class="fa fa-shopping-cart"></i> <?php echo $text_cart; ?>
			        </strong>
		        </a>&nbsp;&nbsp;&nbsp;
		        <a href="<?php echo $checkout; ?>">
			        <strong>
				        <i class="fa fa-share"></i> <?php echo $text_checkout; ?>
			        </strong>
		        </a>
	        </p>
	      </div>
	    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>
<?php */ ?>