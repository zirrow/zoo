<section class="top-slider">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php foreach ($banners as $banner) { ?>
          <div style="background-image: url(<?php echo $banner['image']; ?>); <?php echo $banner['style']; ?>" class="swiper-slide">
            <?php if ($banner['link']) { ?>
              <a href="<?php echo $banner['link']; ?>">
            <?php } ?>
              <div class="top-slider__flex">
                <div class="top-slider__flex-center">
                  <div class="container">
                    <div class="top-slider__flex-composition">
                      <div class="top-slider__flex-composition-content">
                        <h2><span><?php echo $banner['title']; ?></span></h2>
                        <p><?php echo $banner['text']; ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php if ($banner['link']) { ?>
              </a>
            <?php } ?>
          </div>
      <?php } ?>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-pagination"></div>
  </div>
</section>
<!--
background-color: #000; background-repeat: no-repeat; background-size: cover;
-->


