
<a href="javascript:;" data-fancybox="" data-src="#callback-popup-<?php echo $position; ?>" ><?php echo $button_send; ?></a>

<div id="callback-popup-<?php echo $position; ?>" style="display: none;" class="callback-popup popup">
	<h3 class="callback-popup__title"><?php echo $heading_title; ?></h3>
	<div class="error-box"></div>
	<form action="<?php echo $action; ?>" method="post" class="callback-popup__form callback-head-form" id="callback-head-form-<?php echo $position; ?>">
		<div class="callback-popup__field">
			<input type="text" placeholder="<?php echo $text_holder_name; ?>" name="name" id="name">
		</div>
		<div class="callback-popup__field">
			<input type="text" placeholder="<?php echo $text_holder_tel; ?>" name="tel" id="tel">
		</div>
		<div class="callback-popup__field"><span><?php echo $text_question; ?></span>
			<textarea placeholder="<?php echo $text_question; ?>" name="text" id="text"></textarea>
		</div>
		<div class="callback-popup__field">
			<input type="hidden" name="link" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
			<input type="submit" id="callback-head-submit-<?php echo $position; ?>" value="<?php echo $button_send; ?>">
		</div>
	</form>
</div>


<script type="text/javascript">

	$('#callback-head-form-<?php echo $position; ?>').submit(function(e){

	    e.preventDefault();
	    var $form = $(this);
	    if(! $form.valid()) return false;

        $.ajax({
            type: 'POST',
            url: '<?php echo $action; ?>',
            data: $("#callback-head-form-<?php echo $position; ?>").serialize(),
            dataType: 'json',
            success: function(data) {
                if (data['error']) {
                    if (data['error']['name']) {
                        $('#callback-head-form-<?php echo $position; ?> input[name=\'name\']').addClass('error');
                        $('#callback-head-form-<?php echo $position; ?> input[name=\'name\']').after('<label id=\'name-error\' class=\'error\' for=\'name\'>'+data['error']['name']+'</label>');
                    }
                    if (data['error']['tel']) {
                        $('#callback-head-form-<?php echo $position; ?> input[name=\'tel\']').addClass('error');
                        $('#callback-head-form-<?php echo $position; ?> input[name=\'tel\']').after('<label id=\'phone-error\' class=\'error\' for=\'tel\'>'+data['error']['tel']+'</label>');
                    }
                } else {
                    $('#callback-head-form-<?php echo $position; ?>').hide();
                    $('.error-box').html(data['success']).css('color','#66ca3b');
                }

            }
        });
	});

</script>