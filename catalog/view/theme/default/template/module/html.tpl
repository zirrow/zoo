<?php if(!$wrapper) { ?>
	<?php if($heading_title) { ?>
		<h4 class="main-title"><span class="main-title__text"><?php echo $heading_title; ?></span></h4>
	<?php } ?>
	<?php echo $html; ?>
<?php } else { ?>
	<section class="standard">
		<div class="container">
		  <?php if($heading_title) { ?>
			<h4 class="main-title"><span class="main-title__text"><?php echo $heading_title; ?></span></h4>
		  <?php } ?>
		  <?php echo $html; ?>
		</div>
	</section>
<?php } ?>