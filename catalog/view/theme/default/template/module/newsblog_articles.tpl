<section class="standard">
    <div class="standard__inner">
        <div class="container">
            <?php if ($link_to_category) { ?>
                <a href="<?php echo $link_to_category; ?>">
            <?php } ?>
                <?php if ($heading_title) { ?>
                    <h4 class="main-title"><span class="main-title__text"><?php echo $heading_title; ?></span></h4>
                <?php } ?>
            <?php if ($link_to_category) { ?>
                </a>
            <?php } ?>
            <?php if ($html) { ?>
                <?php echo $html; ?>
            <?php } ?>

            <div class="standard__discount">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        <?php foreach ($articles as $article) { ?>
                            <div class="swiper-slide">
                                <div class="product">
                                    <div class="product__layout">
                                        <div class="product__item">
                                            <?php if ($article['thumb']) { ?>
                                                <div class="product__img">
                                                    <a href="<?php echo $article['href']; ?>">
                                                        <img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['title']; ?>" title="<?php echo $article['name']; ?>" />
                                                    </a>
                                                </div>
                                            <?php } ?>

                                            <div class="product__content">
                                                <h3 class="product__caption">
                                                    <a href="<?php echo $article['href']; ?>" class="product__caption-title">
                                                        <?php echo $article['name']; ?>
                                                    </a>
                                                </h3>

                                                <?php echo $article['preview']; ?>
                                            </div>

                                            <div class="product__button-wrap">
                                                <button type="button" class="news__module__button" onclick="location.href = ('<?php echo $article['href']; ?>');"><?php echo $text_more; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>

<!--
              <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="product-thumb transition">
                  <?php if ($article['thumb']) { ?>
                  <div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['title']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
                  <?php } ?>
                  <div class="caption">
                    <h4><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
                    <?php echo $article['preview']; ?>
                  </div>
                  <div class="button-group">
                    <button onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_more; ?></span></button>
                    <?php if ($article['date']) { ?><button type="button" data-toggle="tooltip" title="<?php echo $article['date']; ?>"><i class="fa fa-clock-o"></i></button><?php } ?>
                    <button type="button" data-toggle="tooltip" title="<?php echo $article['viewed']; ?>"><i class="fa fa-eye"></i></button>
                  </div>
                </div>
              </div>
            </div>-->

        </div>
    </div>
</section>