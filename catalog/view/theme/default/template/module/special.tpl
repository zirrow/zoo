<section class="standard">
	<div class="standard__inner">
		<div class="container">
			<h4 class="main-title"><span class="main-title__text"><?php echo $heading_title; ?></span></h4>
			<div class="standard__discount">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						<?php foreach ($products as $product) { ?>
							<div class="swiper-slide">
								<div class="product">
									<div class="product__layout">
										<?php echo $product['stikers']; ?>
										<div class="product__item">
											<div class="product__img">
												<a href="<?php echo $product['href']; ?>">
													<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
												</a>
											</div>
											<div class="product__content">
												<h3 class="product__caption">
													<a href="<?php echo $product['href']; ?>" class="product__caption-title">
														<?php echo $product['name']; ?>
													</a>
												</h3>
												<div class="product__review">
													<div class="product__rating rating">
														<?php for ($i = 1; $i <= 5; $i++) { ?>
															<?php if ($product['rating'] < $i) { ?>
																<span class="rating__span fa fa-stack">
																	<i class="rating__icon fa fa-star-o fa-stack-1x"></i>
																</span>
															<?php } else { ?>
																<span class="rating__span fa fa-stack">
																	<i class="rating__icon fa fa-star fa-stack-1x"></i>
																</span>
															<?php } ?>
														<?php } ?>
													</div><a href="<?php echo $product['href']; ?>#tab-review" class="product__review-count"><?php echo $product['reviews']; ?></a>
												</div>
												<div class="product-grid__weight">
													<div class="product-grid__select"><span class="product-grid__select-current"><?php echo $text_range; ?></span>
														<div class="product-grid__hidden">
															<ul class="product-grid__hidden-list">
																<?php foreach ($product['options'] as $option){ ?>
																	<li class="product-grid__hidden-item" id="option-combination-id-<?php echo $option['relatedoptions_id']; ?>">
																		<span class="product-grid__hidden-name"><?php echo $option['name']; ?></span>
																		<div class="product-grid__hidden-cost">

																			<?php if (!empty($option['specials']) || !empty($option['discounts'])) { ?>
																			<?php if (!empty($option['specials'])) { ?>
																			<span class="product-grid__hidden-cost-new"><?php echo $option['specials'][0]['price']; ?></span>
																			<strike class="product-grid__hidden-cost-old"><?php echo $option['price']; ?></strike>
																			<span class="product-grid__hidden-cost-sale">- <?php echo $option['specials'][0]['percent']; ?> %</span>
																			<?php }else{ ?>
																			<span class="product-grid__hidden-cost-new"><?php echo $option['discounts'][0]['price']; ?></span>
																			<strike class="product-grid__hidden-cost-old"><?php echo $option['price']; ?></strike>
																			<span class="product-grid__hidden-cost-sale">- <?php echo $option['discounts'][0]['percent']; ?> %</span>
																			<?php } ?>
																			<?php }else{ ?>
																			<span class="product-grid__hidden-cost-current"><?php echo $option['price']; ?></span>
																			<?php } ?>

																		</div>
																		<?php foreach ($option['options'] as $option_key => $option_value) { ?>
																			<input type="hidden" name="<?php echo $option_key; ?>" value="<?php echo $option_value; ?>">
																		<?php } ?>
																	</li>
																<?php } ?>
															</ul>

														</div>
													</div>
												</div>
												<div class="product__cost">
													<?php if($product['special']){ ?>
														<span class="product__cost-price product__cost-price--new"><?php echo $product['special']; ?></span>
														<span class="product__cost-price product__cost-price--old"><?php echo $product['price']; ?></span>
														<span class="product__cost-discount">-<?php echo $product['percent']; ?>%</span>
													<?php } else { ?>
														<span class="product__cost-price product__cost-price--current"><?php echo $product['price']; ?></span>
													<?php } ?>
												</div>
												<div class="product__button-wrap">
													<button type="button" class="product__button" onclick="cart.add('<?php echo $product['product_id']; ?>',options);"><?php echo $button_cart; ?></button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-button-next"></div>
			</div>
		</div>
	</div>
</section>