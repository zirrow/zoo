<?php if ($options || $show_price) { ?>

	<button class="aside-hide">Фильтр</button>
	<aside class="aside inner-box__aside" id="ocfilter">
		<div class="aside__filters" id="ocfilter-content">
			<div class="aside__title">
				<h2><?php echo $heading_title; ?></h2>
			</div>
			<div class="hidden" id="ocfilter-button">
				<button class="btn btn-primary disabled" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Загрузка.."></button>
			</div>

			<?php if ($selecteds) { # Selected options ?>
				<div class="aside__filters-content">
					<?php foreach ($selecteds as $option) { ?>
						<?php foreach ($option['values'] as $value) { ?>
							<div class="aside__filters-field">
								<a href="<?php echo $value['href']; ?>" class="aside__filters-line-close"></a>
								<span class="aside__filters-line-title"><?php echo $option['name']; ?>:</span>
								<span class="aside__filters-line-text"><?php echo $value['name']; ?></span>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			<?php } ?>

			<?php if ($show_price) { # Price filtering ?>

				<div class="aside__option">
					<div class="aside__option-name">
						<h3 class="aside__option-name-text"><?php echo $text_price; ?>:</h3>
					</div>

					<div class="option-values">
						<div id="scale-price" class="scale ocf-target"
						     data-option-id="p"
						     data-start-min="<?php echo $min_price_get; ?>"
						     data-start-max="<?php echo $max_price_get; ?>"
						     data-range-min="<?php echo $min_price; ?>"
						     data-range-max="<?php echo $max_price; ?>"
						     data-element-min="#price-from"
						     data-element-max="#price-to"
						     data-control-min="#min-price-value"
						     data-control-max="#max-price-value"
						></div>
					</div>

					<div class="aside__option-values"><span>&nbsp;</span>
						<input id="min-price-value" id="price-from" type="text" name="price[min]" value="<?php echo $min_price_get; ?>" class="aside__option-input"><span>&nbsp;-&nbsp;</span>
						<input id="max-price-value" id="price-to" type="text" name="price[max]" value="<?php echo $max_price_get; ?>" class="aside__option-input"><span><?php echo $symbol_right; ?></span>
					</div>
				</div>

			<?php } # Price filtering end ?>

		</div>

		<?php foreach ($options as $option) { ?>

			<div class="aside__option" id="option-<?php echo $option['option_id']; ?>">
				<div class="aside__option-name">
					<h3 class="aside__option-name-text"><?php echo $option['name']; ?></h3>
				</div>

				<div class="aside__option-values">

					<?php if ($option['type'] == 'select') { # Select type start ?>

						<select class="form-control ocf-target<?php echo ($option['selected'] ? ' selected' : ''); ?>">
							<?php foreach ($option['values'] as $value) { ?>
								<?php if ($value['selected']) { ?>
									<option value="<?php echo $value['href']; ?>" id="v-<?php echo $value['id']; ?>" selected="selected"><?php echo $value['name']; ?></option>
								<?php } elseif ($value['count']) { ?>
									<option value="<?php echo $value['href']; ?>" id="v-<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
								<?php } else { ?>
									<option value="" id="v-<?php echo $value['id']; ?>" disabled="disabled"><?php echo $value['name']; ?></option>
								<?php } ?>
							<?php } ?>
						</select>

					<?php } elseif ($option['type'] == 'radio' || $option['type'] == 'checkbox') { # Radio and Checkbox types start ?>

						<?php foreach ($option['values'] as $key => $value) { ?>

							<div class="aside__option-field">

								<?php if ($option['color']) { ?>
									<div class="color" style="background-color: #<?php echo $value['color']; ?>;"></div>
								<?php } ?>

								<?php if ($option['image']) { ?>
									<div class="image" style="background-image: url(<?php echo $value['image']; ?>);"></div>
								<?php } ?>

								<?php if ($value['selected']) { ?>
									<label for="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" id="v-<?php echo $value['id']; ?>" class="aside__option-values-name selected">
										<input type="<?php echo $option['type']; ?>" id="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="<?php echo $value['href']; ?>" checked="checked" class="ocf-target" />
										<a href="<?php echo $value['href']; ?>" class="aside__option-values-label"></a><?php echo $value['name']; ?>
										<?php if ($show_counter) { ?>
											<small class="badge"></small>
										<?php } ?>
									</label>
								<?php } elseif ($value['count']) { ?>
									<label for="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" id="v-<?php echo $value['id']; ?>" class="aside__option-values-name">
										<input type="<?php echo $option['type']; ?>" id="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="<?php echo $value['href']; ?>" class="ocf-target" />
										<a href="<?php echo $value['href']; ?>" class="aside__option-values-label"></a><?php echo $value['name']; ?>
										<?php if ($show_counter) { ?>
											<small class="badge"><?php echo $value['count']; ?></small>
										<?php } ?>
									</label>
								<?php } else { ?>
									<label for="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" id="v-<?php echo $value['id']; ?>" class="aside__option-values-name disabled">
										<input type="<?php echo $option['type']; ?>" id="<?php echo $option['option_id']; ?>-<?php echo $value['id']; ?>" name="ocfilter_filter[<?php echo $option['option_id']; ?>]" value="" disabled="disabled" class="ocf-target" />
										<span class="aside__option-values-label"></span><?php echo $value['name']; ?>
										<?php if ($show_counter) { ?>
											<small class="badge">0</small>
										<?php } ?>
									</label>
								<?php } ?>
							</div>
						<?php } ?>
					<?php } # End type switcher ?>
				</div>
			</div>

		<?php } # End "foreach $options" ?>

	</aside>

	<script type="text/javascript"><!--
	$(function() {
		var options = {
	    mobile: false,
	    php: {
		    showPrice    : <?php echo $show_price; ?>,
		    showCounter  : <?php echo $show_counter; ?>,
			manualPrice  : <?php echo $manual_price; ?>,
	        link         : '<?php echo $link; ?>',
		    path         : '<?php echo $path; ?>',
		    params       : '<?php echo $params; ?>',
		    index        : '<?php echo $index; ?>'
		  },
	    text: {
		    show_all: '<?php echo $text_show_all; ?>',
		    hide    : '<?php echo $text_hide; ?>',
		    load    : '<?php echo $text_load; ?>',
			any     : '<?php echo $text_any; ?>',
		    select  : '<?php echo $button_select; ?>'
		  }
		};

	  if ($('#ocfilter').is(':hidden')) {
	    $('#navbar-ocfilter').html($('#ocfilter').remove().get(0).outerHTML);

	    var html = $('#ocfilter-mobile').remove().get(0).outerHTML;

	    $('.breadcrumb').after(html);

	    options['mobile'] = true;
	  }

	  setTimeout(function() {
	    $('#ocfilter').ocfilter(options);
	  }, 1);
	});
	//--></script>

<?php } ?>