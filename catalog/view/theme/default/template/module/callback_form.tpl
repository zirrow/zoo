<section style="background-color: #000; background-image: url(<?php echo $background; ?>); background-repeat: no-repeat; background-size: cover; background-position: center;" class="callback">
  <div id="callback_container" class="container">
    <h2 class="callback__title"><?php echo $text_call_back; ?></h2>
    <form id="callback" class="callback__form">
      <div class="callback__field">
        <input type="text" placeholder="<?php echo $entry_name; ?>" name="name">
      </div>
      <div class="callback__field">
        <input type="text" placeholder="<?php echo $entry_telephone; ?>" name="phone">
      </div>
      <input type="hidden" name="link" value="">
      <input type="button" class="callback_btn" id="callback_btn" value="<?php echo $text_submit; ?>">
    </form>
  </div>
</section>



<script type="text/javascript"><!--

        $(document).ready(function() {
            $('#callback input[name=\'link\']').val(location.href);
        });

        $('#callback_btn').on('click', function() {

            $.ajax({
                url: 'index.php?route=module/callback/addCall',
                type: 'post',
                data: $('#callback input[name=\'name\'], #callback input[name=\'phone\'], #callback input[name=\'link\']'),
                dataType: 'json',
                success: function(json) {
                    if(json['error']) {
                        if(json['error']['name']){
                            $('#callback input[name=\'name\']').addClass('error');
                            $('#callback input[name=\'name\']').after('<label id=\'name-error\' class=\'error\' for=\'name\'>'+json['error']['name']+'</label>');
                        }
                        if(json['error']['phone']){
                            $('#callback input[name=\'phone\']').addClass('error');
                            $('#callback input[name=\'phone\']').after('<label id=\'phone-error\' class=\'error\' for=\'phone\'>'+json['error']['phone']+'</label>');
                        }
                    }else{
                        $('#callback_container').html('<div class=\'callback_container_text\'>'+json['success']+'</div>');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

//-->
</script>