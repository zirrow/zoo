<div class="simplecheckout-block methods__block methods__block--reg" id="simplecheckout_customer" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
  <?php if ($display_header || $display_login) { ?>
  <div class="checkout-heading panel-heading">
      <h4 class="methods__title methods__title--reg"><?php echo $text_checkout_customer ?></h4>
      <?php if ($display_login) { ?>
<!--          <span class="checkout-heading-button">-->
<!--          <a href="javascript:void(0)" data-onclick="openLoginBox">--><?php //echo $text_checkout_customer_login ?><!--</a>-->
<!--          </span>-->
      <?php } ?>
  </div>
  <?php } ?>
  <div class="simplecheckout-block-content">
    <?php if ($display_registered) { ?>
      <div class="success"><?php echo $text_account_created ?></div>
    <?php } ?>
    <?php if ($display_you_will_registered) { ?>
      <h4 class="methods__title methods__title--reg"><?php echo $text_you_will_be_registered ?></h4>
    <?php } ?>
    <div class="methods__content">
      <div class="methods__field-subtitle">
        <span>Вы уже зарегистрированы?
          <a href="javascript:void(0);" data-fancybox="" data-src="#personal">Войти</a></span>
      </div>

        <?php foreach ($rows as $row) { ?>
	      <?php echo $row ?>
      <?php } ?>
    </div>
  </div>
</div>