<div class="simplecheckout-block" id="simplecheckout_cart" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
<?php if ($display_header) { ?>
    <div class="checkout-heading panel-heading"><?php echo $text_cart ?></div>
<?php } ?>
<?php if ($attention) { ?>
    <div class="simplecheckout-warning-block"><?php echo $attention; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="simplecheckout-warning-block"><?php echo $error_warning; ?></div>
<?php } ?>
    <div class="table-responsive">
        <table class="simplecheckout-cart cart__wrap">
            <tbody>
            <?php foreach ($products as $product) { ?>
                <?php if (!empty($product['recurring'])) { ?>
                    <tr>
                        <td class="simplecheckout-recurring-product" style="border:none;"><img src="<?php echo $additional_path ?>catalog/view/theme/default/image/reorder.png" alt="" title="" style="float:left;" />
                            <span style="float:left;line-height:18px; margin-left:10px;">
                            <strong><?php echo $text_recurring_item ?></strong>
                            <?php echo $product['profile_description'] ?>
                            </span>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td>
                        <div class="item-cart">
                            <div class="item-cart__inner">
                                <div class="item-cart__banner">
                                    <?php echo $product['stickers'] ?>
                                </div>
                                <div class="image item-cart__img">
		                            <?php if ($product['thumb']) { ?>
                                        <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
		                            <?php } ?>
                                </div>
                                <div class="item-cart__content">
                                    <div class="name item-cart__title">
                                        <a href="<?php echo $product['href']; ?>">
                                            <span class="item-cart__title-text"><?php echo $product['name']; ?></span>
                                        </a>
			                            <?php if (!$product['stock'] && ($config_stock_warning || !$config_stock_checkout)) { ?>
                                            <span class="product-warning">***</span>
			                            <?php } ?>
                                    </div>
                                    <div class="item-cart__article">
			                            <?php foreach ($product['option'] as $option) { ?>
                                            <span class="item-cart__article-pack"> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></span><br />
			                            <?php } ?>
			                            <?php if (!empty($product['recurring'])) { ?>
                                            - <span class="item-cart__article-pack"><?php echo $text_payment_profile ?>: <?php echo $product['profile_name'] ?></span>
			                            <?php } ?>
			                            <?php if ($product['reward']) { ?>
                                            <span class="item-cart__article-pack"><?php echo $product['reward']; ?></span>
			                            <?php } ?>
                                    </div>
                                    <div class="item-cart__favorites">
                                        <span onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></span>
                                    </div>

	                                <?php if($product['price_old']){ ?>
                                        <div class="item-cart__cost item-cart__cost--one">
                                            <div class="item-cart__cost-inner">
                                                <span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['price']; ?></span>
                                                <span class="item-cart__cost-price item-cart__cost-price--old"><?php echo $product['price_old']; ?></span>
                                            </div>
                                            <span class="item-cart__cost-discount">-<?php echo $product['percent']; ?> %</span>
                                        </div>
	                                <?php } else { ?>
                                        <div class="item-cart__cost item-cart__cost--one">
                                            <div class="item-cart__cost-inner price">
                                                <span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['price']; ?></span>
                                            </div>
                                        </div>
	                                <?php } ?>
                                    <div class="item-cart__count quantity">
                                        <div class="count count--alt input-group btn-block" style="max-width: 200px;">
                                            <button class="count__max count__max--active" data-onclick="increaseProductQuantity" data-toggle="tooltip" type="button">
                                            </button>
<!--                                            <span class="count__max count__max--active" onclick="decreaseProductQuantity()"></span>-->
                                            <input class="count__input" type="text" data-onchange="changeProductQuantity"
                                                   name="quantity[<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key']; ?>]"
                                                   value="<?php echo $product['quantity']; ?>" size="1"/>
<!--                                            <span class="count__min" onclick="increaseProductQuantity()"></span>-->
                                            <button class="count__min" data-onclick="decreaseProductQuantity" data-toggle="tooltip" type="button">
                                            </button>
                                        </div>
                                    </div>
	                                <?php if(isset($product['total_old']) && $product['total_old']){ ?>
                                        <div class="item-cart__cost item-cart__cost--two">
                                            <div class="item-cart__cost-inner">
                                                <span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['total']; ?></span>
                                                <span class="item-cart__cost-price item-cart__cost-price--old"><?php echo $product['total_old']; ?></span>
                                            </div>
                                        </div>
	                                <?php } else { ?>
                                        <div class="item-cart__cost item-cart__cost--two">
                                            <div class="item-cart__cost-inner">
                                                <span class="item-cart__cost-price item-cart__cost-price--new"><?php echo $product['total']; ?></span>
                                            </div>
                                        </div>
	                                <?php } ?>
                                </div>
                                <div class="item-cart__close">
                                    <button class="item-cart__close-button"
                                            data-onclick="removeProduct"
                                            data-product-key="<?php echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key'] ?>"
                                            data-toggle="tooltip" type="button"></button>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php } ?>
                <?php foreach ($vouchers as $voucher_info) { ?>
                    <tr>
                        <td class="image"></td>
                        <td class="name"><?php echo $voucher_info['description']; ?></td>
                        <td class="model"></td>
                        <td class="quantity">1</td>
                        <td class="price"><?php echo $voucher_info['amount']; ?></td>
                        <td class="total"><?php echo $voucher_info['amount']; ?></td>
                        <td class="remove">
                            <i data-onclick="removeGift" data-gift-key="<?php echo $voucher_info['key']; ?>" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<div id="simplecheckout-cart-total-block">
<?php foreach ($totals as $total) { ?>
    <div class="simplecheckout-cart-total" id="total_<?php echo $total['code']; ?>">
        <span><b><?php echo $total['title']; ?>:</b></span>
        <span class="simplecheckout-cart-total-value"><?php echo $total['text']; ?></span>
        <span class="simplecheckout-cart-total-remove">
            <?php if ($total['code'] == 'coupon') { ?>
                <i data-onclick="removeCoupon" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
            <?php if ($total['code'] == 'voucher') { ?>
                <i data-onclick="removeVoucher" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
            <?php if ($total['code'] == 'reward') { ?>
                <i data-onclick="removeReward" title="<?php echo $button_remove; ?>" class="fa fa-times-circle"></i>
            <?php } ?>
        </span>
    </div>
<?php } ?>
<?php if (isset($modules['coupon'])) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_coupon; ?>&nbsp;<input class="form-control" type="text" data-onchange="reloadAll" name="coupon" value="<?php echo $coupon; ?>" /></span>
    </div>
<?php } ?>
<?php if (isset($modules['reward']) && $points > 0) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_reward; ?>&nbsp;<input class="form-control" type="text" name="reward" data-onchange="reloadAll" value="<?php echo $reward; ?>" /></span>
    </div>
<?php } ?>
<?php if (isset($modules['voucher'])) { ?>
    <div class="simplecheckout-cart-total">
        <span class="inputs"><?php echo $entry_voucher; ?>&nbsp;<input class="form-control" type="text" name="voucher" data-onchange="reloadAll" value="<?php echo $voucher; ?>" /></span>
    </div>
<?php } ?>
<?php if (isset($modules['coupon']) || (isset($modules['reward']) && $points > 0) || isset($modules['voucher'])) { ?>
    <div class="simplecheckout-cart-total simplecheckout-cart-buttons">
        <span class="inputs buttons"><a id="simplecheckout_button_cart" data-onclick="reloadAll" class="button btn-primary button_oc btn"><span><?php echo $button_update; ?></span></a></span>
    </div>
<?php } ?>
<div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
<?php if ($display_weight) { ?>
    <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
<?php } ?>
</div>
<input type="hidden" name="remove" value="" id="simplecheckout_remove">
<?php if (!$display_model) { ?>
    <style>
    .simplecheckout-cart col.model,
    .simplecheckout-cart th.model,
    .simplecheckout-cart td.model {
        display: none;
    }
    </style>
<?php } ?>
</div>