<div class="simplecheckout-block cart__order" id="simplecheckout_comment">
    <div class="container">
        <div class="cart__order-inner">
            <div class="cart__order-half simplecheckout-block-content">
                <div class="cart__order-comment">
					<?php if ($display_header) { ?>
                        <span><?php echo $label ?></span>
					<?php } ?>
                    <textarea class="form-control" name="comment" id="comment" placeholder="<?php echo $placeholder ?>"
                              data-reload-payment-form="true"><?php echo $comment ?></textarea>
                </div>
            </div>
            <div class="cart__order-half cart__order-half--end">
                <div class="cart__order-total" id="cart__order-total">
                </div>
            </div>
        </div>
    </div>
</div>