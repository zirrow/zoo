<?php echo $header; ?>

  <div class="breadcrumbs">
    <div class="container">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
      <?php } ?>
    </div>
  </div>

  <?php echo $column_left; ?>
  <?php echo $content_top; ?>

    <section class="g-template g-template--stock">
      <div class="container">
        <?php echo $description; ?>
      </div>
    </section>

  <?php echo $content_bottom; ?>
  <?php echo $column_right; ?>

<?php echo $footer; ?>