<script type="text/javascript"><!--
	// 2017/04/17 (based on so-lovefashion.product.tpl)

	var poip_product_custom = function(){
		poip_product_default.call(this);
	}
	poip_product_custom.prototype = Object.create(poip_product_default.prototype);
	poip_product_custom.prototype.constructor = poip_product_custom;
	
	poip_product_custom.prototype.custom_init = function(){

	}
	
	// << ITS OWN FUNCTIONS
	// >> ITS OWN FUNCTIONS
	
	// << ADDITIONAL FUNCTIONS
	// without replacing or stopping original script execution, just addition
	
	poip_product_custom.prototype.additional_endOfEventAdditionalImageMouseover = function(image_a) {
		$(image_a).closest('#thumb-slider').find('a.active').removeClass('active');
		$(image_a).addClass('active');	
	}
	
	// >> ADDITIONAL FUNCTIONS 
	
	// << REPLACING FUNCTIONS
	// to be called from standard function, to work instead of standard algorithm
	
	poip_product_custom.prototype.replace_setVisibleImages = function(images, counter) {
		var this_object = this;
		
		clearTimeout(this_object.set_visible_images_timer_id);
		if (!counter) counter = 1;
		if (counter == 100) {
			this_object.set_visible_images_timer_id = false;
			return;
		}
		
		var carousel_selector = '#thumb-slider';
		var carousel_elem = $(carousel_selector);
	
		if ( carousel_elem.length ) {
			
			if ( !$('#poip_images').length ) {
				carousel_elem.before('<div id="poip_images" style="display:none;"></div>');
				carousel_elem.find('a').each(function(){
					$('#poip_images').append( $(this).clone() );
				});
			}
			
			if ( carousel_elem.is('.owl-theme') ) { // bottom position of additional images owlCarousel2
				
				var current_carousel = carousel_elem.data('owlCarousel2');
				
				if ( this_object.set_visible_images_first_call ) {
					if (!current_carousel || !carousel_elem.find('.owl2-item').length || document.readyState != "complete" ) {
						this_object.set_visible_images_timer_id = setTimeout(function(){ this_object.replace_setVisibleImages(images, counter+1); }, 100);
						return;
					}
					this_object.set_visible_images_first_call = false;
				} else {
				
					var current_imgs = [];
					carousel_elem.find('a').each( function(){
						current_imgs.push($(this).attr('data-image'));
					});
					
					if ( current_imgs.toString() == images.toString() ) {
						this_object.set_visible_images_timer_id = false;
						return; // nothing to change
					}
				}

				var shown_imgs = [];
				var mfp_items = [];
				
				html = '';
				// correct order of images
				for (var i in images) {
					if ( !images.hasOwnProperty(i) ) continue;
					var elem = $('#poip_images a[data-image="'+images[i]+'"]:first');
					if (elem.length) {
						
						mfp_items.push({src: elem.attr('data-image')});
						
						html+= this_object.getElementOuterHTML(elem);
						
					}
				}
				
				if ( $('#product-quick').length ) { // quick view
					// actually owlCarousel2 is changable by replacing only
					html = '<div id="thumb-slider" class="owl-theme owl-loaded owl-drag '+(images.length<3 ? 'not_full_slider' : 'full_slider')+'">'+html+'</div>';
					
					carousel_elem.replaceWith(html);
					
					var carousel_elem = $(carousel_selector);
					
					carousel_elem.owlCarousel2({
						nav:true,
						dots: false,
						slideBy: 1,
						margin:10,
						navText: ['',''],
						
						responsive:{
							0:{
								items: 2,
							},
							481:{
								items:2,
							},
							768:{
								items:4,	
							},
							992:{
								items:4
							},
							1200:{
								items:4
							}
						}
					});
				} else {
				
					// actually owlCarousel2 is changable by replacing only
					html = '<div id="thumb-slider" class="owl-theme owl-loaded owl-drag full_slider owl-carousel">'+html+'</div>';
					
					carousel_elem.replaceWith(html);
					
					var carousel_elem = $(carousel_selector);
					
					carousel_elem.owlCarousel2({
						nav:true,
						dots: false,
						slideBy: 1,
						margin:10,
						navText: ["", ""],
						<?php if($direction=='rtl'):?> rtl:true, <?php endif;?>
						responsive:{
							0:{
								items:2
							},
							600:{
								items:3
							},
							1000:{
								items:4
							}
						}
					});
				}
				
				var elem_cnt = 0;
				carousel_elem.find('a').each( function(){
					$(this).attr('data-index', elem_cnt);
					elem_cnt++;
				} );
			
			} else { // left position of additional images - lightSlider 
				
				if ( this_object.set_visible_images_first_call ) {
					if ( !carousel_elem.find('.lslide').length || document.readyState != "complete" ) {
						this_object.set_visible_images_timer_id = setTimeout(function(){ this_object.replace_setVisibleImages(images, counter+1); }, 100);
						return;
					}
				}
				
				var current_imgs = [];
				carousel_elem.find('a').each( function(){
					current_imgs.push($(this).attr('data-image'));
				});
				
				if ( current_imgs.toString() == images.toString() ) {
					this_object.set_visible_images_timer_id = false;
					return; // nothing to change
				}
				
				// lightSlider carousel, let's replace it completely
				
				var shown_imgs = [];
				
				html = '';
				// correct order of images
				for (var i in images) {
					if ( !images.hasOwnProperty(i) ) continue;
					
					var elem = $('#poip_images a[data-image="'+images[i]+'"]:first');
					if (elem.length) {
						html += '<li class="owl2-item">'+ this_object.getElementOuterHTML(elem) +'</li>';
						//current_carousel.addItem('<div>'+ this_object.getElementOuterHTML($(this)) +'</div>');
					}
					
				}
				
								
				html = '<div id="thumb-slider" class="thumb-vertical-outer">'
					+'<span class="btn-more prev-thumb nt"><i class="fa fa-chevron-up"></i></span>'
					+'<span class="btn-more next-thumb nt"><i class="fa fa-chevron-down"></i></span>'
					+'<ul class="thumb-vertical">'
					+html
					+'</ul>'
					+'</div>';
				
				carousel_elem.replaceWith(html);
				// get again after replacement
				carousel_elem = $(carousel_selector);
				
				var a_cnt = 0;
				carousel_elem.find('li a').each(function(){
					$(this).attr('data-index', a_cnt);
					$(this).removeClass('active');
					a_cnt++;
				});
				
				var thumbslider = carousel_elem.find('.thumb-vertical').lightSlider({
					item: 4,
					autoWidth: false,
					vertical:true,
					slideMargin: 10,
					verticalHeight:410,
					pager: false,
					controls: true,
					prevHtml: '<i class="fa fa-angle-up"></i>',
					nextHtml: '<i class="fa fa-angle-down"></i>',
					responsive: [
						{
							breakpoint: 1199,
							settings: {
								verticalHeight: 300,
								item: 3,
							}
						},
						{
							breakpoint: 1024,
							settings: {
								verticalHeight: 490,
								item: 5,
								slideMargin: 5,
							}
						},
						{
							breakpoint: 768,
							settings: {
								verticalHeight: 360,
								item: 3,
							}
						},
						{
							breakpoint: 480,
							settings: {
								verticalHeight: 110,
								item: 1,
							}
						},
					]
				});
				
				
				//Call JQuery lightSlider - Go to previous slide
				//if( images.length >= 4){
					carousel_elem.find('.prev-thumb').click(function(){
						thumbslider.goToPrevSlide();
					});
					carousel_elem.find('.next-thumb').click(function(){
						thumbslider.goToNextSlide();
					});
				/*	
				}else{
					$('#thumb-slider .slider-btn').hide();
				}
				*/
				
				
				var mfp_items = [];
				for (var i in images) {
					if ( !images.hasOwnProperty(i) ) continue;
					mfp_items.push( {src: images[i] } );
				}
			}
			
			// common part
			$("#thumb-slider .owl2-item").each(function() {
				$(this).find("[data-index='0']").addClass('active');
			});
			
			// ZOOM
			if ( $('#product-quick').length ) { // quickview
				
				var zoomCollection = '.large-image img';
				
				$.removeData(zoomCollection, 'elevateZoom');
				$.removeData(zoomCollection, 'zoomImage');
				$('.zoomContainer').remove();
				
				$( zoomCollection ).elevateZoom({
					zoomType        : "inner",
					lensSize    :"350",
					easing:true,
					gallery:'thumb-slider',
					cursor: 'pointer',
					galleryActiveClass: "active"
				});
				
			} else {
				<?php if(isset($product_enablezoom) && $product_enablezoom) { ?>
					
					var zoomCollection = '.large-image img';
					
					$.removeData(zoomCollection, 'elevateZoom');
					$.removeData(zoomCollection, 'zoomImage');
					$('.zoomContainer').remove();
					
					$( zoomCollection ).elevateZoom({
						<?php if( $product_zoommode != 'basic' ) { ?>
						zoomType        : "<?php echo isset($product_zoommode) ? $product_zoommode : 'basic';?>",
						<?php } ?>
						lensSize    :"<?php echo isset($product_zoomlenssize) ? $product_zoomlenssize : '300';?>",
						easing:true,
						gallery:'thumb-slider',
						cursor: 'pointer',
						galleryActiveClass: "active"
					});
					$('.large-image img').magnificPopup({
						items: mfp_items,
						gallery: { enabled: true, preload: [0,2] },
						type: 'image',
						mainClass: 'mfp-fade',
						callbacks: {
							open: function() {
								<?php if ($images) { ?>
								var activeIndex = parseInt($('#thumb-slider .img.active').attr('data-index'));
								<?php }else{ ?>
								var activeIndex = 0;
								<?php } ?>
								var magnificPopup = $.magnificPopup.instance;
								magnificPopup.goTo(activeIndex);
							}
						}
					});
					
				<?php } else { ?>
				
					$('#thumb-slider .owl2-item').magnificPopup({
						items: mfp_items,
						navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
						gallery: { enabled: true, preload: [0,2] },
						type: 'image',
						mainClass: 'mfp-fade',
						callbacks: {
							open: function() {
								$cur = this.st.el;
								<?php if ($images) { ?>
								var activeIndex = parseInt($cur.children(".thumbnail").attr('data-index'));
								<?php }else{ ?>
								var activeIndex = 0;
								<?php } ?>
								var magnificPopup = $.magnificPopup.instance;
								magnificPopup.goTo(activeIndex);
							}
						}
					});
		
				<?php } ?>
			} // ZOOM end	
			
			if (poip_settings['img_hover']) {
				carousel_elem.find('a').mouseover(function(){
					this_object.eventAdditionalImageMouseover(this);
				}); 
			}
		}
		
		this_object.set_visible_images_timer_id = false;
		this_object.set_visible_images_first_call = false;
	}
	
	poip_product_custom.prototype.replace_updateZoomImage = function(img_click) {
		var this_object = this;
		var main_image = this_object.getMainImage();
		if ( main_image.attr('data-zoom-image') ) {
			this_object.getMainImage().attr('data-zoom-image', img_click);
			this_object.getMainImage().data('zoomImage', img_click);
		
			this_object.elevateZoomDirectChange(img_click, 100);
		}
	}
	
	poip_product_custom.prototype.replace_useColorboxRefreshing = function() {
		return false;
	}
	
	poip_product_custom.prototype.replace_getMainImage = function() {
		return $('.large-image img');
	}
	
	poip_product_custom.prototype.if_setMainImage = function(main, popup) {
		var this_object = this;
		
		this_object.getMainImage().attr('src', popup);
		this_object.getMainImage().closest('a').attr('href', popup);
		
		return true;
	}
	
	poip_product_custom.prototype.if_afterSetProductOptionValue = function(option_element) {
		var this_object = this;
		
		var $button = $(option_element).siblings('span.option-content-box');
		if ( $button.length ) {
			$button.click();
			return true;
		}
		return false;
	}

	// >> REPLACING FUNCTIONS	

	var poip_product = new poip_product_custom();

//--></script>