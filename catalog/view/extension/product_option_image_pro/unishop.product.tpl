<script type="text/javascript"><!--

	var poip_product_custom = function(){
		poip_product_default.call(this);
	}
	poip_product_custom.prototype = Object.create(poip_product_default.prototype);
	poip_product_custom.prototype.constructor = poip_product_custom;
	
	poip_product_custom.prototype.custom_init = function(){
		
	}
	
	// << ITS OWN FUNCTIONS
	// >> ITS OWN FUNCTIONS
	
	// << ADDITIONAL FUNCTIONS
	// without replacing or stopping original script execution, just addition
	
	poip_product_custom.prototype.additional_endOfEventAdditionalImageMouseover = function(image_a) {
		var this_object = this;
		
		$(image_a).addClass('selected');
		$('#product .thumbnails .image-additional a').not($(image_a)).removeClass('selected');
		//$('#product .thumbnails li:first a').attr('href', $(this).attr('href')).attr('data-key', $(this).attr('data-key'));
		//$('#product .thumbnails li:first img').attr('src', $(this).attr('data-thumb'));
	}
	
	// >> ADDITIONAL FUNCTIONS 
	
	// << REPLACING FUNCTIONS
	// to be called from standard function, to work instead of standard algorithm (prefixes replace_ and if_)

	
	poip_product_custom.prototype.replace_setVisibleImages = function(images, counter) {
		var this_object = this;
		
		this_object.getAdditionalImagesBlock().find('img:first').each(function(){
			if ( !$(this).parent().is('a') ) {
				$(this).closest('div').hide();
			}
		});
	
		var shown_img = [];
		this_object.getAdditionalImagesBlock().find('a').each( function(){
			var href = $(this).attr('href');
			if ( $.inArray( href, images) != -1 && $.inArray( href, shown_img) == -1) {
				$(this).closest('div').show();
				$(this).attr('data-poip-visible', true);
				shown_img.push( href );
			} else {
				$(this).closest('div').hide();
				$(this).removeAttr('data-poip-visible');
			}
		});
		
		if ( $('.thumbnails').data('magnificPopup') ) { // older unishop
			$('.thumbnails').data('magnificPopup').delegate = 'a[data-poip-visible]';
		} else { // newer unishop
			var img_array = [];

			$('#product .thumbnails .image-additional a[data-poip-visible]').each(function() {
				img_array.push({src: $(this).attr('href')});
			});
			
			if(img_array.length == 0) {
				img_array.push({src: $('#product .thumbnails li:first a').attr('href'), type: 'image'});
			}
			
			$('#product .thumbnails li:first a').off('click');
			$('#product .thumbnails li:first a').click(function(e) {
				var href = $(this).attr('href');
				e.preventDefault();
				$.magnificPopup.open({
					items:img_array,
					gallery:{
						enabled:true,
					},
					type:'image',
				});
				var pos_go_to = 0;
				$('#product .thumbnails .image-additional a[data-poip-visible]').each(function(){
					if ( $(this).attr('href') == href ) {
						return false;
					}
					pos_go_to++;
				});
				$.magnificPopup.instance.goTo(pos_go_to);
			});
		}
	}
	
	// returns result of jQuery call ( $(...) ) or FALSE
	poip_product_custom.prototype.if_getMainImage = function() {
		return $('.thumbnails li:first .thumbnail img');
	}
	
	
	// >> REPLACING FUNCTIONS	

	var poip_product = new poip_product_custom();

//--></script>