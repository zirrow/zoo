<?php
class ControllerCommonCart extends Controller {
	public function index() {
		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['text_cart_name'] = $this->language->get('text_cart_name');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0));
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_total'] = $this->currency->format($total);
        $data['text_submit_one_click'] = $this->language->get('text_submit_one_click');
        $data['text_one_click'] = $this->language->get('text_one_click');

		if ($this->customer->getUserDiscount()){
			$data['cart_user_discount'] = sprintf($this->language->get('text_cart_user_discount'),$this->customer->getUserDiscount());
		} else {
			$data['cart_user_discount'] = sprintf($this->language->get('text_cart_user_discount'),0);
		}


		$data['button_remove'] = $this->language->get('button_remove');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();
        $total_old = 0; // общая сумма без скидки
        $tax_class_id = 0; // для привдения $total_old к пристойному виду (берём как у товара)

		foreach ($this->cart->getProducts() as $product) {

			$percent = 0;

            $tax_class_id = $product['tax_class_id'];
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			} else {
				$total = false;
			}


            // начинаем считать сумму без скидки
            $price_old = 0; //старая цена на товар-опцию без скидки
            $options = $this->load->controller('module/related_options/ro',$product);

			foreach ($options as $combo_option){ // проходим по всем связанным опциям товара
			    if($this->compare_combo_options($product['option'],$combo_option['options'])){ // сравниваем комбинацию опций в выбранном товаре и комбинации из всего списка возможных
			        if($combo_option['price']!=$this->currency->format($this->tax->calculate(0, $product['tax_class_id'], $this->config->get('config_tax')))) { // если цена комбинации не нулевая
                        $total_old += $combo_option['price'] * $product['quantity'];
                        $price_old = $combo_option['price']* $product['quantity'];
                    }else{ // иначе берём цену самого товара
                        $total_old += $total * $product['quantity'];
                    }
                    break;
                }
            }

            if($price_old){
                $price_old = $this->currency->format($this->tax->calculate($price_old, $product['tax_class_id'], $this->config->get('config_tax')));
                $percent = round(($price_old - $total) * 100 / $price_old, 2);
            }

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'product_id'=> $product['product_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'sku'       => $product['sku'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
                'price_old' => $price_old,
                'percent'   => $percent,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id']),
                'options'   => $options
			);
		}

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($total_data as $result) {
			$data['totals'][] = array(
				'title' => $result['title'],
				'text'  => $this->currency->format($result['value']),
			);
		}

        $last = count($data['totals'])-1;
        $data['total'] = $data['totals'][$last ]['text'];

        $total_old = $this->currency->format($this->tax->calculate($total_old, $tax_class_id, $this->config->get('config_tax')));
        $data['total_old'] = $total_old;

		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/cart.tpl', $data);
		} else {
			return $this->load->view('default/template/common/cart.tpl', $data);
		}
	}

	public function info() {
		$this->response->setOutput($this->index());
	}

    public function compare_combo_options($product_options, $combinations_option) {

	    if(count($product_options) != count($combinations_option)){ // если количество опций товара и комбинации не совпало
            return false; // - не наша комбинация
        }

        foreach($product_options as $key=>$product_option){ // для каждой опции товара
            // есле хотябы одно значение опции товара не совпало со значением комбинации
            if( $combinations_option[$product_option['product_option_id']] != $product_option['product_option_value_id']){
                return false; // - не наша комбинация
            }
        }
        return true; // - таки она
    }
}
