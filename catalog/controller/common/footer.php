<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_subscription_title'] = $this->language->get('text_subscription_title');
        $data['text_review'] = $this->language->get('text_review');
        $data['text_newsletter_ok'] = $this->language->get('text_newsletter_ok');

        $data['text_news'] = $this->language->get('text_news');
        $data['text_info'] = $this->language->get('text_info');
        $data['text_regular_feed'] = $this->language->get('text_regular_feed');
        $data['text_info_cat'] = $this->language->get('text_info_cat');
        $data['text_all_brand'] = $this->language->get('text_all_brand');
        $data['text_warranty'] = $this->language->get('text_warranty');
        $data['text_about_us'] = $this->language->get('text_about_us');
        $data['text_delivery_warranty'] = $this->language->get('text_delivery_warranty');
        $data['text_sale'] = $this->language->get('text_sale');



		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

        $data['logged'] = $this->customer->isLogged();

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $data['telephone']  =   $this->config->get('config_telephone');
        $data['telephone2'] =   $this->config->get('config_telephone2');
        $data['telephone3'] =   $this->config->get('config_telephone3');
        $data['telephone4'] =   $this->config->get('config_telephone4');
        $data['email']      =   $this->config->get('config_email');
        $data['open']       =   nl2br($this->config->get('config_open'));

        $data['config_vk']  =   $this->config->get('config_vk');
        $data['config_fb']  =   $this->config->get('config_fb');
        $data['config_yt']  =   $this->config->get('config_yt');
        $data['config_g']   =   $this->config->get('config_g');
        $data['config_tw']  =   $this->config->get('config_tw');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->request->server['HTTP_HOST'], date('Y', time()));
        $data['callback'] = $this->load->controller('module/callback_popup','footer');

        $data['special'] = $this->url->link('product/special');
        $data['contact'] = $this->url->link('information/contact');

        $this->load->model('newsblog/category');
        $this->load->model('newsblog/article');

        $data['newsblog_categories'] = array();


        $categories = $this->model_newsblog_category->getCategories();
		$data['categories'] = array();

		foreach ($categories as $category) {
            if ($category['settings']) {
                $settings = unserialize($category['settings']);
                if ($settings['show_in_top']==0) continue;
            }

            $articles = array();

            if ($category['settings'] && $settings['show_in_top_articles']) {
                $filter = array('filter_category_id'=>$category['category_id'],'filter_sub_category'=>true);
                $results = $this->model_newsblog_article->getArticles($filter);

                foreach ($results as $result) {
                    $articles[] = array(
                        'name'        => $result['name'],
                        'href'        => $this->url->link('newsblog/article', 'newsblog_path=' . $category['category_id'] . '&newsblog_article_id=' . $result['article_id'])
                    );
                }
            }
            $data['categories'][] = array(
                'name'     => $category['name'],
                'children' => $articles,
                'column'   => 1,
                'href'     => $this->url->link('newsblog/category', 'newsblog_path=' . $category['category_id'])
            );
        }

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
