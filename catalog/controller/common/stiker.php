<?php
class ControllerCommonStiker extends Controller {
	public function index($product_id) {

		$this->load->model('design/stiker');
		$this->load->model('tool/image');

		$data['stikers'] = $this->model_design_stiker->getProductStikers($product_id);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/stiker.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/stiker.tpl', $data);
		} else {
			return $this->load->view('default/template/common/stiker.tpl', $data);
		}
	}

}