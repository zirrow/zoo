<?php
class ControllerModuleCallback extends Controller {
	public function index($setting) {

        $this->document->addStyle('catalog/view/theme/default/stylesheet/callback.css');

        $this->load->language('module/callback');
        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['text_submit'] = $this->language->get('text_submit');
        $data['text_call_back'] = $this->language->get('text_call_back');

		if (isset($setting['module_description'])) {
            $data['background'] = 'image/'.$setting['module_description']['image'];

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/callback_form.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/callback_form.tpl', $data);
			} else {
				return $this->load->view('default/template/module/callback_form.tpl', $data);
			}
		}
	}

	public function addCall() {
        $json = array();
        $this->load->language('module/callback');

        $this->load->model('module/callback_form');
        if(!isset($this->request->post['name']) || !$this->request->post['name']){
            $json['error']['name'] = $this->language->get('error');
        }

        if(!isset($this->request->post['phone']) ||  strlen($this->request->post['phone'])<10 || substr($this->request->post['phone'],-1)=='_'){
            $json['error']['phone'] = $this->language->get('error');
        }

        if(!isset($json['error']))
        {
            $this->model_module_callback_form->addCall($this->request->post);
            $json['success'] = $this->language->get('text_success');
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}