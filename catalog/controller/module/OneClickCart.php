<?php
class ControllerModuleOneClickCart extends Controller
{
    private $error = array();

    public function BuyOneClick()
    {
        $json = array();
        $this->load->language('module/OneClickCart');

        if ($this->confirm_order()) {
            //$json['redirect'] = $this->url->link('checkout/cart');
            $json['success'] = $this->language->get('text_all_good');
            $this->cart->clear();
        } else {
            $json['error'] = $this->language->get('text_error');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function confirm_order()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['one_click_cart_phone'])) {

            $order_data = array();

            $this->load->language('checkout/checkout');
            $this->load->model('extension/extension');
            $this->load->model('module/OneClick');
            $this->load->model('account/customer');

            $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');

            if ($order_data['store_id']) {
                $order_data['store_url'] = $this->config->get('config_url');
            } else {
                $order_data['store_url'] = HTTP_SERVER;
            }


            // Customer Details

            $logged = $this->customer->isLogged();

            if(!$logged) {// если пользователь не зарегистрирован
                $customer = $this->model_module_OneClick->getCustomerByPhone($this->request->post['one_click_cart_phone']);

                if ($customer) { // если телефон есть в базе привязываем покупку к данному пользователю
                    $order_data['customer_id'] = $customer['customer_id'];
                    $order_data['customer_group_id'] = $customer['customer_group_id'];
                    $order_data['firstname'] = $customer['firstname'];
                    $order_data['lastname'] = $customer['lastname'];
                    $order_data['email'] = $customer['email'];
                    $order_data['fax'] = $customer['fax'];
                } else { // если это новый покупатель
                    $order_data['customer_id'] = '0';
                    $order_data['customer_group_id'] = $this->config->get('config_customer_group_id');
                    $order_data['firstname'] = "заказ в 1 клик";
                    $order_data['lastname'] = "";
                    $order_data['email'] = "";
                    $order_data['fax'] = "";
                }
            } else {
                $customer = $this->model_account_customer->getCustomer($this->customer->getId());

                $order_data['customer_id'] = $customer['customer_id'];
                $order_data['customer_group_id'] = $customer['customer_group_id'];
                $order_data['firstname'] = $customer['firstname'];
                $order_data['lastname'] = $customer['lastname'];
                $order_data['email'] = $customer['email'];
                $order_data['fax'] = $customer['fax'];
            }
            $order_data['telephone'] = $this->request->post['one_click_cart_phone'];

            $order_data['payment_firstname'] = "Заказ";
            $order_data['payment_lastname'] = "в один клик";
            $order_data['payment_company'] = "";
            $order_data['payment_address_1'] = "";
            $order_data['payment_address_2'] = "";
            $order_data['payment_city'] = "";
            $order_data['payment_postcode'] = "";
            $order_data['payment_zone'] = "Запорожье";
            $order_data['payment_zone_id'] = "3504";
            $order_data['payment_country'] = "Украина";
            $order_data['payment_country_id'] = "220";
            $order_data['payment_address_format'] = "";
            $order_data['payment_method'] = 'Оплата при получении';
            $order_data['payment_code'] = 'cod';
            $order_data['shipping_firstname'] = '';
            $order_data['shipping_lastname'] = '';
            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = '';
            $order_data['shipping_address_2'] = '';
            $order_data['shipping_city'] = '';
            $order_data['shipping_postcode'] = '';
            $order_data['shipping_zone'] = '';
            $order_data['shipping_zone_id'] = '';
            $order_data['shipping_country'] = '';
            $order_data['shipping_country_id'] = '';
            $order_data['shipping_address_format'] = '';
            $order_data['shipping_custom_field'] = array();
            $order_data['shipping_method'] = '';
            $order_data['shipping_code'] = '';
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking'] = '';


            $order_data['products'] = array();
            $order_data['totals'] = array();

            // Start Cart
            $total_data = array();
            $total = 0;
            $taxes = $this->cart->getTaxes();

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $sort_order = array();

                $results = $this->model_extension_extension->getExtensions('total');

                foreach ($results as $key => $value) {
                    $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
                }

                array_multisort($sort_order, SORT_ASC, $results);

                foreach ($results as $result) {
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);

                        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    }
                }

                $sort_order = array();

                foreach ($total_data as $key => $value) {
                    $sort_order[$key] = $value['sort_order'];
                }

                array_multisort($sort_order, SORT_ASC, $total_data);
            }
            $order_data['products'] = $this->cart->getProducts();
            foreach($order_data['products'] as $key=>$product)
            {
                $order_data['products'][$key]['tax'] = $this->tax->getTax($product['price'], $product['tax_class_id']);
            }

            // Gift Voucher
            $order_data['vouchers'] = array();

            if (!empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $key => $voucher) {
                    $data['vouchers'][] = array(
                        'key' => $key,
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'])
                    );
                }
            }

            $order_data['totals'] = $total_data;
            // end cart

            $order_data['comment'] = "Заказ в один клик";

            $last = count($order_data['totals'])-1;
            $order_data['total'] = $order_data['totals'][$last ]['value'];

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId();
            $order_data['currency_code'] = $this->currency->getCode();
            $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                $order_data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                $order_data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                $order_data['accept_language'] = '';
            }


            $this->load->model('checkout/order');
            $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

            // Set the order history
            if (isset($this->request->post['order_status_id'])) {
                $order_status_id = $this->request->post['order_status_id'];
            } else {
                $order_status_id = $this->config->get('config_order_status_id');
            }

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $order_status_id);

            return true;
        }else{
            return false;
        }
    }
}