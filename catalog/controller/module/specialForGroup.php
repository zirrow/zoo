<?php
class ControllerModuleSpecialForGroup extends Controller {
	public function index() {
        $this->load->model('module/specialForGroup');
        $this->load->model('localisation/language');

        $language_info = $this->model_localisation_language->getLanguageByCode($this->config->get('config_language'));
        $front_language_id = $language_info['language_id'];

        $filter = array(
            'lang_id' => $front_language_id,
            'GroupId' => $this->config->get('config_customer_group_id')
        );

        $results = $this->model_module_specialForGroup->getGroups($filter);

        $rnd = rand(0,count($results)-1);
        $data['SpecialForGroup_text'] = html_entity_decode($results[$rnd]['description']);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/specialForGroup.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/specialForGroup.tpl', $data);
		} else {
			return $this->load->view('default/template/module/specialForGroup.tpl', $data);
		}
	}
}