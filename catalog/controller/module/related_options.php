<?php

class ControllerModuleRelatedOptions extends Controller {
	
	public function get_ro_free_quantity() {

		if ( !$this->model_module_related_options ) {
			$this->load->model('module/related_options');
		}

		$json = $this->model_module_related_options->get_ro_free_quantity();

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}

	public function ro($data){

		$this->load->model('module/related_options');
		$this->load->model('catalog/product');

		$options = $this->model_catalog_product->getProductOptions($data['product_id']);
		$ro_main_arr = $this->model_module_related_options->get_ro_data($data['product_id']);
		$ro = array();


		foreach ($ro_main_arr[0]['ro'] as $rm_kay => $rm_value){

			$name = '';
			foreach ($rm_value['options'] as $ro_key => $ro_option){
				foreach ($options as $option){
					if ($option['product_option_id'] == (int) $ro_key){
						foreach ($option['product_option_value'] as $po_value){
							if($po_value['product_option_value_id'] == $ro_option){
								$name .= ' '.$po_value['name'];
							}
						}
					}
				}
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				if($rm_value['price'] > 0){
					$price = $this->currency->format($this->tax->calculate($rm_value['price'], $data['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = $this->currency->format($this->tax->calculate($data['price'], $data['tax_class_id'], $this->config->get('config_tax')));
				}


			} else {
				$price = false;
			}

			$discounts = array();
			foreach ($rm_value['discounts'] as $rmv_discounts){

				if ((float)$rmv_discounts['price']) {
					$special = $this->currency->format($this->tax->calculate($rmv_discounts['price'], $data['tax_class_id'], $this->config->get('config_tax')));
					if($rm_value['price'] != 0){
						$percent = (($rm_value['price'] - $rmv_discounts['price']) * 100) / $rm_value['price'];
					} else {
						$percent = false;
					}
				} else {
					$special = false;
					$percent = false;
				}

				if($this->config->get('config_customer_group_id') == $rmv_discounts['customer_group_id']){

					$discounts[] = array(
						'relatedoptions_id' => $rmv_discounts['relatedoptions_id'],
						'price'             => $special,
						'percent'           => round($percent,0)

					);
				}
			}


			$specials = array();
			foreach ($rm_value['specials'] as $rmv_special){

				if ((float)$rmv_special['price']) {
					$special = $this->currency->format($this->tax->calculate($rmv_special['price'], $data['tax_class_id'], $this->config->get('config_tax')));
					if($rm_value['price'] != 0){
						$percent = round(($rm_value['price'] - $rmv_special['price']) * 100) / $rm_value['price'];
					} else {
						$percent = false;
					}
				} else {
					$special = false;
					$percent = false;
				}

				if($this->config->get('config_customer_group_id') == $rmv_special['customer_group_id']){

					$specials[] = array(
						'relatedoptions_id' => $rmv_special['relatedoptions_id'],
						'price'             => $special,
						'percent'           => round($percent,0)

					);
				}
			}

			$ro[$rm_kay] = array(
				'relatedoptions_id' => $rm_value['relatedoptions_id'],
				'product_id'        => $rm_value['product_id'],
                'sku'               => $rm_value['sku'],
				'name'              => $name,
				'price'             => $price,
				'options'           => $rm_value['options'],
				'discounts'         => $discounts,
				'specials'          => $specials,
				'options_original'  => $rm_value['options_original'],
			);

		}

		return $ro;
	}
  
}
