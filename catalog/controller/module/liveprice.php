<?php
//  Live Price PRO / Живая цена PRO
//  Support: support@liveopencart.com / Поддержка: help@liveopencart.ru

class ControllerModuleLivePrice extends Controller {
	
	public function get_price() {
		
		if ( $this->config->get('config_customer_price') && !$this->customer->isLogged() ) {
			$this->response->setOutput(json_encode(array()));
			return;
		}
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			exit;
		}
		
		if (isset($this->request->get['quantity'])) {
			$quantity = (int)$this->request->get['quantity'];
		} else {
			$quantity = 1;
		}
		
		if (isset($this->request->post['option_oc'])) {
			$options = $this->request->post['option_oc'];
		} elseif (isset($this->request->post['option'])) {
			$options = $this->request->post['option'];
		} else {
			$options = array();
		}
		
		$non_standard_theme = '';
		if ( isset($this->request->get['non_standard_theme']) ) {
			$non_standard_theme = $this->request->get['non_standard_theme'];
		}
		
		$this->load->model('module/liveprice');
		
		if ( !empty($this->request->post['quantity_per_option']) && is_array($this->request->post['quantity_per_option']) ) {
			// specific calculation for a specific options (quantity is set for each option value)
			$quantity_per_options = $this->request->post['quantity_per_option'];
			$lp_data = $this->model_module_liveprice->getProductTotalPriceForQuantityPerOptionWithHtml( $product_id, $options, $quantity_per_options, $non_standard_theme );
		} else { // standard way
			$lp_data = $this->model_module_liveprice->getProductPriceWithHtml( $product_id, max($quantity, 1), $options, array(), array(), array(), true, $non_standard_theme );
		}
		
		// return only required data
		$prices = array('htmls'=>$lp_data['prices']['htmls'], 'ct'=>$lp_data['prices']['ct']);
		if (isset($this->request->get['rnd'])) {
			$prices['rnd'] = $this->request->get['rnd'];
		}
		
		$this->response->setOutput(json_encode($prices));
		
		/*
		$prices = array();
		$product_data = array();
		$options_data = array();
		$this->model_module_liveprice->getProductPriceWithHtml($product_id, max($quantity, 1), $options, $prices, $product_data, $options_data, true );
		
		// return only required data
		
		$prices = array('htmls'=>$prices['htmls'], 'ct'=>$prices['ct']);
		
		if (isset($this->request->get['rnd'])) {
			$prices['rnd'] = $this->request->get['rnd'];
		}
		
		
		//print_r($prices);
			
		echo json_encode($prices);
		exit;
		*/
	}
	
	
}
