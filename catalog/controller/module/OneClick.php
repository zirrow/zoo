<?php
class ControllerModuleOneClick extends Controller {
    private $error = array();

    public function BuyOneClick()
    {
        $json = array();

        if($this->send_mail() && $this->confirm_order()) {
            $json['success'] = "success";
        } else {
            $json['error'] = "Ошибка формирования заказа или его отправки.";
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
        
        
    public function send_mail()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['product_id'])) {

                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                $mail->setTo($this->config->get('config_email'));
                $mail->setFrom('buy_one_click--'.$this->config->get('config_email'));
                $mail->setSender(html_entity_decode("OneClick", ENT_QUOTES, 'UTF-8'));
                $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), "OneClick"), ENT_QUOTES, 'UTF-8'));

                $this->load->model('catalog/product');
                $product = $this->model_catalog_product->getProduct($this->request->post['product_id']);

                $product_url = $this->url->link('product/product', '?product_id=' . $this->request->post['product_id']);
                $product_href = "<a href='" . $product_url . "'>".$product['name']."</a>";

                $text_send = "С номера ";
                $text_send .= $this->request->post['one_click_phone'];
                $text_send .= " поступил заказ товара:";
                $text_send .= $product_href;
                $mail->setText($text_send);
                $mail->send();

                return true;
        }

        return false;
    }
        
    public function confirm_order()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];

            $order_data = array();

            $this->load->language('checkout/checkout');

            $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
            $order_data['store_id'] = $this->config->get('config_store_id');
            $order_data['store_name'] = $this->config->get('config_name');

            if ($order_data['store_id']) {
                    $order_data['store_url'] = $this->config->get('config_url');
            } else {
                    $order_data['store_url'] = HTTP_SERVER;
            }

            // Customer Details
            $order_data['customer_id'] = '0';
            $order_data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $order_data['firstname'] = "заказ в 1 клик";
            $order_data['lastname'] = "";
            $order_data['email'] = "";
            $order_data['telephone'] = $this->request->post['one_click_phone'];
            $order_data['fax'] = "";

            $order_data['payment_firstname'] = "Заказ";
            $order_data['payment_lastname'] = "в один клик";
            $order_data['payment_company'] = "";
            $order_data['payment_address_1'] = "";
            $order_data['payment_address_2'] = "";
            $order_data['payment_city'] = "";
            $order_data['payment_postcode'] = "";
            $order_data['payment_zone'] = "Запорожье";
            $order_data['payment_zone_id'] = "3504";
            $order_data['payment_country'] = "Украина";
            $order_data['payment_country_id'] = "220";
            $order_data['payment_address_format'] = "";
            $order_data['payment_method'] = 'Оплата при получении';
            $order_data['payment_code'] = 'cod';
            $order_data['shipping_firstname'] = '';
            $order_data['shipping_lastname'] = '';
            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = '';
            $order_data['shipping_address_2'] = '';
            $order_data['shipping_city'] = '';
            $order_data['shipping_postcode'] = '';
            $order_data['shipping_zone'] = '';
            $order_data['shipping_zone_id'] = '';
            $order_data['shipping_country'] = '';
            $order_data['shipping_country_id'] = '';
            $order_data['shipping_address_format'] = '';
            $order_data['shipping_custom_field'] = array();
            $order_data['shipping_method'] = '';
            $order_data['shipping_code'] = '';
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking'] = '';

            $order_data['products'] = array();

            $product = $this->model_catalog_product->getProduct($product_id);

            if (isset($this->request->post['option'])) {
                    $options = array_filter($this->request->post['option']);
            } else {
                    $options = array();
            }

            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

            foreach ($product_options as $product_option) {
                    if ($product_option['required'] && empty($options[$product_option['product_option_id']])) {
                            $this->error['error_option'] = "Обязательные опции не выбраны";
                            return false;
                    }
            }

            $option_data = array();

            $this->load->model('catalog/option');

            $option_price = 0;
            foreach ($options as $key=>$option) {

                $product_option = $this->model_catalog_option->getProductOptionValue($option,$key);
                $test = 0;
                $option_data[] = array(
                        'name'  => $product_option['name'],
                        'value' => (utf8_strlen($product_option['value']) > 20 ? utf8_substr($product_option['value'], 0, 20) . '..' : $product_option['value']),
                        'product_option_id' => $product_option['product_option_id'],
                        'product_option_value_id' => $product_option['product_option_value_id'],
                        'type' => $product_option['type']
                );
                $option_price += $product_option['price'];
            }

            if($product['special'])
            {
                $price_product = ($product['special']+$option_price);
            }else{
                $price_product = ($product['price']+$option_price);
            }
            $total = $price_product * $this->request->post['quantity'];

            $order_data['products'][] = array(
                    'product_id' => $product['product_id'],
                    'name'       => $product['name'],
                    'model'      => $product['model'],
                    'option'     => $option_data,
                    'quantity'   => $this->request->post['quantity'],
                    'subtract'   => $product['subtract'],
                    'price'      => $price_product,
                    'total'      => $total,
                    'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward'     => $product['reward']
            );

            // Totals
            $order_data['totals'] = array();

            $order_data['totals'][] = array(
                'code' => "total",
                'title' => "Итого",
                'value' => $total,
                'sort_order' => '999'
            );

            $order_data['comment'] = "Заказ в один клик";
            $order_data['total'] = $total;

            $order_data['language_id'] = $this->config->get('config_language_id');
            $order_data['currency_id'] = $this->currency->getId();
            $order_data['currency_code'] = $this->currency->getCode();
            $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
            $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

            if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
            } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
            } else {
                    $order_data['forwarded_ip'] = '';
            }

            if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
            } else {
                    $order_data['user_agent'] = '';
            }

            if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
            } else {
                    $order_data['accept_language'] = '';
            }


            $this->load->model('checkout/order');
            $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

            // Set the order history
            if (isset($this->request->post['order_status_id'])) {
                    $order_status_id = $this->request->post['order_status_id'];
            } else {
                    $order_status_id = $this->config->get('config_order_status_id');
            }

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $order_status_id);

            return true;
        }
        return false;
    }
 
}
