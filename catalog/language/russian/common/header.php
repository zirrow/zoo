<?php
// Text
$_['text_home']          = 'Главная';
$_['text_compare']       = 'Сравнение <span>%s</span>';
$_['text_wishlist']      = 'Избранное <span>%s</span>';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Вход';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';
$_['text_page']          = 'страница';
$_['text_special']       = 'Скидки и акции';
$_['text_contact']       = 'Контакты';
$_['text_schedule']      = 'График работы';

$_['text_enter']         = 'Вход в личный кабинет';
$_['text_login']         = 'Логин';
$_['text_pass']          = 'Пароль';
$_['text_forgot']        = 'Забыли пароль?';
$_['text_reg']           = 'Зарегистрируйтесь';
$_['text_social']        = 'Или войдите используя социальные сети';
$_['text_user_discount'] = '<span>(Скидка %s &#37;)</span>';

$_['button_enter']       = 'Войти';
