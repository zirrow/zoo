<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Мы в соц. сетях';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Все бренды';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_review']       = 'Отзывы о магазине';
$_['text_newsletter_ok']= 'Вы успешно подписались на рассылку новостей!';
$_['text_powered']      = ' Copyright &copy; %s 2009-%s. All ridhts reserved';

$_['text_news']        = 'Новости';
$_['text_info']        = 'Статьи';
$_['text_regular_feed']= 'Как выбрать правельный корм?';
$_['text_info_cat']    = 'Что нужно знать о кошках?';
$_['text_all_brand']   = 'Все бренды';
$_['text_warranty']    = '100% гарантии качества';
$_['text_about_us']    = 'О нас';
$_['text_delivery_warranty']    = 'Доставка, оплата, гарантии';
$_['text_sale']        = 'Скидки и Акции';

$_['text_subscription_title'] = '<h4>Подписать Вас на письмо со скидками</h4><span>Каждый месяц у нас скидки до -30%</span>';