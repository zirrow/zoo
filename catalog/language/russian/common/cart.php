<?php
// Text
$_['text_cart_name']          = 'Корзина';
$_['text_items']              = 'Корзина (%s)';
$_['text_empty']              = 'В корзине пусто!';
$_['text_cart']               = 'Открыть Корзину';
$_['text_checkout']           = 'Оформить Заказ';
$_['text_recurring']          = 'Профиль платежа';
$_['text_cart_user_discount'] = 'Ваша скидка: %s &#37;';
$_['text_submit_one_click']   = 'Заказ в один клик';
$_['text_one_click']          = 'Спешите?';