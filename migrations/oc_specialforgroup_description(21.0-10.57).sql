-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 21 2017 г., 10:55
-- Версия сервера: 5.6.34
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zoo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_specialforgroup_description`
--

CREATE TABLE `oc_specialforgroup_description` (
  `id` int(11) NOT NULL,
  `SpecialForGroup_id` int(11) NOT NULL,
  `language` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `oc_specialforgroup_description`
--

INSERT INTO `oc_specialforgroup_description` (`id`, `SpecialForGroup_id`, `language`, `description`) VALUES
(106, 58, 1, 'Текущая скидка – 0%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(107, 58, 3, 'Текущая скидка –1%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(108, 59, 1, 'Текущая скидка – 2%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(109, 59, 3, 'Текущая скидка – 3%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(110, 60, 1, 'Текущая скидка – 4%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(111, 60, 3, 'Текущая скидка – 5%, для получения скидки 5% на весь товар –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(112, 61, 1, 'Будьте всегда с нами –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(113, 61, 3, 'Будьте всегда с нами –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(114, 62, 1, 'Будьте в курсе новостей  –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;'),
(115, 62, 3, 'Будьте в курсе новостей –&lt;a href=&quot;#&quot;&gt;зарегистрируйтесь!&lt;/a&gt;');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_specialforgroup_description`
--
ALTER TABLE `oc_specialforgroup_description`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_specialforgroup_description`
--
ALTER TABLE `oc_specialforgroup_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
