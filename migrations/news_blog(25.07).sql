-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 25 2017 г., 10:54
-- Версия сервера: 5.6.34
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zoo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article`
--

CREATE TABLE `oc_newsblog_article` (
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_available` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_article`
--

INSERT INTO `oc_newsblog_article` (`article_id`, `image`, `date_available`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(1, 'catalog/slider/news-1.jpg', '2017-07-25 10:41:54', 10, 1, 3, '2017-07-25 10:44:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_attribute`
--

CREATE TABLE `oc_newsblog_article_attribute` (
  `article_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_description`
--

CREATE TABLE `oc_newsblog_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `preview` text NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_article_description`
--

INSERT INTO `oc_newsblog_article_description` (`article_id`, `language_id`, `name`, `preview`, `description`, `tag`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(1, 1, 'Новость - бомба!', '&lt;p&gt;Новость - бомба!&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;Новость - бомба!&lt;br&gt;&lt;/p&gt;', '', '', '', '', ''),
(1, 3, 'Новость - бомба!', '&lt;p&gt;Новость - бомба!&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;Новость - бомба!&lt;br&gt;&lt;/p&gt;', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_image`
--

CREATE TABLE `oc_newsblog_article_image` (
  `product_image_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_related`
--

CREATE TABLE `oc_newsblog_article_related` (
  `article_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  `type` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_to_category`
--

CREATE TABLE `oc_newsblog_article_to_category` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_category` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_article_to_category`
--

INSERT INTO `oc_newsblog_article_to_category` (`article_id`, `category_id`, `main_category`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_to_layout`
--

CREATE TABLE `oc_newsblog_article_to_layout` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_article_to_layout`
--

INSERT INTO `oc_newsblog_article_to_layout` (`article_id`, `store_id`, `layout_id`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_article_to_store`
--

CREATE TABLE `oc_newsblog_article_to_store` (
  `article_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_article_to_store`
--

INSERT INTO `oc_newsblog_article_to_store` (`article_id`, `store_id`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_category`
--

CREATE TABLE `oc_newsblog_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `settings` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_category`
--

INSERT INTO `oc_newsblog_category` (`category_id`, `image`, `parent_id`, `sort_order`, `status`, `date_added`, `date_modified`, `settings`) VALUES
(1, 'catalog/slider/xoma.jpg', 0, 0, 1, '2017-07-25 10:41:49', '2017-07-25 10:47:44', 'a:19:{s:11:\"date_format\";s:5:\"d.m.Y\";s:16:\"image_size_width\";s:2:\"80\";s:17:\"image_size_height\";s:2:\"80\";s:30:\"images_size_articles_big_width\";s:3:\"500\";s:31:\"images_size_articles_big_height\";s:3:\"500\";s:32:\"images_size_articles_small_width\";s:3:\"228\";s:33:\"images_size_articles_small_height\";s:3:\"228\";s:17:\"images_size_width\";s:3:\"228\";s:18:\"images_size_height\";s:3:\"228\";s:5:\"limit\";s:2:\"10\";s:15:\"show_in_sitemap\";i:1;s:24:\"show_in_sitemap_articles\";i:1;s:11:\"show_in_top\";i:0;s:20:\"show_in_top_articles\";i:0;s:12:\"show_preview\";i:0;s:7:\"sort_by\";s:16:\"a.date_available\";s:14:\"sort_direction\";s:4:\"desc\";s:16:\"template_article\";s:0:\"\";s:17:\"template_category\";s:0:\"\";}');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_category_description`
--

CREATE TABLE `oc_newsblog_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_category_description`
--

INSERT INTO `oc_newsblog_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_h1`, `meta_description`, `meta_keyword`) VALUES
(1, 3, 'Дикие животные', 'Дикие животные', '', '', '', ''),
(1, 1, 'Дикие животные', 'Дикие животные', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_category_path`
--

CREATE TABLE `oc_newsblog_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_category_path`
--

INSERT INTO `oc_newsblog_category_path` (`category_id`, `path_id`, `level`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_category_to_layout`
--

CREATE TABLE `oc_newsblog_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_category_to_layout`
--

INSERT INTO `oc_newsblog_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oc_newsblog_category_to_store`
--

CREATE TABLE `oc_newsblog_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `oc_newsblog_category_to_store`
--

INSERT INTO `oc_newsblog_category_to_store` (`category_id`, `store_id`) VALUES
(1, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `oc_newsblog_article`
--
ALTER TABLE `oc_newsblog_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Индексы таблицы `oc_newsblog_article_attribute`
--
ALTER TABLE `oc_newsblog_article_attribute`
  ADD PRIMARY KEY (`article_id`,`attribute_id`,`language_id`);

--
-- Индексы таблицы `oc_newsblog_article_description`
--
ALTER TABLE `oc_newsblog_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_newsblog_article_image`
--
ALTER TABLE `oc_newsblog_article_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Индексы таблицы `oc_newsblog_article_related`
--
ALTER TABLE `oc_newsblog_article_related`
  ADD PRIMARY KEY (`article_id`,`related_id`);

--
-- Индексы таблицы `oc_newsblog_article_to_category`
--
ALTER TABLE `oc_newsblog_article_to_category`
  ADD PRIMARY KEY (`article_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `oc_newsblog_article_to_layout`
--
ALTER TABLE `oc_newsblog_article_to_layout`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_newsblog_article_to_store`
--
ALTER TABLE `oc_newsblog_article_to_store`
  ADD PRIMARY KEY (`article_id`,`store_id`);

--
-- Индексы таблицы `oc_newsblog_category`
--
ALTER TABLE `oc_newsblog_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `oc_newsblog_category_description`
--
ALTER TABLE `oc_newsblog_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `oc_newsblog_category_path`
--
ALTER TABLE `oc_newsblog_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Индексы таблицы `oc_newsblog_category_to_layout`
--
ALTER TABLE `oc_newsblog_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Индексы таблицы `oc_newsblog_category_to_store`
--
ALTER TABLE `oc_newsblog_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `oc_newsblog_article`
--
ALTER TABLE `oc_newsblog_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `oc_newsblog_article_image`
--
ALTER TABLE `oc_newsblog_article_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `oc_newsblog_category`
--
ALTER TABLE `oc_newsblog_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
