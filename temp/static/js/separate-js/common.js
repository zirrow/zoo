(function () {
    var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
    $("img, a").on("dragstart", function(e) { e.preventDefault(); });

    $("input[name='phone']").inputmask("+38(999) 99-99-999");
    $("input[name='callback-phone']").inputmask("+38(999) 99-99-999");


    if (document.querySelector('.top-slider') !== null) {
        var topSlidesCount = document.querySelectorAll('.top-slider .swiper-slide').length,
            topSlidesButtons = document.querySelectorAll('.top-slider .swiper-button-prev, .top-slider .swiper-button-next');
        if (topSlidesCount < 2) {
            $(topSlidesButtons).css('display', 'none');
        }
        var topSlider = new Swiper('.top-slider .swiper-container', {
            speed: 400,
            prevButton: '.top-slider .swiper-button-prev',
            nextButton: '.top-slider .swiper-button-next',
            pagination: '.top-slider .swiper-pagination'
        });
    }


    if (document.querySelector('.standard__discount') !== null) {
        var standardDiscount = new Swiper('.standard__discount .swiper-container', {
            speed: 400,
            prevButton: '.standard__discount .swiper-button-prev',
            nextButton: '.standard__discount .swiper-button-next',
            slidesPerView: 4,
            breakpoints: {
                1200: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 2
                },
                600: {
                    slidesPerView: 1
                }
            }
        });
    }

    if (document.querySelector('.standard__recommend') !== null) {
        var standardRecommend = new Swiper('.standard__recommend .swiper-container', {
            speed: 400,
            prevButton: '.standard__recommend .swiper-button-prev',
            nextButton: '.standard__recommend .swiper-button-next',
            slidesPerView: 4,
            breakpoints: {
                1200: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 2
                },
                600: {
                    slidesPerView: 1
                }
            }
        });
    }

    if (document.querySelector('.standard__new') !== null) {
        var standardNew = new Swiper('.standard__new .swiper-container', {
            speed: 400,
            prevButton: '.standard__new .swiper-button-prev',
            nextButton: '.standard__new .swiper-button-next',
            slidesPerView: 4,
            breakpoints: {
                1200: {
                    slidesPerView: 3
                },
                992: {
                    slidesPerView: 2
                },
                600: {
                    slidesPerView: 1
                }
            }
        });
    }

    if (document.querySelector('.standard__complex') !== null) {
        var slidesCount = document.querySelectorAll('.standard__complex .swiper-slide').length,
            slidesButtons = document.querySelectorAll('.standard__complex .swiper-button-prev, .standard__complex .swiper-button-next');
        if (slidesCount < 2) {
            $(slidesButtons).css('display', 'none');
        }
        var standardComplex = new Swiper('.standard__complex .swiper-container', {
            speed: 400,
            prevButton: '.standard__complex .swiper-button-prev',
            nextButton: '.standard__complex .swiper-button-next'
        });
    }

    $('.personal__form').validate({
        rules: {
            login: {required: true, minlength: 3},
            password: {required: true}
       },
        messages: {
            login: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во символов 3'},
            password: {required: 'Это поле обязательно для заполнения'}
        }
    });

    // self phone methods
    $.validator.addMethod("minlenghtphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 11;
        },
        "Введите номер телефона");
    $.validator.addMethod("requiredphone", function (value, element) {
            return value.replace(/\D+/g, '').length > 1;
        },
        "Это поле обязательно для заполнения");

    $('#callback').validate({
        rules: {
            name: {required: true, minlength: 2},
            phone: {requiredphone: true, minlenghtphone: true}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимальное кол-во символов 2'}
        }
    });

    $('#subscribe').validate({
        rules: {
            email: {required: true, email: true}
        },
        messages: {
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'}
        }
    });

    $('#oneClick').validate({
        rules: {
            phone: {requiredphone: true, minlenghtphone: true}
        }
    });

    // Sticky Header
    function stickyMenu() {
        "use strict";
        var top_head_height = jQuery('.header__bottom').height(),
            header = $('.header');
        if (jQuery(window).scrollTop() > 230) {
            header.addClass("sticky");
            header.css("padding-top", top_head_height);
            jQuery('.header__menu').append($('#cart'));
        }
        else{
            header.removeClass("sticky");
            header.css("padding-top", "0");
            jQuery('.header__middle-inner').append($('#cart'));
        }
    }
    function stickyMobileMenu() {
        "use strict";
        var mobile_head_height = jQuery('.header__middle').height(),
            mobile_header = jQuery('.header');
        if (jQuery(window).scrollTop() > mobile_head_height) {
            mobile_header.addClass("sticky-mobile");
            mobile_header.css("padding-top", mobile_head_height);
        }
        else{
            mobile_header.removeClass("sticky-mobile");
            mobile_header.css("padding-top", "0");
        }
    }
    function stickyCartMenu() {
        "use strict";
        var mobile_head_height = jQuery('.header__middle').height(),
            mobile_header = jQuery('.header');
        if (jQuery(window).scrollTop() > mobile_head_height) {
            mobile_header.addClass("sticky-mobile");
            mobile_header.css("padding-top", mobile_head_height);
        }
        else{
            mobile_header.removeClass("sticky-mobile");
            mobile_header.css("padding-top", "0");
        }
    }
    var flag;
    function menuResize() {
        if(jQuery(window).width() > 992) {
            flag = true;
        } else {
            flag = false;
        }
    }
    menuResize();
    jQuery(window).resize(function () {
        menuResize();
    });
    jQuery(window).scroll(function() {
        if(flag === true && (!$('body').hasClass('cart-page'))) {
            stickyMenu();
        } else if (flag === false && (!$('body').hasClass('cart-page'))) {
            stickyMobileMenu();
        }
        if($('body').hasClass('cart-page')) {
            stickyCartMenu();
        }
    });

    var sideNav = {
        navTrigger:     jQuery('.hamburger, .side-navigation__close'),
        navContainer:   jQuery('.side-navigation'),
        pushLeft:       jQuery('.header__middle, .footer, .navigation'),
        navClose:       jQuery('.side-navigation__close, .hamburger')
    };

    function initializeSideNav() {
        "use strict";
        sideNav.navTrigger.on('click', function(e) {

            sideNav.navClose.toggleClass('is-active');
            sideNav.pushLeft.toggleClass('is-pushed-left');
            sideNav.navContainer.toggleClass('is-active');

            if (sideNav.navContainer.hasClass('overflow-hidden')) {
                sideNav.navContainer.removeClass('overflow-hidden');
            } else {
                sideNav.navContainer.addClass('overflow-hidden');
            }

            if( is_firefox ) {
                jQuery('.page__wrapper').toggleClass('is-pushed-left').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                    jQuery('body').toggleClass('overflow-hidden');
                });
            } else {
                jQuery('.page__wrapper').toggleClass('is-pushed-left');
                jQuery('body').toggleClass('overflow-hidden');
            }
            e.preventDefault();

        });
    }
    function mobileAccordion() {
        "use strict";
        var nav             = sideNav.navContainer;
        var allPanels       = nav.find('.side-navigation__sub-nav').hide();
        var clickableItems  = nav.find('.side-navigation__main-link');
        if(clickableItems.next('.side-navigation__sub-nav').length > 0) {
            clickableItems.next('.side-navigation__main-item-children').addClass('open');
        } else {
            clickableItems.next('.side-navigation__main-item-children').removeClass('open');
        }
        clickableItems.parent('.side-navigation__main-item-children.open').children('.side-navigation__sub-nav').slideDown();
        nav.find('.side-navigation__arrow').on('click', function (e) {
            e.stopPropagation();
            var element     = jQuery(this).parent(),
                is_open     = element.hasClass('open'),
                exclude     = element.parentsUntil('side-navigation__main-menu', '.side-navigation__sub-nav');
            allPanels.not(exclude).slideUp();
            nav.find('.open').not(exclude.parent().parent().find('.open')).removeClass('open');
            if(!is_open) {
                element.addClass('open').children('.side-navigation__sub-nav').slideDown();
            }
            return false;
        });
    }
    initializeSideNav();
    mobileAccordion();

    $(".count__input").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $('.count__min').on('click', min);
    $('.count__max').on('click', max);
    function min() {
        var input = $(this).parent().find('.count__input'),
            currentVal = parseInt(input.val()),
            newVal = (!isNaN(currentVal) && currentVal > 1) ? currentVal - 1 : 1;
        input.val(newVal);
        if(currentVal<3) {
            $(this).removeClass('count__min--active');
        }
    }
    function max() {
        var input = $(this).parent().find('.count__input'),
            currentVal = parseInt(input.val()),
            newVal = (!isNaN(currentVal)) ? currentVal + 1 : 1;
        input.val(newVal);
        if(currentVal>0) {
            $(this).parents('.count').find('.count__min').addClass('count__min--active');
        } else {
            $(this).parents('.count').find('.count__min').removeClass('count__min--active');
        }
    }

    if(document.querySelector('.easyzoom') !== null) {
        //slider
        var $easyzoom = $('.easyzoom').easyZoom();
        var api = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
        var block =  $('.product-page__slider-container a img');

        $('.thumbnail li a').on('click', function (e) {
            e.preventDefault();
            var src = $(this).data('standard');
            var src_easyzoom = $(this).data('easyzoom');
            $('.thumbnail li').removeClass('item--active');
            $(this).parent().addClass('item--active');
            block.fadeOut(300, function () {
                api.swap(src_easyzoom, src_easyzoom);
                block.attr('src', src);
            }).fadeIn(300);
        });
    }

    // self tabs
    var selfTabs = {
        title: jQuery('.self-tabs .self-tabs__title'),
        content: jQuery('.self-tabs .self-tabs__content')
    };
    function initializeSelfTabs() {
        "use strict";
        selfTabs.title.on('click', function(e) {
            var id = '#' + jQuery(this).data('id');
            if(!$(this).hasClass('self-tabs__title--active')) {
                selfTabs.title.removeClass('self-tabs__title--active');
                selfTabs.content.animate({"opacity" : 0}, 200).hide();
                $(this).addClass('self-tabs__title--active');
                $(id).show().animate({"opacity" : 1}, 200);
            }
            e.preventDefault();
        });
    }
    initializeSelfTabs();


    var reviewArea = $('.review__leave-textarea textarea');
    reviewArea.on('focusin', function () {
        $(this).parent().find('span').fadeOut('fast');
    });
    reviewArea.on('focusout', function () {
        $(this).parent().find('span').fadeIn('fast');
    });

    $('.review-rating label:not(.control-label)').on('click', ratings);

    function ratings() {
        $('.review-rating input[type=radio]').each(function () {
            var _self = $(this);
            if(_self.prop('checked') == true) {
                _self.parents('.review-rating').find('span:not(next)').css('color', '#66ca3b');
            } else {
                _self.siblings('span').css('color', '');
            }
        });
    }

    $('.aside-hide').on('click', function () {
       $('.aside').slideToggle('fast');
    });

    var productList = document.querySelectorAll('.product-list'),
        productGrid = document.querySelectorAll('.product-grid'),
        catalog_inner = document.querySelector('.catalog__content-inner'),
        btn_grid = document.querySelectorAll('.button-toggle--grid'),
        btn_list = document.querySelectorAll('.button-toggle--list');

    if ($(productList) !== null) {
        $(catalog_inner).css('margin', '0 0');
    }  if ($(productGrid) !== null) {
        $(catalog_inner).css('margin', '0 -10px');
    }

    if($(btn_list).hasClass('button-toggle--active')) {
        $(productGrid).animate({"opacity" : 0}, 200).hide();
    } else if($(btn_grid).hasClass('button-toggle--active')) {
        $(productList).animate({"opacity" : 0}, 200).hide();
    }

    $('.button-toggle').on('click', function () {
        if($(this).hasClass('button-toggle--list')) {
            $(btn_grid).removeClass('button-toggle--active');
            $(btn_list).addClass('button-toggle--active');
            $(productGrid).animate({"opacity" : 0}, 200).hide();
            $(productList).show().animate({"opacity" : 1}, 200);
            $(catalog_inner).css('margin', '0 0');
        } else {
            $(btn_list).removeClass('button-toggle--active');
            $(btn_grid).addClass('button-toggle--active');
            $(productList).animate({"opacity" : 0}, 200).hide();
            $(productGrid).show().animate({"opacity" : 1}, 200);
            $(catalog_inner).css('margin', '0 -10px');
        }
    });

    $(window).resize(function () {
        if($(window).width() < 600) {
            $(productList).animate({"opacity" : 0}, 200).hide();
            $(productGrid).show().animate({"opacity" : 1}, 200);
        }
    });
    if($(window).width() < 600) {
        $(productList).animate({"opacity" : 0}, 200).hide();
        $(productGrid).show().animate({"opacity" : 1}, 200);
    }

    $('.product-list__read').on('click', function (e) {
        $(this).parents('.product-list').find('.product-list__hidden-block').slideToggle('fast');
        e.preventDefault();
    });

    var inputs = document.querySelectorAll('.file-upload__input');
    Array.prototype.forEach.call(inputs, function(input) {
        var label    = input.nextElementSibling,
            labelVal = label.innerHTML;
        input.addEventListener('change', function(e) {
            var fileName = '';
            if(this.files && this.files.length > 1) {
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            }
            else {
                fileName = e.target.value.split('\\').pop();
            }
            if(fileName) {
                label.innerHTML = fileName;
            }
            else {
                label.innerHTML = labelVal;
            }
        });
    });

    $('.methods__btn').bind('click.smoothscroll', function (e) {
        $('#boxSave').fadeIn();

        var target = $(this).attr('href'),
            bl_top,
            header = $('.header');
        if(header.hasClass('sticky-mobile')) {
            bl_top = $(target).offset().top-100;
        } else {
            bl_top = $(target).offset().top-135;
        }

        $('html, body').animate({scrollTop: bl_top}, {
            duration: 700,
            easing: "swing"
        });
        e.preventDefault();
    });

    $('#cart ul.dropdown-menu').on('click', function (e) {
        e.stopPropagation();
    });

    if($(window).width() < 480) {
        $(".address ol").replaceWith(function(index, oldHTML){
            return $("<ul>").html(oldHTML);
        });
    } else {
        $(".address ul").replaceWith(function(index, oldHTML){
            return $("<ol>").html(oldHTML);
        });
    }

    $(window).resize(function () {
        if($(window).width() < 480) {
            $(".address ol").replaceWith(function(index, oldHTML){
                return $("<ul>").html(oldHTML);
            });
        } else {
            $(".address ul").replaceWith(function(index, oldHTML){
                return $("<ol>").html(oldHTML);
            });
        }
    });


    jQuery('ul.header__menu-root > li').hover(function() {
        jQuery(this).find('.header__menu-dropdown').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.header__menu-dropdown').stop(true, true).delay(200).fadeOut();
    });

    jQuery('ul.header__menu-dropdown > li').hover(function() {
        jQuery(this).find('.header__menu-dropdown-second').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.header__menu-dropdown-second').stop(true, true).delay(200).fadeOut();
    });







    $('.datepicker').datepicker();


    //personal-account validate
    $('#edit-you').validate({
        rules: {
            name: {required: true, minlength: 2},
            surname: {required: true, minlength: 2},
            midname: {required: true, minlength: 2},
            phone: {requiredphone: true, minlenghtphone: true},
            email: {required: true, email: true}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            surname: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            midname: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            email: {required: 'Это поле обязательно для заполнения', email: 'Введите почту'}
        }
    });

    $('#change-password').validate({
        rules: {
            name: {required: true, minlength: 2},
            password: {required: true, minlength: 5},
            rePassword: {required: true, minlength: 5, equalTo: '#pass'},
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            password: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 5 символов'},
            rePassword: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 5 символов', equalTo: 'Введите пароль как в поле выше'}
        }
    });


    $('#address').validate({
        rules: {
            name: {required: true, minlength: 2},
            surname: {required: true, minlength: 2},
            city: {required: true, minlength: 2},
            phone: {requiredphone: true, minlenghtphone: true},
            address: {required: true, minlength: 5}
        },
        messages: {
            name: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            surname: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            city: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 2 символа'},
            address: {required: 'Это поле обязательно для заполнения', minlength: 'Минимум 5 символа'}
        }
    });



    var product  = $('.product__layout, .product-grid__layout'),
        hidden = product.find('.product-grid__hidden'),
        hiddenItem = product.find('.product-grid__hidden-item'),
        discount = product.find('.product-grid__hidden-cost-sale');

    $('.product-grid__select').on('click', function () {
        $(this).find('.product-grid__hidden').show();
    });
    function optionCombinate() {
        var hiddenCurrCost = $(this).find('.product-grid__hidden-cost-current').text(),
            hiddenSaleCost = $(this).find('.product-grid__hidden-cost-sale').text(),
            hiddenNewCost = $(this).find('.product-grid__hidden-cost-new').text(),
            hiddenOldCost = $(this).find('.product-grid__hidden-cost-old').text(),
            hiddenName = $(this).find('.product-grid__hidden-name').text(),
            costBlock = $(this).parents('.product-grid, .product').find('.product-grid__cost');

        costBlock.find('span').remove();

        if($(this).has('.product-grid__hidden-cost-current').size()) {
            costBlock.append('<span class="product-grid__cost-price product__cost-price--current">' + hiddenCurrCost + '</span>');
        } else {
            costBlock.append('<span class="product-grid__cost-price product__cost-price--new">' + hiddenNewCost + '</span>');
            costBlock.append('<span class="product-grid__cost-price product__cost-price--old">' + hiddenOldCost + '</span>');
            costBlock.append('<span class="product-grid__cost-discount">' + hiddenSaleCost + '</span>');
        }
        $(this).parents('.product, .product-grid').find('.product-grid__select-current').text(hiddenName);

        $(this).parents('.product-grid__hidden').hide();
    }

    hiddenItem.on('click',optionCombinate);


    product.on('mouseleave',function () {
        $(this).find('.product-grid__hidden').hide();
    });


    var item = $('.header__menu-dropdown > li'),
        link = $('.header__menu-dropdown a');
    $.each(item, function (index,element) {
       if($(element).has('.header__menu-dropdown-second').size()) {
           $(element).find('>a').addClass('hasDropdown');
        } else {
           $(element).find('.header__menu-dropdown a').removeClass('hasDropdown');
       }
    });


}());
