<?php
// Heading
$_['heading_title']      	= 'Стикеры';

// Text
$_['text_success']          = 'Стикеры успешно обновлены!';
$_['text_list']          	= 'Список стикеров';
$_['text_add']          	= 'Добавление стикеров';
$_['text_edit']          	= 'Редактирование стикеров';
$_['text_default']          = 'По умолчанию';

// Column
$_['column_name']          	= 'Название стикера';
$_['column_status']         = 'Статус';
$_['column_action']         = 'Действие';

// Entry
$_['entry_name']          	= 'Название стикера:';
$_['entry_title']          	= 'Заголовок:';
$_['entry_link']          	= 'Ссылка:';
$_['entry_image']          	= 'Изображение:';
$_['entry_status']          = 'Статус:';
$_['entry_sort_order']      = 'Сортировка';

// Error
$_['error_permission']      = 'У Вас нет прав для изменения стикеров!';
$_['error_name']          	= 'Название стикера должно быть от 3 до 64 символов!';
$_['error_title']          	= 'Заголовок должен быть от 2 до 64 символов!';