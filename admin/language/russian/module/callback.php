<?php
// Heading
$_['heading_title']    = 'Обратный звонок';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified SpecialForGroup module!';
$_['text_edit']        = 'Редактирование модуля';
$_['text_name']        = 'Название';

// Entry
$_['entry_status']     = 'Статус:';
$_['select_group']     = 'Группа:';
$_['group_text']       = 'Текст:';
// Error
$_['error_permission'] = 'Warning: You do not have permission to modify specialForGroup module!';