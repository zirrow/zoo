<?php
// Heading
$_['heading_title']    = 'Просмотренные';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Success: You have modified featured module!';
$_['text_edit']        = 'Редактирвоание модуля "Просмотернные"';

// Entry
$_['entry_limit']      = 'Лимит';
$_['entry_name']       = 'Название';
$_['entry_image']      = 'Image (W x H) and Resize Type';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify featured module!';
$_['error_height']     = 'Image height dimensions required!';
$_['error_width']      = 'Image width dimensions required!';
$_['error_name']       = 'Name of module is required';