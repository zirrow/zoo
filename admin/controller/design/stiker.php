<?php
class ControllerDesignStiker extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('design/stiker');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/stiker');

		$this->getForm();
	}

	public function edit() {
		$this->load->language('design/stiker');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('design/stiker');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_design_stiker->editStikers($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		}

		$this->getForm();
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['banner_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_link'] = $this->language->get('entry_link');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_banner_add'] = $this->language->get('button_banner_add');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['stiker_image'])) {
			$data['error_stiker_image'] = $this->error['stiker_image'];
		} else {
			$data['error_stiker_image'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('design/stiker', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['action'] = $this->url->link('design/stiker/edit', 'token=' . $this->session->data['token'] . $url, 'SSL');


		$stikers_info = $this->model_design_stiker->getStikers();

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('tool/image');

		$data['stiker_images'] = array();

		foreach ($stikers_info as $stiker_image) {
			if (is_file(DIR_IMAGE . $stiker_image['image'])) {
				$image = $stiker_image['image'];
				$thumb = $stiker_image['image'];
			} else {
				$image = '';
				$thumb = 'no_image.png';
			}

			$data['stiker_images'][] = array(
				'stiker_id'                => $stiker_image['stiker_id'],
				'stiker_image_description' => $stiker_image['stiker_image_description'],
				'image'                    => $image,
				'thumb'                    => $this->model_tool_image->resize($thumb, 100, 100),
				'sort_order'               => $stiker_image['sort_order'],
				'text_on'                  => $stiker_image['text_on']
			);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('design/stiker_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'design/stiker')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (isset($this->request->post['stiker_image'])) {
			foreach ($this->request->post['stiker_image'] as $stiker_image_id => $stiker_image) {
				foreach ($stiker_image['stiker_image_description'] as $language_id => $stiker_image_description) {
					if ((utf8_strlen($stiker_image_description['title']) < 2) || (utf8_strlen($stiker_image_description['title']) > 64)) {
						$this->error['stiker_image'][$stiker_image_id][$language_id] = $this->language->get('error_title');
					}
				}
			}
		}

		return !$this->error;
	}
}