<?php
class ControllerModuleSpecialForGroup extends Controller {
    private $error;

	public function index() {
		$this->language->load('module/specialForGroup');

		$this->load->model('setting/setting');
        $this->load->model('module/specialForGroup');
        $this->load->model('customer/customer_group');
        $this->load->model('localisation/language');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('specialForGroup', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
            $this->model_module_specialForGroup->editSpecialForGroup($this->request->post);

			//echo '<pre>';
			//print_r($this->request->post);
			//echo '</pre>';
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
        $data['token'] = $this->session->data['token'];

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
        $data['select_group'] = $this->language->get('select_group');
        $data['group_text'] = $this->language->get('group_text');


        //CKEditor
        if ($this->config->get('config_editor_default')) {
            $this->document->addScript('view/javascript/ckeditor/ckeditor.js');
            $this->document->addScript('view/javascript/ckeditor/ckeditor_init.js');
        }
        $data['ckeditor'] = $this->config->get('config_editor_default');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/specialForGroup', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/specialForGroup', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['specialForGroup_status'])) {
			$data['specialForGroup_status'] = $this->request->post['specialForGroup_status'];
		} else {
			$data['specialForGroup_status'] = $this->config->get('specialForGroup_status');
		}

        $data['languages'] = $this->model_localisation_language->getLanguages();
        $this->load->model('localisation/language');
        $data['lang'] = $this->language->get('lang');

        $filter_data = array();
        $results = $this->model_customer_customer_group->getCustomerGroups($filter_data);

        foreach ($results as $result) {
            $data['customer_groups'][] = array(
                'customer_group_id' => $result['customer_group_id'],
                'discount_percent' => $result['discount_percent'],
                'discount_threshold' => $result['discount_threshold'],
                'name'              => $result['name'] . (($result['customer_group_id'] == $this->config->get('config_customer_group_id')) ? $this->language->get('text_default') : null),
                'sort_order'        => $result['sort_order'],
                'discount_percent' => $result['discount_percent'],
                'discount_threshold' => $result['discount_threshold'],
            );
        }

        $data['groups'] = array();

        $results = $this->model_module_specialForGroup->getGroups();
        foreach ($results as $result) {
            $language = array();

            $descriptions = $this->model_module_specialForGroup->getGroupsDescription($result['id']);

            foreach($descriptions as $description)
            {
                $language[$description['language']] = array(
                    'description' => $description['description']
                );
            }

            $data['groups'][] = array(
                'id' => $result['id'],
                'language' => $language,
                'id_group' => $result['id_group'],
            );
        }

        //echo '<pre>';
        //print_r($data['groups']);
        //echo '</pre>';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/specialForGroup.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/specialForGroup')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}