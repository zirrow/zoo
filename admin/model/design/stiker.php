<?php
class ModelDesignStiker extends Model {

	public function editStikers($data) {
		$this->event->trigger('pre.admin.stiker.edit', $data);

		$id_arr = array();

		$sql = "SELECT `stiker_id` FROM " . DB_PREFIX . "stiker";
		$ids = $this->db->query($sql);

		if(!empty($ids->rows)){
			foreach ($ids->rows as $key => $id){
				$id_arr[] = $id['stiker_id'];
			}
		}

		if(isset($data['stiker_image'])){
			foreach ($data['stiker_image'] as $stiker_image){

				if (!isset($stiker_image['text_on'])){
					$stiker_image['text_on'] = 0;
				}

				if (isset($stiker_image['stiker_id'])){

					if (in_array($stiker_image['stiker_id'],$id_arr)){

						$this->editStiker($stiker_image);

						$id_arr = array_flip($id_arr);
						unset ($id_arr[$stiker_image['stiker_id']]);
						$id_arr = array_flip($id_arr);

					}

				} else {
					$this->addStiker($stiker_image);
				}

			}
		}

		if(!empty($id_arr)){

			foreach ($id_arr as $kill_id){

				$this->db->query("DELETE FROM " . DB_PREFIX . "stiker WHERE stiker_id = '" . (int)$kill_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "stiker_description WHERE stiker_id = '" . (int)$kill_id . "'");

			}

		}

		$this->event->trigger('post.admin.stiker.edit');
	}

	public function addStiker($addStiker) {
		$this->event->trigger('pre.admin.stiker.add', $addStiker);

		if (!isset($addStiker['text_on'])){
			$addStiker['text_on'] = 0;
		}

		if (!isset($addStiker['sort_order'])){
			$addStiker['sort_order'] = 1;
		}

		$this->db->query("INSERT INTO " . DB_PREFIX . "stiker SET image = '" . $this->db->escape($addStiker['image']) . "', sort_order = '" . (int)$addStiker['sort_order'] . "', text_on = '" . (int)$addStiker['text_on'] . "'");

		$stiker_id = $this->db->getLastId();

		if (isset($addStiker['stiker_image_description'])) {

			foreach ($addStiker['stiker_image_description'] as $language_id => $stiker_image_description) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "stiker_description SET language_id = '" . (int)$language_id . "', stiker_id = '" . (int)$stiker_id . "', title = '" .  $this->db->escape($stiker_image_description['title']) . "'");
			}

		}

		$this->event->trigger('post.admin.stiker.add', $stiker_id);

		return $stiker_id;
	}

	public function editStiker($editStiker){

		$this->db->query("UPDATE " . DB_PREFIX . "stiker SET image = '" . $this->db->escape($editStiker['image']) . "', sort_order = '" . (int)$editStiker['sort_order'] . "', text_on = '" . (int)$editStiker['text_on'] . "' WHERE stiker_id = '" . (int)$editStiker['stiker_id'] . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "stiker_description WHERE stiker_id = '" . (int)$editStiker['stiker_id'] . "'");

		if (isset($editStiker['stiker_image_description'])) {

			foreach ($editStiker['stiker_image_description'] as $language_id => $stiker_image_description) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "stiker_description SET language_id = '" . (int)$language_id . "', stiker_id = '" . (int)$editStiker['stiker_id'] . "', title = '" .  $this->db->escape($stiker_image_description['title']) . "'");
			}

		}
	}

	public function getStikers(){
		$stiker_image_data = array();
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "stiker";
		$stikers_arr = $this->db->query($sql);

		foreach ($stikers_arr->rows as $stiker_image) {
			$stiker_image_description_data = array();

			$sql = "SELECT * FROM " . DB_PREFIX . "stiker_description WHERE stiker_id = '" . (int)$stiker_image['stiker_id'] . "'";

			$stiker_image_description_query = $this->db->query($sql);

			foreach ($stiker_image_description_query->rows as $stiker_image_description) {
				$stiker_image_description_data[$stiker_image_description['language_id']] = array('title' => $stiker_image_description['title']);
			}

			$stiker_image_data[] = array(
				'stiker_id'                => $stiker_image['stiker_id'],
				'stiker_image_description' => $stiker_image_description_data,
				'image'                    => $stiker_image['image'],
				'sort_order'               => $stiker_image['sort_order'],
				'text_on'                  => $stiker_image['text_on']
			);
		}

		return $stiker_image_data;

	}
}