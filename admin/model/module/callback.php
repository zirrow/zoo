<?php
class ModelModuleSpecialForGroup extends Model {
	public function getGroups() {

		$sql = "SELECT * FROM " . DB_PREFIX . "SpecialForGroup" ;

        $query = $this->db->query($sql);
		return $query->rows;
	}

	public function getGroupsDescription($SpecialForGroup_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "SpecialForGroup_Description WHERE SpecialForGroup_id = '" .$SpecialForGroup_id. "'" ;

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function editSpecialForGroup($data)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "SpecialForGroup");
        $this->db->query("DELETE FROM " . DB_PREFIX . "SpecialForGroup_Description");

        foreach($data['SpecialForGroup'] as $group) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "SpecialForGroup SET id_group = '" . $group['code'] . "'");
            $group_id = $this->db->getLastId();
            foreach ($group['language'] as $key => $language)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialforgroup_description SET SpecialForGroup_id = '" . $group_id . "', language = '" . $key . "', description = '" . $language['description'] . "'");
            }
        }
    }
}