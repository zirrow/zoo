<?php
class ModelModuleSpecialForGroup extends Model {
	public function getGroups() {

		$sql = "SELECT * FROM " . DB_PREFIX . "specialforgroup" ;

        $query = $this->db->query($sql);
		return $query->rows;
	}

	public function getGroupsDescription($SpecialForGroup_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "specialforgroup_description WHERE specialforgroup_id = '" .$SpecialForGroup_id. "'" ;

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function editSpecialForGroup($data)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialforgroup");
        $this->db->query("DELETE FROM " . DB_PREFIX . "specialforgroup_description");

        foreach($data['SpecialForGroup'] as $group) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "specialforgroup SET id_group = '" . $group['code'] . "'");
            $group_id = $this->db->getLastId();
            foreach ($group['language'] as $key => $language)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "specialforgroup_description SET specialforgroup_id = '" . $group_id . "', language = '" . $key . "', description = '" . $language['description'] . "'");
            }
        }
    }
}