<?php
class ModelCustomerCustomerGroup extends Model {
	public function addCustomerGroup($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group SET approval = '" . (int)$data['approval'] . "', sort_order = '" . (int)$data['sort_order'] . "', discount_percent = '" . (int)$data['discount_percent'] . "', discount_threshold = '" . (int)$data['discount_threshold'] . "'");

		$customer_group_id = $this->db->getLastId();

		foreach ($data['customer_group_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->refreshSpecialsById($customer_group_id);
	}

	public function editCustomerGroup($customer_group_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_group SET approval = '" . (int)$data['approval'] . "', sort_order = '" . (int)$data['sort_order'] . "', discount_percent = '" . (int)$data['discount_percent'] . "', discount_threshold = '" . (int)$data['discount_threshold'] . "' WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		foreach ($data['customer_group_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_group_description SET customer_group_id = '" . (int)$customer_group_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->refreshSpecialsById($customer_group_id);

	}

	public function deleteCustomerGroup($customer_group_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE customer_group_id = '" . (int)$customer_group_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . (int)$customer_group_id . "'");
	}

	public function getCustomerGroup($customer_group_id) {

		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customer_group cg 
					LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) 
				WHERE cg.customer_group_id = '" . (int)$customer_group_id . "' 
					AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);

		return $query->row;
	}

	public function getCustomerGroups($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		$sort_data = array(
			'cgd.name',
			'cg.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY cgd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCustomerGroupDescriptions($customer_group_id) {
		$customer_group_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id = '" . (int)$customer_group_id . "'");

		foreach ($query->rows as $result) {
			$customer_group_data[$result['language_id']] = array(
				'name'        => $result['name'],
				'description' => $result['description']
			);
		}

		return $customer_group_data;
	}

	public function getTotalCustomerGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_group");

		return $query->row['total'];
	}

	public function refreshAllDiscount(){

		$sql = "DELETE FROM " . DB_PREFIX . "product_discount WHERE 1";

		$this->db->query($sql);

		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group";

		$group = $this->db->query($sql);

		$sql = "SELECT product_id, price FROM " . DB_PREFIX . "product";

		$product = $this->db->query($sql);

		if ($group->rows){

			foreach ($group->rows as $group_value){

				foreach ($product->rows as $product_value){

					$sql = "INSERT INTO " . DB_PREFIX . "product_discount 
								SET 
									`product_id` = '" . $product_value['product_id'] . "'
									, `customer_group_id` = '" . $group_value['customer_group_id'] . "'
									, `quantity` = '1'
									, `priority` = '9'
									, `price` = '". $product_value['price'] * ((100 - $group_value['discount_percent']) / 100) ."'";

					$this->db->query($sql);

				}

			}

		}

        /* start related options table */
        $sql = "DELETE FROM " . DB_PREFIX . "relatedoptions_discount";
        $this->db->query($sql);

        $sql = "SELECT * FROM " . DB_PREFIX . "relatedoptions GROUP BY relatedoptions_id";
        $relatedoptions = $this->db->query($sql);

        if ($group->rows) {

            foreach ($group->rows as $group_value) {

                foreach ($relatedoptions->rows as $relatedoption) {
                    $sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_discount 
								SET 
									`relatedoptions_id` = '" . $relatedoption['relatedoptions_id'] . "',
									`customer_group_id` = '" . $group_value['customer_group_id'] . "',
									`quantity`          = '1',
									`priority`          = '0',
									`price`             = '" . $relatedoption['price'] * ((100 - $group_value['discount_percent']) / 100) . "'";

                    $this->db->query($sql);
                }
            }
        }
        /* end related options table */

		return true;

	}

	public function refreshSpecialsById($id){

		$sql = "DELETE FROM " . DB_PREFIX . "product_discount WHERE customer_group_id = '" . $id ."'";

		$this->db->query($sql);

		$sql = "SELECT * FROM " . DB_PREFIX . "customer_group WHERE customer_group_id = '" . $id ."'";

		$group = $this->db->query($sql);

		$sql = "SELECT product_id, price FROM " . DB_PREFIX . "product";

		$product = $this->db->query($sql);

		if ($group->rows){

			foreach ($group->rows as $group_value){

				foreach ($product->rows as $product_value){

					$sql = "INSERT INTO " . DB_PREFIX . "product_discount 
								SET 
									`product_id` = '" . $product_value['product_id'] . "'
									, `customer_group_id` = '" . $group_value['customer_group_id'] . "'
									, `quantity` = '1'
									, `priority` = '9'
									, `price` = '". $product_value['price'] * ((100 - $group_value['discount_percent']) / 100) ."'";

					$this->db->query($sql);

				}

			}

		}

        /* start related options table */
        $sql = "DELETE FROM " . DB_PREFIX . "relatedoptions_discount WHERE customer_group_id = '" . $id ."'";
        $this->db->query($sql);

        $sql = "SELECT * FROM " . DB_PREFIX . "relatedoptions GROUP BY relatedoptions_id";
        $relatedoptions = $this->db->query($sql);

        foreach ($relatedoptions->rows as $relatedoption){
            $sql = "INSERT INTO " . DB_PREFIX . "relatedoptions_discount 
								SET 
									`relatedoptions_id` = '" . $relatedoption['relatedoptions_id'] . "',
									`customer_group_id` = '" . $id . "',
									`quantity`          = '1',
									`priority`          = '0',
									`price`             = '". $relatedoption['price'] * ((100 - $group_value['discount_percent']) / 100) ."'";

            $this->db->query($sql);
        }
        /* end related options table */

		return true;

	}
}