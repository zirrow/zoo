<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-pp-layout" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-pp-layout" class="form-horizontal">
          <div class="form-group">

            <table id="module" class="table table-striped table-bordered table-hover">
              <thead>
              <tr>
                <td class="text-left"><?php echo $select_group; ?></td>
                <td class="text-left"><?php echo $group_text; ?></td>
                <td></td>
              </tr>
              </thead>
              <tbody>
                <?php $module_row = 0; ?>
                <?php foreach ($groups as $group) { ?>
                  <tr id="module-row<?php echo $module_row; ?>">
                    <td class="text-left">
                      <select name="SpecialForGroup[<?php echo $module_row; ?>][code]" class="form-control">
                        <?php foreach($customer_groups as $customer_group){ ?>
                          <?php if($group['id_group'] == $customer_group['customer_group_id']){ ?>
                            <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php }else{ ?>
                            <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                        <?php } ?>
                      </select>
                    </td>
                    <td class="text-left">
                      <?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <textarea name="SpecialForGroup[<?php echo $module_row; ?>][language][<?php echo $language['language_id']; ?>][description]" placeholder="" id="input-description<?php echo $module_row; ?><?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($group['language'][$language['language_id']]['description']) ? $group['language'][$language['language_id']]['description'] : ''; ?></textarea>
                        </div>
                      <?php } ?>
                    </td>
                    <td class="text-left"><button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                  </tr>
                <?php $module_row++; ?>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td class="text-left"><button type="button" id="addModule" data-toggle="tooltip" title="Add module" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>

            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="specialForGroup_status" id="input-status" class="form-control">
                <?php if ($specialForGroup_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
    <?php $module_row = 0; ?>
    <?php foreach ($groups as $group) { ?>
      <?php foreach ($languages as $language) { ?>
        <?php if ($ckeditor) { ?>
              ckeditorInit('input-description<?php echo $module_row; ?><?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
          <?php } else { ?>
              $('#input-description<?php echo $module_row; ?><?php echo $language['language_id']; ?>').summernote({height: 100, lang:'<?php echo $lang; ?>'});
          <?php } ?>
      <?php } ?>
      <?php $module_row++; ?>
    <?php } ?>
//--></script>

<script type="text/javascript"><!--
  var module_row = <?php echo $module_row; ?>;

  $('#addModule').on('click', function(){

      html  = '<tr id="module-row' + module_row + '">';
      html  += '    <td class="text-left">';
      html  += '      <select name="SpecialForGroup[' + module_row + '][code]" class="form-control">';
      <?php foreach($customer_groups as $customer_group){ ?>
          html  += '            <option value="<?php echo $customer_group["customer_group_id"] ?>"><?php echo $customer_group["name"]; ?></option>';
      <?php } ?>
      html  += '      </select>';
      html  += '    </td>';
      html  += '    <td class="text-left">';
      <?php foreach ($languages as $language) { ?>
          html  += '        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language["image"]; ?>" title="<?php echo $language["name"]; ?>" /></span>';
          html  += '          <textarea name="SpecialForGroup[' + module_row + '][language][<?php echo $language['language_id']; ?>][description]" placeholder="" id="input-description' + module_row + '<?php echo $language['language_id']; ?>" class="form-control"></textarea>';
          html  += '        </div>';
      <?php } ?>
      html  += '    </td>';
      html  += '  <td class="text-left"><button type="button" onclick="$(\'#module-row' + module_row + '\').remove();" data-toggle="tooltip" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
      html  += '</tr>';

      $('#module tbody').append(html);

      <?php foreach ($languages as $language) { ?>
          <?php if ($ckeditor) { ?>
              ckeditorInit('input-description' + module_row + '<?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
          <?php } else { ?>
              $('#module tbody #input-description' + module_row + '<?php echo $language['language_id']; ?>').summernote({height: 100, lang:'<?php echo $lang; ?>'});
          <?php } ?>
      <?php } ?>

      module_row++;

  });
//--></script>


<?php echo $footer; ?>