<form id="form-<?php echo $link; ?><?php echo $product_id; ?>">
 <?php if ($product_id == -1) { ?>
 <p><span class="be-help"><label><input name="none[<?php echo $link; ?>]" type="checkbox" /> <?php echo $text_not_contain; ?> > <?php echo ${'text_' . $link}; ?></label></span></p>
 <?php } ?>
 <?php if ($product_id > 0) { ?>
 <table class="be-form">
  <tr>
   <td width="1%"><img src="<?php echo $product_image; ?>" /></td>
   <td width="99%"><h3><?php echo $product_name; ?></h3></td>
  </tr>
 </table>
 <?php } ?>
 <table class="be-list">
  <tfoot>
   <tr>
    <td class="left">
     <?php if ($ro_installed) { ?>
     <div class="tab-pane" id="tab-related_options">
      <div class="tab-content" id="ro_content<?php echo $product_id; ?>">
       <input type="hidden" name="ro_data_included" value="1">
      </div>
      <span class="help-block" style="margin-top: 30px;"><?php echo $entry_ro_version.": ".$ro_version; ?> | <?php echo $text_ro_support; ?> | <?php echo $text_extension_page; ?></span><!---->
     </div>
    <?php } ?>
    </td>
   </tr>
   <tr>
    <td class="center">
     <?php if ($product_id > -1) { ?>
     <input id="product-copy-data-product_name-<?php echo $product_id; ?>" type="text" />
     <input id="product-copy-data-product_id-<?php echo $product_id; ?>" type="hidden" />
     <a class="btn btn-primary btn-sm" onclick="copyProductData('<?php echo $product_id; ?>', '<?php echo $link; ?>');" title="<?php echo $button_copy; ?>" style="margin-right:50px;"><i class="fa fa-copy"></i></a>
     <?php } ?>
     <?php if ($product_id == -1) { ?>
     <a class="btn btn-primary" onclick="setLinkFilter('<?php echo $link; ?>');"><?php echo $button_add_to_filter; ?></a>
     <a class="btn btn-danger" onclick="delLinkFilter('<?php echo $link; ?>');"><?php echo $button_remove_from_filter; ?></a>
     <?php } ?>
     <?php if ($product_id == 0) { ?>
     <a class="btn btn-primary" onclick="editLink('<?php echo $link; ?>', 'add', <?php echo $product_id; ?>);"><?php echo $button_insert_sel; ?></a>
     <a class="btn btn-primary" onclick="editLink('<?php echo $link; ?>', 'del', <?php echo $product_id; ?>);"><?php echo $button_delete_sel; ?></a>
     <a class="btn btn-primary" onclick="editLink('<?php echo $link; ?>', 'upd', <?php echo $product_id; ?>);"><?php echo $text_edit; ?></a>
     <?php } ?>
     <?php if ($product_id > 0) { ?>
     <a class="btn btn-primary" onclick="listProductLink('<?php echo $product_id; ?>', '<?php echo $link; ?>', 'prev');" title="<?php echo $button_prev; ?>"><i class="fa fa-chevron-left"></i></a>
     <a class="btn btn-primary" onclick="listProductLink('<?php echo $product_id; ?>', '<?php echo $link; ?>', 'next');" title="<?php echo $button_next; ?>" style="margin-right:50px;"><i class="fa fa-chevron-right"></i></a>
     <a class="btn btn-success" onclick="editLink('<?php echo $link; ?>', 'upd', <?php echo $product_id; ?>);"><?php echo $button_save; ?></a>
     <a class="btn btn-danger" onclick="$('#dialogLink').modal('hide');" title="<?php echo $button_close; ?>">&times;</a>
     <?php } ?>
    </td>
   </tr>
  </tfoot>
 </table>
</form>
<script type="text/javascript"><!--
if (typeof ro_tabs_cnt == 'undefined') {
	var ro_tabs_cnt = [];
}

ro_tabs_cnt[<?php echo $product_id; ?>] = 0;

if (typeof ro_counter == 'undefined') {
	var ro_counter = [];
}

ro_counter[<?php echo $product_id; ?>] = 0;

if (typeof ro_discount_counter == 'undefined') {
	var ro_discount_counter = [];
}

ro_discount_counter[<?php echo $product_id; ?>] = 0;

if (typeof ro_special_counter == 'undefined') {
	var ro_special_counter = [];
}

ro_special_counter[<?php echo $product_id; ?>] = 0;

if (typeof ro_variants == 'undefined') {
	var ro_variants = [];
}

ro_variants[<?php echo $product_id; ?>] = [];

if (typeof ro_all_options == 'undefined') {
	var ro_all_options = [];
}

ro_all_options[<?php echo $product_id; ?>] = <?php echo json_encode($ro_all_options); ?>;

if (typeof ro_settings == 'undefined') {
	var ro_settings = [];
}

ro_settings[<?php echo $product_id; ?>] = <?php echo json_encode($ro_settings); ?>;

if (typeof ro_variants == 'undefined') {
	var ro_variants = [];
}

ro_variants[<?php echo $product_id; ?>] = <?php echo json_encode($variants_options['vopts']); ?>;

if (typeof ro_variants_sorted == 'undefined') {
	var ro_variants_sorted = [];
}

ro_variants_sorted[<?php echo $product_id; ?>] = <?php echo json_encode($variants_options['sorted']); ?>;

if (typeof ro_data == 'undefined') {
	var ro_data = [];
}

ro_data[<?php echo $product_id; ?>] = <?php echo json_encode($ro_data); ?>;

if (ro_variants[<?php echo $product_id; ?>].length == 0) {
	$('#form-<?php echo $link; ?><?php echo $product_id; ?>').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $text_ro_set_options_variants; ?><button type="button" class="close" data-dismiss="alert">&times;</button></div>');
}

// ROPRO
function ro_tab_name_change(product_id, ro_tabs_num) {
	if ($('#form-<?php echo $link; ?>' + product_id + ' #ro-use-' + ro_tabs_num + '').is(':checked')) {
		var new_tab_name = $('#form-<?php echo $link; ?>' + product_id + ' #rov-' + ro_tabs_num + ' option[value="' + $('#form-<?php echo $link; ?>' + product_id + ' #rov-' + ro_tabs_num).val() + '"]').html();
	} else {
		var new_tab_name = '<?php echo addslashes($related_options_title); ?>';
	}
	
	$('#form-<?php echo $link; ?>' + product_id + ' #ro_nav_tabs a[data-ro-cnt=\'' + ro_tabs_num + '\']').html(new_tab_name);
}

function ro_add_tab(product_id, tab_data_param) {
	var tab_data = tab_data_param ? tab_data_param : false;
	
	html = '<div id="tab-ro-' + ro_tabs_cnt[product_id]+'" data-ro-cnt="' + ro_tabs_cnt[product_id] + '">' + ro_tabs_cnt[product_id] + '</div>';
	
	$('#form-<?php echo $link; ?>' + product_id + ' #ro_content' + product_id ).append(html);
	
	html = '';
	html+= '<input type="hidden" name="ro_data[' + ro_tabs_cnt[product_id] + '][rovp_id]" value="' + (tab_data['rovp_id'] ? tab_data['rovp_id'] : '0')+'">';
	html+= '<div class="form-group">';
	html+= '<label class="col-sm-2 control-label"><?php echo addslashes($entry_ro_use); ?></label>';
	html+= '<div class="col-sm-10">';
	html+= '<label class="radio-inline">';
	html+= '<input type="radio" name="ro_data[' + ro_tabs_cnt[product_id]+'][use]" id="ro-use-'+ro_tabs_cnt[product_id]+'" value="1" '+((tab_data['use'])?('checked'):(''))+' onchange="ro_use_check(' + product_id + ', ' + ro_tabs_cnt[product_id] + ');" />';
	html+= ' <?php echo $text_yes; ?>';
	html+= '</label>';
	html+= '<label class="radio-inline">';
	html+= '<input type="radio" name="ro_data[' + ro_tabs_cnt[product_id] + '][use]" value="" ' + ((tab_data['use']) ? (''):('checked')) + ' onchange="ro_use_check(' + product_id + ', ' + ro_tabs_cnt[product_id] + ');" />';
	html+= ' <?php echo $text_no; ?>';
	html+= '</label>';
	html+= '</div>';
	
	html+= '</div>';
	
	html+= '<div id="ro-use-data-' + ro_tabs_cnt[product_id] + '">';
	html+= '<div class="form-group">';
	html+= '<label class="col-sm-2 control-label" for="rov-' + ro_tabs_cnt[product_id] + '" ><?php echo $entry_ro_variant; ?></label>';
	html+= '<div class="col-sm-3" >';
	html+= '<select name="ro_data['+ro_tabs_cnt[product_id]+'][rov_id]" id="rov-'+ro_tabs_cnt[product_id]+'" class="form-control" onChange="ro_tab_name_change(' + product_id + ', ' + ro_tabs_cnt[product_id] + ');">';
	
	if (ro_settings[product_id]['ro_use_variants']) {
		for (var i in ro_variants_sorted[product_id]) {
			var ro_variant = ro_variants_sorted[product_id][i];
			
			if (ro_variant['rov_id'] == 0) {
				html+= '<option value="0"><?php echo addslashes($text_ro_all_options); ?></option>';
			} else {
				html+= '<option value="'+ro_variant['rov_id']+'" '+(tab_data['rov_id'] && tab_data['rov_id'] == ro_variant['rov_id'] ? 'selected':'')+' >'+ro_variant['name']+'</option>';
			}
		}
	} else {
		html+= '<option value="0"><?php echo addslashes($text_ro_all_options); ?></option>';
	}
	
	html+= '</select>';
	html+= '</div>';
	html+= '<button type="button" onclick="ro_fill_all_combinations(' + product_id + ', '+ro_tabs_cnt[product_id]+');" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_all_variants); ?>"><?php echo addslashes($entry_add_all_variants); ?></button>';
	html+= '<button type="button" onclick="ro_fill_all_combinations(' + product_id + ', '+ro_tabs_cnt[product_id]+',1);" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_product_variants); ?>"><?php echo addslashes($entry_add_product_variants); ?></button>';
	html+= '<button type="button" onclick="ro_delete_all_combinations(' + product_id + ', '+ro_tabs_cnt[product_id]+');" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="<?php echo addslashes($entry_delete_all_combs); ?>"><?php echo addslashes($entry_delete_all_combs); ?></button>';
	html+= '</div>';
	
	html+= '<div class="table-responsive">';
	html+= '<table class="table table-striped table-bordered table-hover">';
	html+= '<thead>';
	html+= '<tr>';
	html+= '<td class="text-left"><?php echo addslashes($entry_options_values); ?></td>';
	html+= '<td class="text-left" width="90"><?php echo addslashes($entry_related_options_quantity); ?>:</td>';
	
	var ro_fields = {
		spec_model: "<?php echo addslashes($entry_model); ?>"
		,spec_sku: "<?php echo addslashes($entry_sku); ?>"
		,spec_upc: "<?php echo addslashes($entry_upc); ?>"
		,spec_ean: "<?php echo addslashes($entry_ean); ?>"
		,spec_location: "<?php echo addslashes($entry_location); ?>"
		,spec_ofs: "<?php echo addslashes($entry_stock_status); ?>"
		,spec_weight: "<?php echo addslashes($entry_weight); ?>"
	};
	
	for (var i in ro_fields) {
		if (ro_settings[product_id][i] && ro_settings[product_id][i] != 0) {
			html+= '<td class="text-left" width="90">'+ro_fields[i]+'</td>';
		}
	}
	
	if (ro_settings[product_id]['spec_price'] ) {
		html+= '<td class="text-left" width="90" ><?php echo addslashes($entry_price); ?></td>';
		
		if (ro_settings[product_id]['spec_price_discount'] ) {
			html+= '<td class="text-left" style="90"><?php echo addslashes($tab_discount); ?>: <font style="font-weight:normal;font-size:80%;">(<?php echo addslashes(str_replace(":","",$entry_customer_group." | ".$entry_quantity." | ".$entry_price)); ?>)</font></td>';
		}
		
		if (ro_settings[product_id]['spec_price_special'] ) {
			html+= '<td class="text-left" style="90"><?php echo addslashes($tab_special); ?>: <font style="font-weight:normal;font-size:80%;">(<?php echo addslashes(str_replace(":","",$entry_customer_group." | ".$entry_price)); ?>)</font></td>';
		}
	}
	
	if (ro_settings[product_id]['select_first'] && ro_settings[product_id]['select_first'] == 1 ) {
		html+= '<td class="text-left" width="90" style="white-space:nowrap"><?php echo addslashes($entry_select_first_short); ?>:</td>';
	}
	
	html+= '<td class="text-left" width="90"></td>';
	
	html+= '<tr>';
	html+= '</thead>';
	html+= '<tbody id="tbody-ro-' + ro_tabs_cnt[product_id] + '"></tbody>';
	html+= '</table>';
	html+= '<div class="col-sm-2" >';
	html+= '<button type="button" onclick="ro_add_combination(' + product_id + ', ' + ro_tabs_cnt[product_id] + ', false);" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_related_options); ?>"><?php echo addslashes($entry_add_related_options); ?></button>';
	html+= '</div>';
	html+= '</div>';
	html+= '';
	html+= '';
	html+= '</div>';
	
	$('#form-<?php echo $link; ?>' + product_id + ' #tab-ro-' + ro_tabs_cnt[product_id]).html(html);
	
	ro_use_check(product_id, ro_tabs_cnt[product_id]);
	
	if (tab_data['ro']) {
		for (var i in tab_data['ro']) {
			ro_add_combination(product_id, ro_tabs_cnt[product_id], tab_data['ro'][i]);
		}
	}
	
	$('#form-<?php echo $link; ?>' + product_id + ' #ro_nav_tabs a[data-ro-cnt="' + ro_tabs_cnt[product_id] + '"]').click();
	
	ro_tabs_cnt[product_id]++;
	
	return ro_tabs_cnt[product_id] - 1;
}

function ro_use_check(product_id, ro_tabs_num) {
	$('#form-<?php echo $link; ?>' + product_id + ' #ro-use-data-' + ro_tabs_num).toggle('fast', 
		function() {
			$('input[type=radio][name=\'ro_data[' + ro_tabs_num + '][use]\'][value="1"]').is(':checked')
		}
	);
	
	ro_tab_name_change(product_id, ro_tabs_num);
}

function ro_add_combination(product_id, ro_tabs_num, params) {
	var rov_id = $('#form-<?php echo $link; ?>' + product_id + ' #rov-'+ro_tabs_num).val();
	var ro_variant = ro_variants[product_id][ rov_id ];

	var entry_add_discount = "<?php echo addslashes($entry_add_discount); ?>";
	var entry_del_discount_title = "<?php echo addslashes($entry_del_discount_title); ?>";
	
	var entry_add_special = "<?php echo addslashes($entry_add_special); ?>";
	var entry_del_special_title = "<?php echo addslashes($entry_del_special_title); ?>";
	
	str_add = '';
	str_add += "<tr id=\"related-option"+ro_counter[product_id]+"\"><td>";
	
	var div_id = "ro_status"+ro_counter[product_id];
	
	str_add +="<div id='"+div_id+"' style='color: red'></div>";
	
	for (var i in ro_variant['options']) {
		var ro_option = ro_variant['options'][i];
		var option_id = ro_option['option_id'];
	
		str_add += "<div style='float:left;'><label class='col-sm-1 control-label' for='ro_o_"+ro_counter[product_id]+"_"+option_id+"'> ";
		str_add += "<nobr>"+ro_option['name']+":</nobr>";
		str_add += "</label>";
		str_add += "<select class='form-control' id='ro_o_"+ro_counter[product_id]+"_"+option_id+"' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][options]["+option_id+"]' onChange=\"ro_refresh_status(" + product_id + ", " + ro_tabs_num + "," + ro_counter[product_id] + ")\">";
		str_add += "<option value=0></option>";
		
		for (var j in ro_all_options[product_id][option_id]['values']) {
			if((ro_all_options[product_id][option_id]['values'][j] instanceof Function) ) { continue; }
			
			var option_value_id = ro_all_options[product_id][option_id]['values'][j]['option_value_id'];
			
			str_add += "<option value='"+option_value_id+"'";
			
			if (params['options'] && params['options'][option_id] && params['options'][option_id] == option_value_id) str_add +=" selected ";
			
			str_add += ">"+ro_all_options[product_id][option_id]['values'][j]['name']+"</option>";
		}
		
		str_add += "</select>";
		str_add += "</div>";
	}
	
	str_add += "</td>";
	str_add += "<td>&nbsp;";
	str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][quantity]' size='2' value='"+(params['quantity']||0)+"'>";
	str_add += "<input type='hidden' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][relatedoptions_id]' value='"+(params['relatedoptions_id']||"")+"'>";
	str_add += "</td>";
	
	str_add += ro_add_text_field(product_id, ro_tabs_num, ro_counter[product_id], 'spec_model', params, 'model');
	str_add += ro_add_text_field(product_id, ro_tabs_num, ro_counter[product_id], 'spec_sku', params, 'sku');
	str_add += ro_add_text_field(product_id, ro_tabs_num, ro_counter[product_id], 'spec_upc', params, 'upc');
	str_add += ro_add_text_field(product_id, ro_tabs_num, ro_counter[product_id], 'spec_ean', params, 'ean');
	str_add += ro_add_text_field(product_id, ro_tabs_num, ro_counter[product_id], 'spec_location', params, 'location');
	
	if (ro_settings[product_id]['spec_ofs']) {
		str_add += '<td>';
		str_add += '&nbsp;<select name="ro_data['+ro_tabs_num+'][ro]['+ro_counter[product_id]+'][stock_status_id]" class="form-control">';
		str_add += '<option value="0">-</option>';
		
		<?php foreach ($stock_statuses as $stock_status) { ?>
		str_add += '<option value="<?php echo $stock_status["stock_status_id"]; ?>"';
		
		if ("<?php echo $stock_status['stock_status_id'] ?>" == params['stock_status_id']) {
			str_add += ' selected ';
		}
		
		str_add += '><?php echo addslashes($stock_status["name"]); ?></option>';
		<?php } ?>
		
		str_add += '</select>';
		str_add += '</td>';
	}
	
	if (ro_settings[product_id]['spec_weight']) {
		str_add += "<td>&nbsp;";
		str_add += "<select class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][weight_prefix]'>";
		str_add += "<option value='=' "+( (params['weight_prefix'] && params['weight_prefix']=='=')? ("selected") : (""))+">=</option>";
		str_add += "<option value='+' "+( (params['weight_prefix'] && params['weight_prefix']=='+')? ("selected") : (""))+">+</option>";
		str_add += "<option value='-' "+( (params['weight_prefix'] && params['weight_prefix']=='-')? ("selected") : (""))+">-</option>";
		str_add += "</select>";
		str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][weight]' value=\""+(params['weight']||'0.000')+"\" size='5'>";
		str_add += "</td>";
	}
	
	if (ro_settings[product_id]['spec_price'])	{
		str_add += "<td>&nbsp;";
		
		if (ro_settings[product_id]['spec_price_prefix']) {
			str_add += "<select name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][price_prefix]' class='form-control'>";
			
			var price_prefixes = ['=', '+', '-'];
			
			for (var i in price_prefixes) {
				if((price_prefixes[i] instanceof Function) ) { continue; }
				var price_prefix = price_prefixes[i];
				str_add += "<option value='"+price_prefix+"' "+(price_prefix==params['price_prefix']?"selected":"")+">"+price_prefix+"</option>";
			}
			
			str_add += "</select>";
		}
		
		str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][price]' value='"+(params['price']||'')+"' size='10'>";
		str_add += "</td>";
	}
	
	
	if (ro_settings[product_id]['spec_price'] && ro_settings[product_id]['spec_price_discount'])	{
		str_add += "<td>";
		str_add += "<button type='button' onclick=\"ro_add_discount(" + product_id + ", "+ro_tabs_num+", "+ro_counter[product_id]+", '');\" data-toggle='tooltip' title='"+entry_add_discount+"' class='btn btn-primary'><i class='fa fa-plus-circle'></i></button>";
		str_add += "<div id='ro_price_discount"+ro_counter[product_id]+"' >";
		str_add += "</div>";
		str_add += "</td>";	
	}
	
	if (ro_settings[product_id]['spec_price'] && ro_settings[product_id]['spec_price_special'])	{
		str_add += "<td>";
		str_add += "<button type='button' onclick=\"ro_add_special(" + product_id + ", "+ro_tabs_num+", "+ro_counter[product_id]+", '');\" data-toggle='tooltip' title='"+entry_add_special+"' class='btn btn-primary'><i class='fa fa-plus-circle'></i></button>";
		str_add += "<div id='ro_price_special"+ro_counter[product_id]+"'>";
		str_add += "</div>";
		str_add += "</td>";	
	}
	
	if (ro_settings[product_id]['select_first'] && ro_settings[product_id]['select_first']==1) {
		str_add += "<td>&nbsp;";
		str_add += "<input id='defaultselect_"+ro_counter[product_id]+"' type='checkbox' onchange='ro_check_defaultselectpriority(" + product_id + ", "+ro_tabs_num+");' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][defaultselect]' "+((params && params['defaultselect']==1)?("checked"):(""))+" value='1'>";
		str_add += "<input id='defaultselectpriority_"+ro_counter[product_id]+"' type='text' class='form-control' title='<?php echo $entry_select_first_priority; ?>' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][defaultselectpriority]'  value=\""+((params && params['defaultselectpriority'])?(params['defaultselectpriority']):(""))+"\" >";
		str_add += "</td>";	
	}
	
	str_add += "<td><br>";
	str_add += "<button type=\"button\" class='btn btn-danger' onclick=\"$('#related-option" + ro_counter[product_id] + "').remove();ro_refresh_status(" + product_id + ", " + ro_tabs_num + ");\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class='btn btn-primary' data-original-title=\"<?php echo $button_remove; ?>\" ><i class=\"fa fa-minus-circle\"></i></button>";
	str_add += "</td></tr>";
	
	$('#form-<?php echo $link; ?>' + product_id + ' #tbody-ro-'+ro_tabs_num).append(str_add);
	
	if (ro_settings[product_id]['spec_price'] && ro_settings[product_id]['spec_price_discount'])	{
		if (params && params['discounts'] ) {
			for (var i_dscnt in params['discounts']) {
				if (!params['discounts'].hasOwnProperty(i_dscnt)) continue;
				
				ro_add_discount(product_id, ro_tabs_num, ro_counter[product_id], params['discounts'][i_dscnt]);
			}
		}
	}
	
	if (ro_settings[product_id]['spec_price'] && ro_settings[product_id]['spec_price_special'])	{
		if (params && params['specials'] ) {
			for (var i_dscnt in params['specials']) {
				if (!params['specials'].hasOwnProperty(i_dscnt)) continue;
				
				ro_add_special(product_id, ro_tabs_num, ro_counter[product_id], params['specials'][i_dscnt]);
			}
		}
	}
	
	ro_update_combination(product_id, ro_tabs_num, ro_counter[product_id]);
	
	if (params == false) {
		ro_refresh_status(product_id, ro_tabs_num);
		ro_check_defaultselectpriority(product_id, ro_tabs_num);
	}
	
	ro_counter[product_id]++;
}

function ro_refresh_status(product_id, ro_tabs_num, ro_num) {
	if (ro_num || ro_num==0) {
		ro_update_combination(product_id, ro_tabs_num, ro_num);
	}
	
	var rov_id = $('#form-<?php echo $link; ?>' + product_id + ' #rov-' + ro_tabs_num).val();
	var ro_variant = ro_variants[product_id][rov_id];
	
	$('#form-<?php echo $link; ?>' + product_id + ' #tab-ro-' + ro_tabs_num + ' div[id^=ro_status]').html('');
	
	var opts_combs = [];
	var checked_opts_combs = [];
	
	$('#form-<?php echo $link; ?>' + product_id + ' #tab-ro-' + ro_tabs_num + ' tr[id^=related-option]').each( function () {
		var opts_comb = $(this).attr('ro_opts_comb');
		
		if ( $.inArray(opts_comb, opts_combs) != -1 && $.inArray(opts_comb, checked_opts_combs) == -1 ) {
			$('#form-<?php echo $link; ?>' + product_id + ' #tab-ro-' + ro_tabs_num + ' tr[ro_opts_comb=' + opts_comb + ']').each( function () {
				$(this).find('div[id^=ro_status]').html('<?php echo $warning_equal_options; ?>');
			});
			checked_opts_combs.push(opts_comb);
		} else {
			opts_combs.push(opts_comb);
		}
	})
	
	return;
}

function ro_update_combination(product_id, ro_tabs_num, ro_num) {
	var rov_id = $('#form-<?php echo $link; ?>' + product_id + ' #rov-' + ro_tabs_num).val();
	var ro_variant = ro_variants[product_id][ rov_id ];
	var str_opts = "";
	
	for (var i in ro_variant['options']) {
		if((ro_variant['options'][i] instanceof Function) ) { continue; }
		
		var option_id = ro_variant['options'][i]['option_id'];
	
		str_opts += "_o"+option_id;
		str_opts += "_"+$('#ro_o_'+ro_num+'_'+option_id).val();
	}
	
	$('#form-<?php echo $link; ?>' + product_id + ' #related-option' + ro_num).attr('ro_opts_comb', str_opts);
}

function ro_add_text_field(product_id, ro_tabs_num, ro_num, setting_name, params, field_name) {
	str_add = "";
	
	if (ro_settings[product_id][setting_name] && ro_settings[product_id][setting_name] != '0') {
		str_add += "<td>&nbsp;";
		str_add += "<input type='text' class='form-control' name='ro_data[" + ro_tabs_num+"][ro][" + ro_num + "][" + field_name + "]' value=\"" + (params[field_name]||'') + "\">";
		str_add += "</td>";
	}
	
	return str_add;
}

function ro_add_discount(product_id, ro_tabs_num, ro_counter, discount) {
	var first_name = "ro_data[" + ro_tabs_num + "][ro][" + ro_counter[product_id] + "][discounts][" + ro_discount_counter[product_id] + "]";
	var customer_group_id = (discount == "") ? (0) : (discount['customer_group_id']);
	
	str_add = "";
	str_add += "<table id='related-option-discount"+ro_discount_counter[product_id]+"' style='width:300px;'><tr><td>";
	
	str_add += "<select name='"+first_name+"[customer_group_id]' class='form-control' title=\"<?php echo htmlspecialchars($entry_customer_group); ?>\" style='float:left;width:80px;'>";
	<?php foreach ($customer_groups as $customer_group) { ?>
	str_add += "<option value='<?php echo $customer_group['customer_group_id']; ?>' "+((customer_group_id==<?php echo $customer_group['customer_group_id']; ?>)?("selected"):(""))+"><?php echo $customer_group['name']; ?></option>";
	<?php } ?>
	str_add += "</select>";
	str_add += "<input type='text' class='form-control' style='float:left;width:100px;' size='2' name='"+first_name+"[quantity]' value='"+((discount=="")?(""):(discount['quantity']))+"' title=\"<?php echo htmlspecialchars($entry_quantity); ?>\">";
	str_add += "";
	// hidden
	str_add += "<input type='hidden' name='"+first_name+"[priority]' value='"+((discount=="")?(""):(discount['priority']))+"' title=\"<?php echo htmlspecialchars($entry_priority); ?>\">";
	str_add += "<input type='text' class='form-control' style='float:left;width:80px;' size='10' name='"+first_name+"[price]' value='"+((discount=="")?(""):(discount['price']))+"' title=\"<?php echo htmlspecialchars($entry_price); ?>\">";
	str_add += "<button type=\"button\" onclick=\"$('#related-option-discount" + ro_discount_counter[product_id] + "').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\" style='float:left;' data-original-title=\"\"><i class=\"fa fa-minus-circle\"></i></button>";
	str_add += "</td></tr></table>";
	
	$('#form-<?php echo $link; ?>' + product_id + ' #ro_price_discount' + ro_counter[product_id]).append(str_add);
	
	ro_discount_counter[product_id]++;
}

function ro_add_special(product_id, ro_tabs_num, ro_counter, special) {
	var first_name = "ro_data["+ro_tabs_num+"][ro]["+ro_counter[product_id]+"][specials]["+ro_special_counter[product_id]+"]";
	var customer_group_id = (special=="")?(0):(special['customer_group_id']);
	
	str_add = "";
	str_add += "<table id='related-option-special"+ro_special_counter[product_id]+"' style='width:200px;'><tr><td>";
	str_add += "<select name='"+first_name+"[customer_group_id]' class='form-control' style='float:left;width:80px;' title=\"<?php echo htmlspecialchars($entry_customer_group); ?>\">";
	<?php foreach ($customer_groups as $customer_group) { ?>
	str_add += "<option value='<?php echo $customer_group['customer_group_id']; ?>' "+((customer_group_id==<?php echo $customer_group['customer_group_id']; ?>)?("selected"):(""))+"><?php echo $customer_group['name']; ?></option>";
	<?php } ?>
	str_add += "</select>";
	// hidden
	str_add += "<input type='hidden' size='2' name='"+first_name+"[priority]' value='"+((special=="")?(""):(special['priority']))+"' title=\"<?php echo htmlspecialchars($entry_priority); ?>\">";
	str_add += "<input type='text'  class='form-control' style='float:left;width:80px;' size='10' name='"+first_name+"[price]' value='"+((special=="")?(""):(special['price']))+"' title=\"<?php echo htmlspecialchars($entry_price); ?>\">";
	str_add += "<button type=\"button\" onclick=\"$('#related-option-special" + ro_special_counter[product_id] + "').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\" style='float:left;' data-original-title=\"<?php echo $button_remove; ?>\"><i class=\"fa fa-minus-circle\"></i></button>";
	str_add += "</td></tr></table>";
	
	$('#form-<?php echo $link; ?>' + product_id + ' #ro_price_special' + ro_counter[product_id]).append(str_add);
	
	ro_special_counter[product_id]++;
}

function ro_delete_all_combinations(product_id, ro_tabs_num) {
	if (confirm('<?php echo $text_delete_all_combs; ?>')) {
		// fastest
		$('#form-<?php echo $link; ?>' + product_id + ' #tbody-ro-' + ro_tabs_num + ' tr').detach().remove();
		//$('#tbody-ro-'+ro_tabs_num).empty();
		//$('#tbody-ro-'+ro_tabs_num+' tr').remove();
		//$('#tbody-ro-'+ro_tabs_num).html('');
		ro_refresh_status(product_id, ro_tabs_num);
	}
}

function numberOfPossibleCombinations(product_id, ro_variant) {
	var numberOfCombs = 1;
	
	for (var i in ro_variant['options']) {
		var option_id = ro_variant['options'][i]['option_id'];
		var numberOfValues = ro_all_options[product_id][option_id]['values'].length || 1;
		
		numberOfCombs = numberOfCombs * numberOfValues;
	}
	
	return numberOfCombs;
}

function confirmNumberOfCombinations(numberOfCombs) {
	var maxNumberOfCombinations = <?php echo $maxNumberOfCombinations; ?>;
	var confirmNumberOfCombinations = <?php echo $confirmNumberOfCombinations; ?>;
	
	if (numberOfCombs > maxNumberOfCombinations) {
		alert('<?php echo $text_combs_number; ?>'+numberOfCombs.toString()+'<?php echo $text_combs_number_out_of_limit; ?>');
		
		return false;
	} else if (numberOfCombs > confirmNumberOfCombinations ) {
		if (!confirm('<?php echo $text_combs_number; ?>'+numberOfCombs.toString()+'<?php echo $text_combs_number_is_big; ?>')) {
			return false;
		}
	} else {
		if (!confirm(numberOfCombs.toString()+'<?php echo $text_combs_will_be_added; ?>')) {
			return false;
		}
	}
	
	return true;
}

function ro_fill_all_combinations(product_id, ro_tabs_num, product_options_only) {
	var rov_id = $('#form-<?php echo $link; ?>' + product_id + ' #rov-' + ro_tabs_num).val();
	var ro_variant = ro_variants[product_id][ rov_id ];
	var all_vars = [];
	
	if (product_options_only) {
		var this_product_options = [];
		
		$('#form-<?php echo $link; ?>' + product_id + ' select[name^=product_option][name*=option_value_id]').each(function() {
			if ( $(this).val() ) {
				this_product_options.push($(this).val());
			}
		});
	}
	
	if (!product_options_only) {
		// if all options used, there may be millinons of combinations, it may freeze script before determination of combinations list
		var numberOfCombs = numberOfPossibleCombinations(product_id, ro_variant);
		
		if (!confirmNumberOfCombinations(numberOfCombs)) {
			return;
		}
	}
	
	var reversed_options = [];
	
	for (var i in ro_variant['options']) {
		if((ro_variant['options'][i] instanceof Function) ) { continue; }
		reversed_options.unshift(i);
	}
	
	for (var i_index in reversed_options) {
		var i = reversed_options[i_index];
		var option_id = ro_variant['options'][i]['option_id'];
		var temp_arr = [];
		
		for (var j in ro_all_options[product_id][option_id]['values']) {
			if((ro_all_options[product_id][option_id]['values'][j] instanceof Function) ) { continue; }
			
			var option_value_id = ro_all_options[product_id][option_id]['values'][j]['option_value_id']
			
			if (product_options_only && $.inArray(option_value_id, this_product_options) == -1 ) { //
				continue;
			}
			
			if (all_vars.length) {
				for (var k in all_vars) {
					if((all_vars[k] instanceof Function) ) { continue; }
					
					var comb_arr = all_vars[k].slice(0);
					comb_arr[option_id] = option_value_id;
					temp_arr.push( comb_arr );
				}
			} else {
				var comb_arr = [];
				comb_arr[option_id] = option_value_id;
				temp_arr.push(comb_arr);
			}
		}
		if (temp_arr && temp_arr.length) {
			all_vars = temp_arr.slice(0);
		}
	}
	
	if (all_vars.length) {
		if (product_options_only) {
			var numberOfCombs = all_vars.length;
			if (!confirmNumberOfCombinations(numberOfCombs)) {
				return;
			}
		}
		
		for (var i in all_vars) {
			if((all_vars[i] instanceof Function) ) { continue; }
			
			rop = {};
			
			for (var j in all_vars[i]) {
				if((all_vars[i][j] instanceof Function) ) { continue; }
				rop[j] = all_vars[i][j];
			}
			
			ro_add_combination(product_id, ro_tabs_num, {options: rop});
		}
		
		ro_use_check(product_id, ro_tabs_num);
		ro_refresh_status(product_id, ro_tabs_num);
		ro_check_defaultselectpriority(product_id, ro_tabs_num);
	}
	
}

// check priority fields (is it available or not) for default options combination
function ro_check_defaultselectpriority(product_id, ro_tabs_num) {
	var dsc = $('#form-<?php echo $link; ?>' + product_id + ' #tab-ro-' + ro_tabs_num + ' input[type=checkbox][id^=defaultselect_]');
	var dsp;
	
	for (var i = 0; i < dsc.length; i++) {
		dsp = $('#form-<?php echo $link; ?>' + product_id + ' #defaultselectpriority_'+dsc[i].id.substr(14));
		
		if (dsp && dsp.length) {
			if (dsc[i].checked) {
				dsp[0].style.display = '';
				
				if (isNaN(parseInt(dsp[0].value))) {
					dsp[0].value = 0;
				}
				
				if (parseInt(dsp[0].value)==0) {
					dsp[0].value = "1";
				}
			} else {
				dsp[0].style.display = 'none';
			}
		}
	}
}

function check_max_input_vars(product_id) {
	var max_input_vars = <?php echo $max_input_vars; ?>;
	
	if (max_input_vars && !$('#form-<?php echo $link; ?>' + product_id + ' #warning_max_input_vars').length) {
		var input_vars = $('#form-<?php echo $link; ?>' + product_id).find('select, input, textarea').length;
		
		if (input_vars / max_input_vars * 100 > 80) {
			var html = '<div class="alert alert-danger" id="warning_max_input_vars"><i class="fa fa-exclamation-circle"></i> <?php echo addslashes($warning_max_input_vars); ?></div>';
			
			$('#form-<?php echo $link; ?>' + product_id + ' div.panel:first').before(html);
		}
	}
}

setInterval(function() {
	check_max_input_vars(<?php echo $product_id; ?>);
}, 1000);

if (ro_data[<?php echo $product_id; ?>] && ro_settings[<?php echo $product_id; ?>]) {
	ro_tabs_cnt[<?php echo $product_id; ?>] = 0;
	
	for (var i in ro_data[<?php echo $product_id; ?>]) {
		var ro_tabs_num = ro_add_tab(<?php echo $product_id; ?>, ro_data[<?php echo $product_id; ?>][i]);
		
		ro_tabs_cnt[<?php echo $product_id; ?>]++;
		
		ro_use_check(<?php echo $product_id; ?>, ro_tabs_num);
		ro_refresh_status(<?php echo $product_id; ?>, ro_tabs_num);
		ro_check_defaultselectpriority(<?php echo $product_id; ?>, ro_tabs_num);
		
		// RO
		break;
	}
	
	// RO
	if (!ro_tabs_cnt[<?php echo $product_id; ?>]) {
		ro_add_tab(<?php echo $product_id; ?>);
	}
}

//--></script>

<?php if ($product_id == -1) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#dialog-<?php echo $link; ?>').find('.modal-header').append('<?php echo ${"text_" . $link}; ?>');
});
//--></script>
<?php } ?>

<?php if ($product_id > 0) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#dialogLink').find('.modal-header').append('<?php echo ${"text_" . $link}; ?>');
});
//--></script>
<?php } ?>

<?php if ($product_id > -1) { ?>
<script type="text/javascript"><!--
autocompleteProductCopyData('<?php echo $product_id; ?>');
//--></script>
<?php } ?>